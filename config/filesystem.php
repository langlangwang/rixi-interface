<?php

return [
    // 默认磁盘
    'default' => env('filesystem.driver', 'local'),
    // 磁盘列表
    'disks'   => [
        'local'  => [
            'type' => 'local',
            'root' => app()->getRuntimePath() . 'storage',
        ],
        'public' => [
            // 磁盘类型
            'type'       => 'local',
            // 磁盘路径
            'root'       => app()->getRootPath() . 'public/storage',
            // 磁盘路径对应的外部URL路径
            'url'        => '/storage',
            // 可见性
            'visibility' => 'public',
        ],
        // 更多的磁盘配置信息
        "qiniu" => [
            "type" => "qiniu",
            "accessKey" => "",
            "secretKey" => "",
            "bucket" => "",
            "domain" => ""
        ],
        "oss" => [
            "type" => "oss",
            "accessId" => "LTAIZ3RALvwBApKQ",
            "accessSecret" => "x4fZfzrJ5O3bKlAbiYyqbtKVegcJq3",
            "bucket" => "qilu-oss",
            "endpoint" => "oss-cn-hangzhou.aliyuncs.com",
            "domain" => "files.jiubaibei.com"
        ],
        "cos" => [
            "type" => "cos",
            "region" => "ap-guangzhou",
            "credentials" => [
                "appId" => "",
                "secretId" => "",
                "secretKey" => ""
            ],
            "bucket" => "",
            "domain" => "",
            "scheme" => "http"
        ]
    ],
];
