<?php
return [
    // 用户API操作日志消息

    // dev/test 配置路径
    // php /www/wwwroot/test-api.rxx1316com/community/think cli stats_goods

    // php think cli test --msg='{"scene":"user_level_check","uid":1002395}'
    'test'                        => [
        'class' => 'app\command\logic\CliTestLogic',
    ],

    // 统计-商城数据
    // php think cli stats_order --msg='{"days":30}'
    'stats_order'                 => [
        'class' => 'app\command\logic\CliStatsOrderLogic',
    ],

    // 统计-会员数据
    // php think cli stats_member --msg='{"date":"2024-07-02"}'
    // php think cli stats_member --msg='{"days":30}'
    'stats_member'                => [
        'class' => 'app\command\logic\CliStatsMemberLogic',
    ],

    // 统计-社区数据
    // php think cli stats_community --msg='{"date":"2024-07-05"}'
    // php think cli stats_community --msg='{"days":30}'
    'stats_community'             => [
        'class' => 'app\command\logic\CliStatsCommunityLogic',
    ],

    // 统计-商平数据
    // php think cli stats_goods --msg='{"date":"2024-07-02"}'
    // php think cli stats_goods --msg='{"days":30}'
    'stats_goods'                 => [
        'class' => 'app\command\logic\CliStatsGoodsLogic',
    ],

    // 统计-交易数据
    // php think cli stats_transaction --msg='{"date":"2024-07-02"}'
    // php think cli stats_transaction --msg='{"days":30}'
    'stats_transaction'           => [
        'class' => 'app\command\logic\CliStatsTransactionLogic',
    ],

    // 统计-控制台数据
    // php think cli stats_home --msg='{"date":"2024-07-02"}'
    // php think cli stats_home --msg='{"days":30}'
    'stats_home'                  => [
        'class' => 'app\command\logic\CliStatsHomeLogic',
    ],

    // 统计-财务明细
    // php think cli stats_finance_detail --msg='{"date":"2024-07-02"}'
    // php think cli stats_finance_detail --msg='{"days":30}'
    'stats_finance_detail'        => [
        'class' => 'app\command\logic\CliStatsFinanceDetailLogic',
    ],

    // 统计-财务概览
    // php think cli stats_finance_overview --msg='{"date":"2024-07-02"}'
    // php think cli stats_finance_overview --msg='{"days":30}'
    'stats_finance_overview'      => [
        'class' => 'app\command\logic\CliStatsFinanceOverviewLogic',
    ],

    // 统计-商家财务概览
    // php think cli stats_shop_finance_overview --msg='{"date":"2024-07-02"}'
    // php think cli stats_shop_finance_overview --msg='{"days":30}'
    'stats_shop_finance_overview' => [
        'class' => 'app\command\logic\CliStatsShopFinanceOverviewLogic',
    ],

    // 统计-商家-交易数据
    // php think cli stats_shop_transaction --msg='{"date":"2024-07-02"}'
    // php think cli stats_shop_transaction --msg='{"days":30}'
    'stats_shop_transaction'      => [
        'class' => 'app\command\logic\CliStatsShopTransactionLogic',
    ],

    // 统计-商家-会员数据
    // php think cli stats_shop_member --msg='{"date":"2024-07-02"}'
    // php think cli stats_shop_member --msg='{"days":30}'
    'stats_shop_member'           => [
        'class' => 'app\command\logic\CliStatsShopMemberLogic',
    ],

    // 统计-商家-商平数据
    // php think cli stats_shop_goods --msg='{"date":"2024-07-02"}'
    // php think cli stats_shop_goods --msg='{"days":30}'
    'stats_shop_goods'            => [
        'class' => 'app\command\logic\CliStatsShopGoodsLogic',
    ],

    // 统计-商家-数据
    // php think cli stats_shop_data --msg='{"date":"2024-07-02"}'
    // php think cli stats_shop_data --msg='{"days":30}'
    'stats_shop_data'             => [
        'class' => 'app\command\logic\CliStatsShopDatqLogic',
    ],

    // 统计-会员推荐管理
    // php think cli stats_shop_data --msg='{"date":"2024-07-02"}'
    // php think cli stats_shop_data --msg='{"days":30}'
    'stats_invite_manage'             => [
        'class' => 'app\command\logic\CliStatsInviteManageLogic',
    ],

    // 统计-每日数据
    // php think cli stats_data --msg='{"date":"2024-07-02"}'
    // php think cli stats_data --msg='{"days":30}'
    'stats_data'      => [
        'class' => 'app\command\logic\CliStatsDataLogic',
    ],

    // 统计-会员竞拍数据
    // php think cli stats_community_member_data
    'stats_community_member_data' => [
        'class' => 'app\command\logic\CliStatsCommunityMemberLogic',
    ],

    // 统计-个人财务数据
    // php think cli stats_community_member_data
    'stats_person_member_data' => [
        'class' => 'app\command\logic\CliStatsPersonMemberLogic',
    ],
    // 统计-杉德提现充值
    'stats_sand_member_data' => [
        'class' => 'app\command\logic\CliStatsSandMemberLogic',
    ],
];
