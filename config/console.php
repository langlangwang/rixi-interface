<?php
// +----------------------------------------------------------------------
// | 控制台配置
// +----------------------------------------------------------------------
return [
    // 指令定义
    'commands' => [
        'cli'                           => \app\command\CliRouter::class,
        'mq:consumer'                   => \app\command\RabbitMQConsumerRouter::class,
        'test:test'                     => \app\command\Test::class,
        'order:cancel'                  => \app\command\OrderCancel::class,
        'community:release'             => \app\command\CommunityRelease::class,
        'community:cancel'              => \app\command\CommunityCancel::class,
        'community:sign:cancel'         => \app\command\CommunitySignCancel::class,
        'rabbitmq:community:order'      => \app\command\RabbitMQConsumer::class,
        'rabbitmq:order:callback'       => \app\command\RabbitMQOrderCallback::class,
        'rabbitmq:info'                 => \app\command\RabbitMQInfo::class,
        'rabbitmq:order:delay:callback' => \app\command\RabbitMQOrderDelayCallback::class,
        'newdata'                       => \app\command\Newdata::class,
        'community:order:refund'        => \app\command\CommunityOrderRefund::class,
        'supply:pull:goods'             => \app\command\SupplyPullGoods::class,
        'supply:message:pool'           => \app\command\SupplyMessagePool::class,
        'supply:order'                  => \app\command\SupplyOrder::class,
        'sand:test'                     => \app\command\SandTest::class,
        'profit'                        => \app\command\Profit::class,
        'profit:pool:reissue'           => \app\command\ProfitPoolReissue::class,
        'reduce:activity'               => \app\command\ReduceActivity::class,
        'community:order:op:refund'     => \app\command\CommunityOrderOpRefund::class,
        'prepay:community'              => \app\command\PrepayCommunityUser::class,
        'prepay:community:refund'       => \app\command\PrepayCommunityRefund::class,
        'china'                         => \app\command\China::class,
        'rank'                          => \app\command\Rank::class,
        'rank:data'                     => \app\command\RankData::class,
        'carefree'                         => \app\command\Carefree::class,
        'send:partner'                  => \app\command\SendPartner::class,
    ],
];
