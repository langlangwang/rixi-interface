<?php
return [
    'hosts'                 => [
        'host'     => env('rabbitmq.hostname'),
        'port'     => env('rabbitmq.port'),
        'username' => env('rabbitmq.username'),
        'password' => env('rabbitmq.password'),
        'vhost'    => env('rabbitmq.vhost', '/'),
    ],

    // 用户API操作日志消息
    // php think mq:consumer member_oplog
    'member_oplog'          => [
        'exchange_type' => 'fanout',
        'exchange_name' => 'member_oplog.exchange',
        'exchange_args' => [],
        'queue_name'    => 'member_oplog.queue',
        'queue_args'    => [], //
        'route_key'     => 'member_oplog.route',
        'class'         => 'app\command\logic\ConsumerLogicApiLog',
    ],

    //社区下单
    'community_order_queue' => [
        'queue_name'    => 'community.order.queue',
        'route_key'     => 'community.order.route',
        'exchange_name' => 'community.order.exchange',
    ],
    //订单回调处理
    'order_callback_queue'  => [
        'queue_name'    => 'order.callback.queue',
        'route_key'     => 'order.callback.route',
        'exchange_name' => 'order.callback.exchange',
    ],
    //同步信息
    'info_queue'            => [
        'queue_name'    => 'info.queue',
        'route_key'     => 'info.route',
        'exchange_name' => 'info.exchange',
    ],
    //延迟队列
    'delay_order_queue'     => [
        'queue_name'    => 'delay.order.queue',
        'route_key'     => 'delay.order.route',
        'exchange_name' => 'delay.order.exchange',
    ],
];
