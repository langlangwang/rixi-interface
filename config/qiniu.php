<?php

return [
    'accessKey' => env('qiniu.accesskey'),
    'secretKey' => env('qiniu.secretkey'),
    'bucket' => env('qiniu.bucket'),
    'url' => env('qiniu.url')
];