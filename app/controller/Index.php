<?php

namespace app\controller;

use app\BaseController;
use app\common\models\SystemHelp;

class Index extends BaseController
{
    public function index()
    {
        return '';
    }

    public function hello($name = 'ThinkPHP6')
    {
        return 'hello,' . $name;
    }

    public function help()
    {
        $id = input('id', 0);
        if (empty($id)) {
            return '';
        }
        $data = SystemHelp::getInstance()->where(['id' => $id])->find();

        if (empty($data)) {
            return '';
        }
        echo "<html><title>" . $data['title'] . "</title><body><div>" . $data['content'] . "</div></body></html>";
    }
}
