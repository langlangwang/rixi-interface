<?php
namespace app\common\libs;

use think\Paginator;

/**
 * PageBootstrap 分页驱动
 */
class PageBootstrap extends Paginator
{
    /**
     * 渲染分页html
     * @return mixed
     */
    public function render()
    {
        if ($this->hasPages()) {
            if ($this->simple) {
                return sprintf(
                    '<ul class="pager">%s %s</ul>',
                    $this->getPreviousButton(),
                    $this->getNextButton()
                );
            } else {
                return sprintf(
                    '<ul class="pagination">%s %s %s</ul>',
                    $this->getPreviousButton(),
                    $this->getLinks(),
                    $this->getNextButton()
                );
            }
        }
    }

    /**
     * 转换为数组
     * @return array
     */
    public function toArray(): array
    {
        if ($this->simple) {
            return [
                'rows' => $this->items->toArray(),
            ];
        }
        try {
            $total = $this->total();
        } catch (DomainException $e) {
            $total = null;
        }

        return [
            'total'        => $total,
            'per_page'     => $this->listRows(),
            'current_page' => $this->currentPage(),
            'last_page'    => $this->lastPage,
            'rows'         => $this->items->toArray(),
        ];
    }

}
