<?php

namespace app\common\libs;

/**
 * 
 */
trait Singleton
{
    private static $instance;

    /**
     * @param array $args
     * @return static
     */
    public static function getInstance(...$args)
    {
        if (!isset(self::$instance)) {
            self::$instance = new static(...$args);
        }
        return self::$instance;
    }
}