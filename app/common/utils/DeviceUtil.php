<?php


namespace app\common\utils;


class DeviceUtil
{
    /**
     * 获取头信息
     *
     * @return string
     */
    public static function getUserAgent()
    {
        return request()->header('USER_AGENT');
    }

    /**
     * @return bool
     *
     */
    public static function isApp()
    {
        $ag = static::getUserAgent();
        CommonUtil::log('useragent', $ag);
        if (strpos($ag, 'okhttp') !== false) {
            return true;
        }
        return false;
    }
}
