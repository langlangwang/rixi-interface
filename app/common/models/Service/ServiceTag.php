<?php
namespace app\common\models\Service;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class ServiceTag extends BaseModel
{
    use Singleton;

    protected $table = 'service_tag';
}