<?php
namespace app\common\models\Service;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class ServiceGoods extends BaseModel
{
    use Singleton;

    protected $table = 'service_goods';
}