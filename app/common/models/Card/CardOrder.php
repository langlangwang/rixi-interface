<?php


namespace app\common\models\Card;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class CardOrder extends BaseModel
{
    use Singleton;
    protected $table = 'card_order';
}