<?php
namespace app\common\models;

use app\common\libs\Singleton;
use app\common\models\BaseModel;

class Coupon extends BaseModel
{
    use Singleton;
    protected $table = 'coupon2';
}
