<?php


namespace app\common\models\Shop;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class ShopPic extends BaseModel
{
    use Singleton;
    protected $table = 'member_shop_pic';
}