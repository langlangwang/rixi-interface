<?php
namespace app\common\models\Shop;

use app\common\libs\Singleton;
use app\common\models\BaseModel;

class Shop extends BaseModel
{
    use Singleton;
    protected $table = 'member_shop';
}
