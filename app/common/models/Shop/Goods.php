<?php


namespace app\common\models\Shop;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class Goods extends BaseModel
{
    use Singleton;
    protected $table = 'goods';
}