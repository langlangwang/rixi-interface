<?php


namespace app\common\models\Shop;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class GoodsSpec extends BaseModel
{
    use Singleton;
    protected $table = 'goods_spec';
}