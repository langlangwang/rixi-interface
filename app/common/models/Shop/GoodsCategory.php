<?php


namespace app\common\models\Shop;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class GoodsCategory extends BaseModel
{
    use Singleton;
    protected $table = 'goods_category';
}