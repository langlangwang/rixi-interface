<?php
namespace app\common\models;

use app\common\libs\Singleton;
use app\common\models\BaseModel;

class ExchangeOrderGoods extends BaseModel
{
    use Singleton;
    protected $table = 'exchange_order_goods';
}
