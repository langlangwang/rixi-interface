<?php


namespace app\common\models\Member;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class MemberEarningsLog extends BaseModel
{
    use Singleton;
    protected $table = 'member_earnings_amount_log';
}