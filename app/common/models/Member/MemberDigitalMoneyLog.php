<?php


namespace app\common\models\Member;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class MemberDigitalMoneyLog extends BaseModel
{
    use Singleton;
    protected $table = 'member_digital_money_log';
}