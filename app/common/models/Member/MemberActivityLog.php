<?php


namespace app\common\models\Member;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class MemberActivityLog extends BaseModel
{
    use Singleton;
    protected $table = 'member_activity_log';
}