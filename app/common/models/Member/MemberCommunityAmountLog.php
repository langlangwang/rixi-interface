<?php


namespace app\common\models\Member;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class MemberCommunityAmountLog extends BaseModel
{
    use Singleton;
    protected $table = 'member_community_amount_log';
}