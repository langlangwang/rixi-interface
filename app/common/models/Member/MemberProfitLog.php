<?php


namespace app\common\models\Member;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class MemberProfitLog extends BaseModel
{
    use Singleton;
    protected $table = 'member_profit_log';
}