<?php


namespace app\common\models\Member;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class MemberDeductionAmountLog extends BaseModel
{
    use Singleton;
    protected $table = 'member_deduction_amount_log';
}