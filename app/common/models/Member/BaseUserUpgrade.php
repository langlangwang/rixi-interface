<?php


namespace app\common\models\Member;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class BaseUserUpgrade extends BaseModel
{
    use Singleton;
    protected $table = 'base_user_upgrade';
}