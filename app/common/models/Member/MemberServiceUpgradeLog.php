<?php


namespace app\common\models\Member;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class MemberServiceUpgradeLog extends BaseModel
{
    use Singleton;
    protected $table = 'member_service_upgrade_log';
}