<?php


namespace app\common\models\Member;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class MemberShopInviteFundLog extends BaseModel
{
    use Singleton;
    protected $table = 'member_shop_invite_fund';
}