<?php


namespace app\common\models\Member;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class MemberRechargeMoneyList extends BaseModel
{
    use Singleton;
    protected $table = 'member_recharge_money_list';
}