<?php


namespace app\common\models\Member;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class MemberInviteFundLog extends BaseModel
{
    use Singleton;
    protected $table = 'member_invite_fund';
}