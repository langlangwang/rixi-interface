<?php


namespace app\common\models\Member;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class MemberScoreLog extends BaseModel
{
    use Singleton;
    protected $table = 'member_score_amount_log';
}