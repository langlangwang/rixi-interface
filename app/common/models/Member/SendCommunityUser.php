<?php

namespace app\common\models\Member;

use app\common\libs\Singleton;
use app\common\models\BaseModel;

class SendCommunityUser extends BaseModel
{
    use Singleton;
    protected $table = 'send_community_user';
}