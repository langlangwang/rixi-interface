<?php


namespace app\common\models\Member;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class MemberGxzLog extends BaseModel
{
    use Singleton;
    protected $table = 'member_gxz_log';
}