<?php

namespace app\common\models\Member;

use app\common\libs\Singleton;
use app\common\models\BaseModel;

class StatsInviteManage extends BaseModel
{
    use Singleton;
    protected $table = 'stats_invite_manage';
}