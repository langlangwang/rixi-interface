<?php
namespace app\common\models\Cart;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class MemberCart extends BaseModel
{
    use Singleton;

    protected $table = 'member_cart';
}