<?php


namespace app\common\models\Marketing;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class LotteryMemberAmountLog extends BaseModel
{
    use Singleton;
    protected $table = 'lottery_member_amount_log';
}