<?php


namespace app\common\models\Marketing;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class LotteryAward extends BaseModel
{
    use Singleton;
    protected $table = 'lottery_award';
}