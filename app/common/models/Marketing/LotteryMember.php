<?php


namespace app\common\models\Marketing;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class LotteryMember extends BaseModel
{
    use Singleton;
    protected $table = 'lottery_member';
}