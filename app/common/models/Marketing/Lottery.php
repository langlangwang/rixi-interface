<?php


namespace app\common\models\Marketing;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class Lottery extends BaseModel
{
    use Singleton;
    protected $table = 'lottery';
}