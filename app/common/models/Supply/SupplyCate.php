<?php

namespace app\common\models\Supply;

use app\common\libs\Singleton;
use app\common\models\BaseModel;

class SupplyCate extends BaseModel
{
    use Singleton;
    protected $table = 'supply_cate';
}