<?php
namespace app\common\models\Order;

use app\common\libs\Singleton;
use app\common\models\BaseModel;

class OrderRefund extends BaseModel
{
    use Singleton;
    protected $table = 'order_refund';

    // 设置json类型字段
    protected $json = ['attach'];

    public $typeArr = [
        ['value' => 10, 'text' => '退货退款'],
        ['value' => 20, 'text' => '退货不退款'],
        ['value' => 30, 'text' => '换货'],
    ];

    public $statusArr = [
        ['value' => 0, 'text' => '处理中'],
        ['value' => 1, 'text' => '已拒绝'],
        ['value' => 2, 'text' => '财务退款'],
        ['value' => 3, 'text' => '已完成'],
        ['value' => 4, 'text' => '已取消'],
    ];

}
