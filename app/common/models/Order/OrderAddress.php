<?php


namespace app\common\models\Order;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class OrderAddress extends BaseModel
{
    use Singleton;
    protected $table = 'order_address';
}