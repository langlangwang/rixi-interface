<?php


namespace app\common\models\Order;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class Postage extends BaseModel
{
    use Singleton;
    protected $table = 'goods_postage';
}