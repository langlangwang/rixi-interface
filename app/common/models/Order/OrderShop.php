<?php


namespace app\common\models\Order;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class OrderShop extends BaseModel
{
    use Singleton;
    protected $table = 'order_shop';
}