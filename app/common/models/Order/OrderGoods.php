<?php


namespace app\common\models\Order;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class OrderGoods extends BaseModel
{
    use Singleton;
    protected $table = 'order_goods';
}