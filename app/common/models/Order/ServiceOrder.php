<?php


namespace app\common\models\Order;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class ServiceOrder extends BaseModel
{
    use Singleton;
    protected $table = 'service_order';
}