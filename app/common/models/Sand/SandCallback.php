<?php

namespace app\common\models\Sand;

use app\common\libs\Singleton;
use app\common\models\BaseModel;

class SandCallback extends BaseModel
{
    use Singleton;
    protected $table = 'sand_callback';
}