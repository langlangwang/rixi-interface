<?php

namespace app\common\models\Sand;

use app\common\libs\Singleton;
use app\common\models\BaseModel;

class MsgCallback extends BaseModel
{
    use Singleton;
    protected $table = 'msg_callback';
}