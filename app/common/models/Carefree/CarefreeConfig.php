<?php


namespace app\common\models\Carefree;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class CarefreeConfig extends BaseModel
{
    use Singleton;
    protected $table = 'carefree_config';
}