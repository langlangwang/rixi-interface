<?php


namespace app\common\models\Carefree;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class CarefreeDeductionAmountLog extends BaseModel
{
    use Singleton;
    protected $table = 'carefree_deduction_amount_log';
}