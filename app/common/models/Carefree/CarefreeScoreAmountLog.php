<?php


namespace app\common\models\Carefree;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class CarefreeScoreAmountLog extends BaseModel
{
    use Singleton;
    protected $table = 'carefree_score_amount_log';
}