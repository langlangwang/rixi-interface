<?php


namespace app\common\models\Carefree;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class CarefreeGoodsParameter extends BaseModel
{
    use Singleton;
    protected $table = 'carefree_goods_parameter';
}