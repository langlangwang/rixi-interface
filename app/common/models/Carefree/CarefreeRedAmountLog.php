<?php


namespace app\common\models\Carefree;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class CarefreeRedAmountLog extends BaseModel
{
    use Singleton;
    protected $table = 'carefree_red_amount_log';
}