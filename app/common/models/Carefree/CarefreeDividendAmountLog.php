<?php


namespace app\common\models\Carefree;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class CarefreeDividendAmountLog extends BaseModel
{
    use Singleton;
    protected $table = 'carefree_dividend_amount_log';
}