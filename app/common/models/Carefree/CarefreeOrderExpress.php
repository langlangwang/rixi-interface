<?php


namespace app\common\models\Carefree;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class CarefreeOrderExpress extends BaseModel
{
    use Singleton;
    protected $table = 'carefree_order_express';
}