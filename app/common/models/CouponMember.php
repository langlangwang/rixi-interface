<?php
namespace app\common\models;

use app\common\libs\Singleton;
use app\common\models\BaseModel;

class CouponMember extends BaseModel
{
    use Singleton;
    protected $table = 'coupon2_member';
}
