<?php
namespace app\common\models\Schools;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class Schools extends BaseModel
{
    use Singleton;

    protected $table = 'schools';
}