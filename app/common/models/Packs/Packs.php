<?php


namespace app\common\models\Packs;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class Packs extends BaseModel
{
    use Singleton;
    protected $table = 'packs';

}