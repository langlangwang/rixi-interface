<?php

namespace app\common\models\Packs;

use app\common\libs\Singleton;
use app\common\models\BaseModel;

class PacksBusinessUser extends BaseModel
{
    use Singleton;

    protected $table = 'packs_business_user';
}