<?php


namespace app\common\models\Packs;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class PacksOrderExpress extends BaseModel
{
    use Singleton;
    protected $table = 'packs_order_express';
}