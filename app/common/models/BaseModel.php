<?php

namespace app\common\models;

use think\Model;

/**
 * @method Model inc(string $field, double $stop)
 * @method Model dec(string $field, double $stop)
 * @method Model find($data = null)
 * @method Model leftJoin(string $table, string $where)
 * @method array column(string $field, string $key = '')
 * Class BaseModel
 * @package app\common\models
 */
class BaseModel extends Model
{
    protected $convertNameToCamel = true;
}