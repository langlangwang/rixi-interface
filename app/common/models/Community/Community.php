<?php


namespace app\common\models\Community;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class Community extends BaseModel
{
    use Singleton;
    protected $table = 'community';
}