<?php


namespace app\common\models\Community;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class PrepayCommunityOrder extends BaseModel
{
    use Singleton;
    protected $table = 'prepay_community_order';
}