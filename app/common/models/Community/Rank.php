<?php


namespace app\common\models\Community;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class Rank extends BaseModel
{
    use Singleton;
    protected $table = 'rank';
}