<?php

namespace app\common\models\Community;

use app\common\libs\Singleton;
use app\common\models\BaseModel;

class CommunityTaskLog extends BaseModel
{
    use Singleton;
    protected $table = 'community_task_log';
}