<?php


namespace app\common\models\Community;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class CommunityConfig extends BaseModel
{
    use Singleton;
    protected $table = 'community_config';
}