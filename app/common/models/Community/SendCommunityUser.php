<?php


namespace app\common\models\Community;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class SendCommunityUser extends BaseModel
{
    use Singleton;
    protected $table = 'send_community_user';
}