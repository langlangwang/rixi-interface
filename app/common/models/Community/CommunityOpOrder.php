<?php


namespace app\common\models\Community;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class CommunityOpOrder extends BaseModel
{
    use Singleton;
    protected $table = 'community_op_order';
}