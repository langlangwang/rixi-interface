<?php


namespace app\common\models\Community;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class OperateActivityData extends BaseModel
{
    use Singleton;
    protected $table = 'operate_activity_data';
}