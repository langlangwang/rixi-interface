<?php


namespace app\common\models\Community;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class RankData extends BaseModel
{
    use Singleton;
    protected $table = 'rank_data';
}