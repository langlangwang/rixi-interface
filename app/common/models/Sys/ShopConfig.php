<?php

namespace app\common\models\Sys;

use app\common\libs\Singleton;
use app\common\models\BaseModel;

class ShopConfig extends BaseModel
{
    use Singleton;
    protected $table = 'shop_config';
}