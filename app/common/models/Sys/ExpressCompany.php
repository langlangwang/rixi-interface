<?php
namespace app\common\models\Sys;

use app\common\libs\Singleton;
use app\common\models\BaseModel;

class ExpressCompany extends BaseModel
{
    use Singleton;
    protected $table = 'express_company';

    public function recommended($type = '国内运输商')
    {
        return $this->field('express_code,express_name')
            ->where('express_type', $type)
            ->where('recommended', 1)
            ->select()
            ->toArray();
    }
}
