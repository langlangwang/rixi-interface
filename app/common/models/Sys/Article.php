<?php


namespace app\common\models\Sys;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class Article extends BaseModel
{
    use Singleton;
    protected $table = 'article';
}