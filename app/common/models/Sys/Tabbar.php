<?php


namespace app\common\models\Sys;


use app\common\libs\Singleton;
use app\common\models\BaseModel;

class Tabbar extends BaseModel
{
    use Singleton;
    protected $table = 'tabbar';
}