<?php


namespace app\api\controller;

use app\api\logic\FriendLogic;
use app\common\controller\RestController;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class Friend extends RestController
{
    protected $noNeedLogin = [];

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function friendShopInfo(): bool
    {
        $userId = input('userId', 0);
        $info = FriendLogic::getInstance()->friendShopInfo($userId);
        return $this->result($info);
    }


    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function friendShopList(): bool
    {
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info = FriendLogic::getInstance()->friendShopList($page, $pageSize);
        return $this->result($info);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function friendList(): bool
    {
        $type = input('type', 1);
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);

        $info = FriendLogic::getInstance()->friendList($type, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function activityList(): bool
    {
        $type = input('type', 1);
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);

        $info = FriendLogic::getInstance()->activityList($type, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function friendInfo(): bool
    {
        $type = input('type', 1);
        $userId = input('userId', 0);
        $info = FriendLogic::getInstance()->friendInfo($userId, $type);
        return $this->result($info);
    }

}