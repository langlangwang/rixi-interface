<?php


namespace app\api\controller;

use app\api\logic\CarefreeGoodsLogic;
use app\api\logic\GoodsLogic;
use app\common\controller\RestController;

class CarefreeGoods extends RestController
{
    protected $noNeedLogin = ['goodsDetail'];

    public function goodsDetail(): bool
    {
        $goodsId = input('goodsId',0);
        $info = CarefreeGoodsLogic::getInstance()->goodsDetail($goodsId);
        return $this->result($info);
    }
    /**
     * @return bool
     */
    public function goodsList(): bool
    {
        $page = input('page',1);
        $pageSize = input('pageSize',10);
        $sort = input('sort',0);
        $keyword = input('keyword','');
        $categoryId = input('categoryId',0);
        $info = CarefreeGoodsLogic::getInstance()->goodsList($page,$pageSize,$sort,$keyword,$categoryId);
        return $this->result($info);
    }
}