<?php
namespace app\api\controller;

use app\api\logic\SysLogic;
use app\common\controller\RestController;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class Sys extends RestController
{
    protected $noNeedLogin = ['*'];

    /**
     * APP公用配置接口
     * @return [type] [description]
     */
    public function config(): bool
    {
        return $this->result([
            //'tabMenu' => \app\api\cache\SysCache::tabMenu(),
            'tabMenu' => SysLogic::getInstance()->tabMenu(),
        ]);
    }

    /**
     * @return bool
     */
    public function imageList(): bool
    {
        $return = [
            'img1' => 'https://files.jiubaibei.com/202205/11101036-80401101611544741.png',
            'img2' => 'https://files.jiubaibei.com/202205/11101053-84591910588246539.png',
            'img3' => 'https://files.jiubaibei.com/202205/11101100-79909355652263176.png',
            'img4' => 'https://files.jiubaibei.com/202205/11101108-99953615408211085.png',
        ];
        return $this->result($return);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function helpDetail(): bool
    {
        $helpId = input('helpId', 0);
        $info   = SysLogic::getInstance()->helpDetail($helpId);
        return $this->result($info);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function helpList(): bool
    {
        $keyword = input('keyword', '');
        $info    = SysLogic::getInstance()->helpList($keyword);
        return $this->result($info);
    }

    public function helpType(): bool
    {
        $info = SysLogic::getInstance()->helpType();
        return $this->result($info);
    }

    public function feedback(): bool
    {
        $type    = input('type', 1);
        $content = input('content', '');
        $imgList = input('imgList', '');
        $info    = SysLogic::getInstance()->feedback($type, $content, $imgList);
        return $this->result($info);
    }

    public function qa(): bool
    {
        $keyword = input('keyword', '');
        $info    = SysLogic::getInstance()->qa($keyword);
        return $this->result($info);
    }

    /**
     * 文章列表
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function noticeList(): bool
    {
        $keyword  = input('keyword', '');
        $page     = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info     = SysLogic::getInstance()->noticeList($keyword, $page, $pageSize);
        return $this->result($info);
    }

    public function noticeDetail(): bool
    {
        $noticeId = input('noticeId', 0);
        $info     = SysLogic::getInstance()->noticeDetail($noticeId);
        return $this->result($info);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function article(): bool
    {
        $articleId = input('articleId', 0);
        $info      = SysLogic::getInstance()->article($articleId);
        return $this->result($info);
    }

    public function getArticle(): bool
    {
        $key  = input('key', 0);
        $info = SysLogic::getInstance()->getArticle($key);
        return $this->result($info);
    }

    public function getCityByLongLat(): bool
    {
        $lon  = input('longitude', 0);
        $lat  = input('latitude', 0);
        $info = SysLogic::getInstance()->getCityByLongLat($lon, $lat);
        return $this->result($info);
    }

    public function advImg(): bool
    {
        $type = input('type', 1);
        $info = SysLogic::getInstance()->advImg($type);
        return $this->result($info);
    }

    public function areaCity(): bool
    {
        $page     = input('page', 1);
        $pageSize = input('pageSize', 20);
        $letter   = input('letter', '');
        $name     = input('name', '');
        $info     = SysLogic::getInstance()->areaCity($page, $pageSize, $letter, $name);
        return $this->result($info);
    }

    /**
     * 省市区县
     * @return bool
     */
    public function area(): bool
    {
        $pId  = input('pId', 0);
        $info = SysLogic::getInstance()->area($pId);
        return $this->result($info);
    }

    public function notice(): bool
    {
        $info = SysLogic::getInstance()->notice();
        return $this->result($info);
    }

    /**
     * 首页banner
     * @return bool
     */
    public function adv()
    {
        $info = SysLogic::getInstance()->adv();
        return $this->result($info);
    }

    /**
     * 首页banner
     * @return bool
     */
    public function nav()
    {
        $info = SysLogic::getInstance()->nav();
        return $this->result($info);
    }

    public function tool(): bool
    {
        $info = SysLogic::getInstance()->tool();
        return $this->result($info);
    }

    /**
     * 平台设置
     * @return [type] [description]
     */
    public function platformConf()
    {
        $conf = sysconf('platform_conf');
        $conf = html_entity_decode($conf, ENT_QUOTES, 'UTF-8');
        $conf = json_decode($conf, true);
        // var_dump($conf);exit;
        return $this->result($conf);
    }
}
