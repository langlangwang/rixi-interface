<?php

namespace app\api\controller;

use app\api\logic\PacksLogic;
use app\common\controller\RestController;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class Packs extends RestController
{
    protected $noNeedLogin = ['getPacksInfo'];


    /**
     * 礼包详情
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function getPacksInfo(): bool
    {
        $id = input('id', '0');
        $info = PacksLogic::getInstance()->getPacksInfo($id);
        return $this->result($info);
    }

    /**
     * 社区列表
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function packsList(): bool
    {
        $name = input('name', '');
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info = PacksLogic::getInstance()->packsList($name, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * 生成订单
     * @throws ModelNotFoundException
     * @throws DbException
     * @throws DataNotFoundException
     */
    public function createPacksOrder(): bool
    {
        $id = input('id', 0);
        $addressId = input('addressId', 0);
        $packsType = input('packsType', '');
        $productsContents = input('productsContents', []);
        $info = PacksLogic::getInstance()->createPacksOrder($id, $addressId, $packsType ,$productsContents);
        return $this->result($info);
    }

    /**
     * 支付订单
     * @throws ModelNotFoundException
     * @throws DbException
     * @throws DataNotFoundException
     */
    public function payPacksOrder(): bool
    {
        $orderNo = input('orderNo', 0);
        $payType = input('payType', 0);
        $payPassword = input('payPassword', 0);
        $isBalance = input('isBalance', 0);
        $paySubType = input('paySubType','');
        $info = PacksLogic::getInstance()->payPacksOrder($orderNo, $payType, $payPassword,$paySubType);
        return $this->result($info);
    }


    /**
     * 订单列表
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function packsOrderList(): bool
    {
        $status = input('type', '');
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info = PacksLogic::getInstance()->getPacksOrderList($status, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * 订单详情
     * @throws ModelNotFoundException
     * @throws DbException
     * @throws DataNotFoundException
     */
    public function getPacksOrderDetail(): bool
    {
        $orderId = input('orderId', 0);
        $info = PacksLogic::getInstance()->getPacksOrderDetail($orderId);
        return $this->result($info);
    }

    /**
     * 取消订单
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function packsOrderCancel(): bool
    {
        $orderId = input('orderId', 0);
        $info = PacksLogic::getInstance()->packsOrderCancel($orderId);
        return $this->result($info);
    }

    /**
     * 取消订单
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function packsOrderDel(): bool
    {
        $orderId = input('orderId', 0);
        $info = PacksLogic::getInstance()->packsOrderDel($orderId);
        return $this->result($info);
    }

    /**
     * 查看物流
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function packsOrderExpress(): bool
    {
        $orderId = input('orderId', 0);
        $info = PacksLogic::getInstance()->packsOrderExpress($orderId);
        return $this->result($info);
    }

    /**
     * 确认收货
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function packsOrderConfirm(): bool
    {
        $orderId = input('orderId', 0);
        $info = PacksLogic::getInstance()->packsOrderConfirm($orderId);
        return $this->result($info);
    }

    /**
     * 升级
     * @return true
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function packsUpgrade(): bool
    {
        $orderId = input('orderId', 0);
        $info = PacksLogic::getInstance()->packsUpgrade($orderId);
        return $this->result($info);
    }

    public function packsUpgradeInfo(): bool
    {
        $info = PacksLogic::getInstance()->packsUpgradeInfo();
        return $this->result($info);
    }
}