<?php


namespace app\api\controller;

use app\api\logic\CollectLogic;
use app\common\controller\RestController;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class Collect extends RestController
{
    protected $noNeedLogin = [''];

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function collectList(): bool
    {
        $type = input('type', 1);
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $lat = input('lat', 0);
        $lng = input('lng', 0);
        $info = CollectLogic::getInstance()->collectList($type, $page, $pageSize, $lat, $lng);
        return $this->result($info);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function addCollect(): bool
    {
        $type = input('type', 1);
        $collectId = input('collectId', '');
        $info = CollectLogic::getInstance()->addCollect($type, $collectId);
        return $this->result($info);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function delCollect(): bool
    {
        $type = input('type', 1);
        $collectId = input('collectId', 0);
        $info = CollectLogic::getInstance()->delCollect($type, $collectId);
        return $this->result($info);
    }
}