<?php


namespace app\api\controller;

use app\api\logic\LoginLogic;
use app\api\logic\MemberLogic;
use app\api\services\MemberService;
use app\common\controller\RestController;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class Member extends RestController
{
    protected $noNeedLogin = ['testLogin', 'login', 'find'];

    public function invitePoster(): bool
    {
        $info = MemberLogic::getInstance()->invitePoster();
        return $this->result($info);
    }

    public function inviteShop(): bool
    {
        $info = MemberLogic::getInstance()->inviteShop();
        return $this->result($info);
    }

    public function bindAddress(): bool
    {
        $provinceId = input('provinceId', '');
        $inviteCode = input('inviteCode', '');
        $cityId = input('cityId', '');
        $countryId = input('countryId', '');
        $streetId = input('streetId', '');
        $commId = input('commId', '');
        $return = LoginLogic::getInstance()->bindAddress($inviteCode, $provinceId, $cityId, $countryId, $streetId, $commId);
        return $this->result($return);
    }

    public function addWithdraw(): bool
    {
        $code = input('code', '');
        $price = input('price', 0);
        $info = MemberLogic::getInstance()->addWithdraw($code, $price);
        return $this->result($info);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function withdrawInfo(): bool
    {
        $info = MemberLogic::getInstance()->withdrawInfo();
        return $this->result($info);
    }

    public function memberAmountLogList(): bool
    {
        $startAt = input('startAt', '');
        $endAt = input('endAt', '');
        $logType = input('logType', '');
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info = MemberLogic::getInstance()->memberAmountLogList($startAt, $endAt, $logType, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * 绿色积分列表
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function getAmountLog(): bool
    {
        $startAt = input('startAt', '');
        $endAt = input('endAt', '');
        $logType = input('logType', '');
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $type = input('type', 0);
        $info = MemberLogic::getInstance()->getAmountLog($startAt, $endAt, $logType, $page, $pageSize, $type);
        return $this->result($info);
    }

    /**
     * 绿色积分分类
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function getAmountTypeList(): bool
    {
        $info = MemberLogic::getInstance()->getAmountTypeList();
        return $this->result($info);
    }


    /**
     * @return bool
     */
    public function defaultAddress(): bool
    {
        $addressId = input('addressId', 0);
        $addressType = input('addressType', 0);
        $info = MemberLogic::getInstance()->defaultAddress($addressId, $addressType);
        return $this->result($info);
    }

    /**
     * 删除地址
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function delAddress(): bool
    {
        $addressId = input('addressId', 1);
        $info = MemberLogic::getInstance()->delAddress($addressId);
        return $this->result($info);
    }

    /**
     * 修改地址
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function editAddress(): bool
    {
        $this->getData($data);
        $addressId = input('addressId', 1);
        $info = MemberLogic::getInstance()->editAddress($data, $addressId);
        return $this->result($info);
    }

    /**
     * 添加地址
     * @return bool
     */
    public function addAddress(): bool
    {
        $this->getData($data);
        $info = MemberLogic::getInstance()->addAddress($data);
        return $this->result($info);
    }

    /**
     * 地址详情
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function addressDetail(): bool
    {
        $addressId = input('addressId', 1);
        $info = MemberLogic::getInstance()->addressDetail($addressId);
        return $this->result($info);
    }

    /**
     * 地址列表
     * @return bool
     */
    public function addressList(): bool
    {
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info = MemberLogic::getInstance()->addressList($page, $pageSize);
        return $this->result($info);
    }

    /**
     * @return bool
     */
    public function testLogin(): bool
    {
        $info = MemberLogic::getInstance()->testLogin();
        return $this->result($info);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function userInfo(): bool
    {
        $info = MemberLogic::getInstance()->userInfo();
        return $this->result($info);
    }

    /**
     * 修改用户信息
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function editAccount(): bool
    {
        $editVal = input('editVal', '');
        $editValDouble = input('editValDouble', '');
        $editKey = input('editKey', '');
        $code = input('code', '');
        $return = MemberLogic::getInstance()->editAccount($editKey, $editVal, $editValDouble, $code);
        return $this->result($return);
    }

    /**
     * 解绑支付宝
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function unbindAli(): bool
    {
        $code = input('code', '');
        $return = MemberLogic::getInstance()->unbindAli($code);
        return $this->result($return);
    }

    /**
     * 修改用户基础信息
     * @return bool
     */
    public function editVal(): bool
    {
        $editVal = input('editVal', '');
        $editKey = input('editKey', '');
        $return = MemberLogic::getInstance()->editVal($editKey, $editVal);
        return $this->result($return);
    }

    /**
     * 找回密码
     */
    public function find(): bool
    {
        $phone = input('phone', '');
        $code = input('code', '');
        $password = input('password', '');
        return $this->result(MemberLogic::getInstance()->find($phone, $code, $password));
    }

    /**
     * @param $data
     */
    public function getData(&$data): void
    {
        $data['real_name'] = input('realName', '');
        $data['address'] = input('address', '');
        $data['phone'] = input('phone', '');
        $data['city_id'] = input('cityId', 0);
        $data['province_id'] = input('provinceId', 0);
        $data['is_default'] = input('isDefault', 0);
        $data['province'] = input('province', '');
        $data['city'] = input('city', '');
        $data['comm'] = input('comm', '');
        $data['comm_id'] = input('commId', 0);
        $data['country'] = input('country', '');
        $data['country_id'] = input('countryId', 0);
        $data['street'] = input('street', '');
        $data['street_id'] = input('streetId', 0);
        $data['address_type'] = input('addressType', 0);
    }

    /**
     * 余额提现列表
     * @return bool
     */
    public function withdrawList(): bool
    {
        $type = input('type', 1);
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info = MemberLogic::getInstance()->withdrawList($type, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function signUserList(): bool
    {
        $status = input('status');
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info = MemberLogic::getInstance()->signUserList($status, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * 数字资产 、积分列表
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function getDigitalLog(): bool
    {
        $startAt = input('startAt', '');
        $endAt = input('endAt', '');
        $logType = input('logType', '');
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $type = input('type', 0);
        $info = MemberLogic::getInstance()->getDigitalLog($startAt, $endAt, $logType, $page, $pageSize, $type);
        return $this->result($info);
    }

    /**
     * 贡献值列表
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function getGxzLog(): bool
    {
        $startAt = input('startAt', '');
        $endAt = input('endAt', '');
        $logType = input('logType', '');
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info = MemberLogic::getInstance()->getGxzLog($startAt, $endAt, $logType, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function rechargeMoneyList(): bool
    {
        $info = MemberLogic::getInstance()->getMemberRechargeMoneyList();
        return $this->result($info);
    }

    /**
     * 充值余额下单
     * @return bool
     */
    public function rechargeCreateOrder(): bool
    {
        $money = input('money', 0);
        $info = MemberLogic::getInstance()->rechargeCreateOrder($money);
        return $this->result($info);
    }


    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function getTaskGxz(): bool
    {
        $type = input('type', 1);
        $info = MemberLogic::getInstance()->getTaskGxz($type);
        return $this->result($info);
    }

    /**
     * @throws DbException
     */
    public function getTaskInfo(): bool
    {
        $info = MemberLogic::getInstance()->getTaskInfo();
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function signIn(): bool
    {
        $info = MemberLogic::getInstance()->signIn();
        return $this->result($info);
    }

    /**
     * 分红倒计时
     * @return bool
     * @throws DbException
     */
    public function bonusTime(): bool
    {
        $info = MemberLogic::getInstance()->bonusTime();
        return $this->result($info);
    }

    /**
     * 手动领取分红
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function getProfit(): bool
    {
        $info = MemberLogic::getInstance()->getProfit();
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function deductionLog(): bool
    {
        $startAt = input('startAt', '');
        $endAt = input('endAt', '');
        $logType = input('logType', '');
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info = MemberLogic::getInstance()->deductionLog($startAt, $endAt, $logType, $page, $pageSize);
        return $this->result($info);
    }

    public function userDisabled(): bool
    {
        $info = MemberLogic::getInstance()->userDisabled();
        return $this->result($info);
    }

}