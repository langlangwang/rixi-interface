<?php


namespace app\api\controller;

use app\api\logic\CarefreeInfoLogic;
use app\common\controller\RestController;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class CarefreeInfo extends RestController
{
    protected $noNeedLogin = [];

    public function info(): bool
    {
        $info = CarefreeInfoLogic::getInstance()->info();
        return $this->result($info);
    }

    public function scoreLog(): bool
    {
        $startAt = input('startAt', '');
        $endAt = input('endAt', '');
        $logType = input('logType', '');
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info = CarefreeInfoLogic::getInstance()->scoreLog($startAt, $endAt, $logType, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function redLog(): bool
    {
        $startAt = input('startAt', '');
        $endAt = input('endAt', '');
        $logType = input('logType', '');
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info = CarefreeInfoLogic::getInstance()->redLog($startAt, $endAt, $logType, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function dividendLog(): bool
    {
        $startAt = input('startAt', '');
        $endAt = input('endAt', '');
        $logType = input('logType', '');
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info = CarefreeInfoLogic::getInstance()->dividendLog($startAt, $endAt, $logType, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function speedLog(): bool
    {
        $startAt = input('startAt', '');
        $endAt = input('endAt', '');
        $logType = input('logType', '');
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info = CarefreeInfoLogic::getInstance()->speedLog($startAt, $endAt, $logType, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function deductionLog(): bool
    {
        $startAt = input('startAt', '');
        $endAt = input('endAt', '');
        $logType = input('logType', '');
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info = CarefreeInfoLogic::getInstance()->deductionLog($startAt, $endAt, $logType, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function withdraw(): bool
    {
        $payPassword = input('payPassword', 0);
        $amount      = input('amount', 0);
        $type        = input('type', 0);
        $info        = CarefreeInfoLogic::getInstance()->withdraw($payPassword, $amount, $type);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DbException
     * @throws DataNotFoundException
     */
    public function withdrawList($type, $page, $pageSize): bool
    {
        $type        = input('type', 0);
        $page        = input('page', 1);
        $pageSize    = input('pageSize', 10);
        $info        = CarefreeInfoLogic::getInstance()->withdrawList($type, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function withdrawInfo(): bool
    {
        $type = input('type', 0);
        $info = CarefreeInfoLogic::getInstance()->withdrawInfo($type);
        return $this->result($info);
    }

    public function switch(): bool
    {
        $info = CarefreeInfoLogic::getInstance()->switch();
        return $this->result($info);
    }

    public function isShowPool(): bool
    {
        $info = CarefreeInfoLogic::getInstance()->isShowPool();
        return $this->result($info);
    }

    public function pool(): bool
    {
        $info = CarefreeInfoLogic::getInstance()->pool();
        return $this->result($info);
    }

}