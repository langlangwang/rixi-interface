<?php
namespace app\api\controller;

use app\common\controller\RestController;
use Exception;
use Qiniu\Auth;
use Qiniu\Storage\UploadManager;

class Upload extends RestController
{
    protected $noNeedLogin = ['*'];

    /**
     * @return bool
     * @throws Exception
     */
    public function index(): bool
    {
        $file = request()->file('file');
        if ($file == null) {
            return $this->result('上传失败');
        }
        $temp      = explode(".", $_FILES["file"]["name"]);
        $extension = end($temp);

        // video/x-msvideo - 通常与 AVI 文件相关，文件后缀是 .avi。
        // video/x-ms-wmv - 与 Windows Media Video 文件相关，文件后缀通常是 .wmv。
        // video/quicktime - 与 QuickTime 文件相关，文件后缀可以是 .mov 或 .qt。
        // video/x-flv - 与 Flash Video 文件相关，文件后缀是 .flv。
        // video/3gpp - 与 3GPP 多媒体容器格式相关，文件后缀是 .3gp 或 .3gpp。
        // video/x-matroska - 与 Matroska 视频格式相关，文件后缀是 .mkv。
        // video/webm - 与 WebM 视频格式相关，文件后缀是 .webm。
        // video/h264 - H.264 视频编码通常被用在多种容器格式中，它可以有多个文件后缀，如 .mp4、.m4v 或 .mov。

        $extArr = [
            "jpeg", "jpg", "png", "bmp", "gif", "webp", "avif","JPG","JPEG",
            'mp4', 'avi', 'wmv', 'mov', 'qt', 'flv', '3gp', '3gpp', 'mkv', 'webm', 'm4v', 'mov',
        ];
        $mimeArr = [
            // image/jpeg ：JPEG 图像，常用于照片。
            // image/png ：PNG 图像，支持无损压缩和透明度。
            // image/gif ：GIF 图像，支持动画和简单透明度。
            // image/bmp ：位图图像，一种较老的格式。
            // image/webp ：WebP 图像，由 Google 开发，支持无损和有损压缩。
            // image/avif ：AVIF 图像，基于 AV1 编码的图像格式，支持无损和有损压缩。
            'image/jpeg', 'image/png', 'image/gif', 'image/bmp', 'image/webp', 'image/avif',
            // video/mp4 ：MPEG-4 视频，广泛用于视频分享和流媒体。
            // video/x-msvideo ：AVI 视频，一种较老的视频格式。
            // video/x-ms-wmv ：Windows Media Video，微软的视频格式。
            // video/quicktime ：QuickTime 视频，苹果公司的视频格式。
            // video/x-flv ：Flash Video，Adobe Flash 的视频格式。
            // video/3gpp ：3GPP 视频，常用于移动设备。
            // video/x-matroska ：MKV 视频，一种开放标准的自由视频格式。
            // video/webm ：WebM 视频，基于 VP8 或 VP9 编码的开源视频格式。
            // video/h264 ：H.264 视频编码，广泛用于视频压缩和流媒体。
            'video/mp4',
            'video/x-msvideo',
            'video/x-ms-wmv',
            'video/quicktime',
            'video/x-flv',
            'video/3gpp',
            'video/x-matroska',
            'video/webm',
            'video/h264',
        ];
        validate(['imgFile' => [
            'fileSize' => 410241024,
            'fileExt'  => implode(',', $extArr),
            'fileMime' => implode(',', $mimeArr),
        ]])->check(['imgFile' => $file]);

        if ( ! in_array($extension, $extArr)) {
            return $this->result('请正确上传图片');
        }
        $accessKey         = config('qiniu.accessKey');
        $secretKey         = config('qiniu.secretKey');
        $auth              = new Auth($accessKey, $secretKey);
        $bucket            = config('qiniu.bucket');
        $uploadManager     = new UploadManager();
        $newFileName       = random(15) . time() . "." . $extension;
        $filePath          = $file->getRealPath();
        $token             = $auth->uploadToken($bucket);
        list($ret, $error) = $uploadManager->putFile($token, date('Ymd') . '/' . $newFileName, $filePath);
        if ($error !== null) {
            return $this->result('上传失败');
        }
        return $this->result(['url' => config('qiniu.url') . $ret['key']]);
    }
}
