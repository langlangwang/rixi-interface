<?php


namespace app\api\controller;

use app\api\logic\ServiceTeamLogic;
use app\common\controller\RestController;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class ServiceTeam extends RestController
{
    protected $noNeedLogin = [''];

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function payOrder(): bool
    {
        $orderNo = input('orderNo', '');
        $payType = input('payType', 1);
        $couponAmount = input('couponAmount', 0);
        $info = ServiceTeamLogic::getInstance()->payOrder($orderNo, $payType, $couponAmount);
        return $this->result($info);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function orderInfo(): bool
    {
        $orderNo = input('orderNo', '');
        $info = ServiceTeamLogic::getInstance()->orderInfo($orderNo);
        return $this->result($info);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function createOrder(): bool
    {
        $info = ServiceTeamLogic::getInstance()->createOrder();
        return $this->result($info);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function serviceInfo(): bool
    {
        $info = ServiceTeamLogic::getInstance()->serviceInfo();
        return $this->result($info);
    }
}