<?php
namespace app\api\controller;

use app\api\logic\CommunityLogic;
use app\api\logic\MemberLogic;
use app\common\controller\AgencyController;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class AgencyInfo extends AgencyController
{
    protected $noNeedLogin = [];

    /**
     * 首页
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function getAmountTypeList(): bool
    {
        $info = MemberLogic::getInstance()->getAmountTypeList(1);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function getAmountLog(): bool
    {
        $startAt = input('startAt', '');
        $endAt = input('endAt', '');
        $logType = input('logType', '');
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $type = input('type', 0);
        $info = MemberLogic::getInstance()->getAmountLog($startAt, $endAt, $logType, $page, $pageSize, $type);
        return $this->result($info);
    }


    /**
     * 收益信息
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function withdrawInfo(): bool
    {
        $type = input('type', 0);
        $info = CommunityLogic::getInstance()->withdrawInfo($type);
        return $this->result($info);
    }

    /**
     * 积分兑换余额
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function withdrawCommunity(): bool
    {
        $payPassword = input('payPassword', 0);
        $amount      = input('amount', 0);
        $type        = input('type', 0);
        $info        = CommunityLogic::getInstance()->withdrawCommunity($payPassword, $amount, $type);
        return $this->result($info);
    }

    /**
     * 积分兑换余额记录
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function withdrawList(): bool
    {
        $type     = input('type', 0);
        $page     = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info     = CommunityLogic::getInstance()->withdrawList($type, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * 余额提现
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function withdraw(): bool
    {
        $payPassword = input('payPassword', 0);
        $amount      = input('amount', 0);
        $type        = input('type', 0);
        $info        = CommunityLogic::getInstance()->withdraw($payPassword, $amount, $type);
        return $this->result($info);
    }

    /**
     * 报表
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function report(): bool
    {
        $startAt = input('startAt');
        $endAt = input('endAt');
        $info        = CommunityLogic::getInstance()->report($startAt, $endAt);
        return $this->result($info);
    }

}
