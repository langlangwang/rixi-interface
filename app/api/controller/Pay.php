<?php


namespace app\api\controller;

use app\api\logic\OrderLogic;
use app\api\logic\PayLogic;
use app\common\controller\RestController;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class Pay extends RestController
{
    protected $noNeedLogin = [''];

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function payOrder(): bool
    {
        $orderNo = input('orderNo', 0);
        $payType = input('payType', 1);
        $payPassword = input('payPassword', 0);
        $paySubType = input('paySubType', 0);
        $info = PayLogic::getInstance()->payOrder($orderNo, $payType, $payPassword,$paySubType);
        return $this->result($info);
    }
}