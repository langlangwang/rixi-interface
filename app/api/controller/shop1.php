<?php


namespace app\api\controller;

use app\api\logic\ShopLogic;
use app\common\controller\RestController;

class Shop1 extends RestController
{
    protected $noNeedLogin = ['*'];

    public function searchHistory(): bool
    {
        $info = ShopLogic::getInstance()->searchHistory();
        return $this->result($info);
    }




    /**
     * 商品详情
     * @return bool
     */
    public function goodsDetail(): bool
    {
        $goodsId = input('goodsId', 1);
        $info = ShopLogic::getInstance()->goodsDetail($goodsId);
        return $this->result($info);
    }

    /**
     * 提交评论
     * @return bool
     */
    public function addComment(): bool
    {
        $content = input('content', '');
        $goodsId = input('goodsId', 0);
        $info = ShopLogic::getInstance()->addComment($content, $goodsId);
        return $this->result($info);
    }

    /**
     * 商品评论
     * @return bool
     */
    public function goodsComment(): bool
    {
        $goodsId = input('goodsId', 0);
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info = ShopLogic::getInstance()->goodsComment($goodsId, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * 商品列表
     * @return bool
     */
    public function goodsList(): bool
    {
        $pcate = input('pcate', 0);
        $ccate = input('ccate', 0);
        $tcate = input('tcate', 0);
        $page = input('page', 1);
        $title = input('title', '');
        $pageSize = input('pageSize', 10);
        $info = ShopLogic::getInstance()->goodsList($pcate, $ccate, $tcate, $title, $page, $pageSize);
        return $this->result($info);
    }

    public function shopdetail()
    {
        $shopId = input('shopId',0);
        $info = ShopLogic::getInstance()->shopdetail($shopId);
        return $this->result($info);
    }

    /**
     * 商品分类列表
     * @return bool
     */
    public function categoryList(): bool
    {
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info = ShopLogic::getInstance()->categoryList($page, $pageSize);
        return $this->result($info);
    }

    /**
     * 门店列表
     * @return bool
     */
    public function shopList(): bool
    {
        $lat = input('lat', 0);
        $lng = input('lng', 0);
        $city = input('city', '杭州市');
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $recommend = input('recommend',0);
        $hot = input('hot',0);
        $groupId = input('groupId',0);
        $info = ShopLogic::getInstance()->shopList($lat, $lng, $city, $page, $pageSize,$recommend,$hot,$groupId);
        return $this->result($info);
    }

}