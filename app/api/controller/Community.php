<?php
namespace app\api\controller;

use app\api\logic\CommunityLogic;
use app\api\services\CommunityService;
use app\common\controller\RestController;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class Community extends RestController
{
    protected $noNeedLogin = [
        'click',
    ];

    /**
     * 仅为 社区点击竞拍数
     * @return [type] [description]
     */
    public function click()
    {
        return $this->result([]);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function communityHistoryList(): bool
    {
        $page     = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info     = CommunityLogic::getInstance()->communityHistoryList($page, $pageSize);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function inPayOrder(): bool
    {
        $info = CommunityLogic::getInstance()->inPayOrder();
        return $this->result($info);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function communityUserLog(): bool
    {
        $page     = input('page', 1);
        $pageSize = input('pageSize', 10);
        $type     = input('type', 0);
        $status   = input('status', 0);
        $info     = CommunityLogic::getInstance()->communityUserLog($status, $type, $page, $pageSize);
        return $this->result($info);
    }

    public function rank(): bool
    {
        $page     = input('page', 1);
        $pageSize = input('pageSize', 10);
        $type     = input('type', 0);
        $timeType = input('timeType', 0);
        $info     = CommunityLogic::getInstance()->rank($type, $timeType, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function CommunityRank(): bool
    {
        $page     = input('page', 1);
        $pageSize = input('pageSize', 10);
        $timeType = input('timeType', 0);
        $info     = CommunityLogic::getInstance()->CommunityRank($timeType, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function paySign(): bool
    {
        $orderNo   = input('orderNo', 0);
        $isBalance = input('isBalance', 0);
        $info      = CommunityLogic::getInstance()->paySign($orderNo, $isBalance);
        return $this->result($info);
    }

    /**
     * @return bool
     */
    public function sign(): bool
    {
        $info = CommunityLogic::getInstance()->sign();
        return $this->result($info);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function signInfo(): bool
    {
        $info = CommunityLogic::getInstance()->signInfo();
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function myCommunityList(): bool
    {
        $page     = input('page', 1);
        $pageSize = input('pageSize', 10);
        $status   = input('status', 0);
        $info     = CommunityLogic::getInstance()->myCommunityList($page, $pageSize, $status);
        return $this->result($info);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function payOrder(): bool
    {
        $orderNo     = input('orderNo', 0);
        $type        = input('type', 0);
        $isBag       = input('isBag', 0);
        $payPassword = input('payPassword', 0);
        $paySubType  = input('paySubType', 0);
        $info        = CommunityLogic::getInstance()->payOrder($orderNo, $isBag, $type, $payPassword, $paySubType);
        return $this->result($info);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function createCommunityOrder(): bool
    {
        $communityId = input('communityId', 0);
        $image       = input('image','');
        $signImage   = input('signImage','');
        $info        = CommunityLogic::getInstance()->createCommunityOrder($communityId,$image,$signImage);
        return $this->result($info);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function communityOrderPreview(): bool
    {
        $communityId = input('communityId', 0);
        $info        = CommunityLogic::getInstance()->communityOrderPreview($communityId);
        return $this->result($info);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function communityPayList(): bool
    {
        $page     = input('page', 1);
        $pageSize = input('pageSize', 10);
        $type     = input('type', 0);
        $info     = CommunityLogic::getInstance()->communityPayList($page, $pageSize, $type);
        return $this->result($info);
    }

    public function meetPayList(): bool
    {
        $page     = input('page', 1);
        $pageSize = input('pageSize', 10);
        //$type = input('type', 0);
        $password = input('password', 0);
        $info     = CommunityLogic::getInstance()->meetPayList($page, $pageSize, $password);
        return $this->result($info);
    }

    public function communityInfo(): bool
    {
        $url = 'https://jbm.qlroad.com/upload/94/306a6b546a0f3104e6dac75e059517.png';
        return $this->result(['url' => $url]);
    }

    public function waitPayList(): bool
    {
        $type     = input('type', 0);
        $page     = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info     = CommunityLogic::getInstance()->waitPayList($type, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * 社区首页
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function communityList(): bool
    {
        $page     = input('page', 1);
        $pageSize = input('pageSize', 10);
        $type     = input('type', 1);
        $info     = CommunityLogic::getInstance()->communityList($type, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * 绿色积分兑换余额
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function withdrawCommunity(): bool
    {
        $payPassword = input('payPassword', 0);
        $amount      = input('amount', 0);
        $type        = input('type', 0);
        $info        = CommunityLogic::getInstance()->withdrawCommunity($payPassword, $amount, $type);
        return $this->result($info);
    }

    /**
     * 余额提现
     * @throws ModelNotFoundException
     * @throws DbException
     * @throws DataNotFoundException
     */
    public function withdraw(): bool
    {
        $payPassword = input('payPassword', 0);
        $amount      = input('amount', 0);
        $type        = input('type', 0);
        $info        = CommunityLogic::getInstance()->withdraw($payPassword, $amount, $type);
        return $this->result($info);
    }

    /**
     * 余额提现记录
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function withdrawAmountList(): bool
    {
        $type     = input('type', 1);
        $page     = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info     = CommunityLogic::getInstance()->withdrawAmountList($type, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * 提现信息
     * @return bool
     */
    public function withdrawInfo(): bool
    {
        $type = input('type', 0);
        $info = CommunityLogic::getInstance()->withdrawInfo($type);
        return $this->result($info);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function withdrawList(): bool
    {
        $type     = input('type', 0);
        $page     = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info     = CommunityLogic::getInstance()->withdrawList($type, $page, $pageSize);
        return $this->result($info);
    }

    public function packetList(): bool
    {
        $page     = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info     = CommunityLogic::getInstance()->packetList($page, $pageSize);
        return $this->result($info);
    }

    /**
     * 我的钱包
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function myAmountInfo(): bool
    {
        $info = CommunityLogic::getInstance()->myAmountInfo();
        return $this->result($info);
    }

    /**
     * 社区类型列表
     * @throws ModelNotFoundException
     * @throws DbException
     * @throws DataNotFoundException
     */
    public function getCateList(): bool
    {
        $list = CommunityLogic::getInstance()->getCateList();
        return $this->result($list);
    }

    /**
     * 转让订单生成
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function createCommunityOp(): bool
    {
        $rate    = input('rate', 0);
        $image   = input('image', '');
        $signImage   = input('signImage', '');
        $orderId = input('orderId', '');
        $info    = CommunityLogic::getInstance()->createCommunityOp($rate, $image, $orderId, $signImage);
        return $this->result($info);
    }

    /**
     * 更新经营状态 弃用
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function opStatus(): bool
    {
        $orderId = input('orderId', '');
        $info    = CommunityLogic::getInstance()->opStatus($orderId);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function payOpOrder(): bool
    {
        $orderNo     = input('orderNo', 0);
        $type        = input('type', 0);
        $payPassword = input('payPassword', 0);
        $paySubType  = input('paySubType', 0);
        $image = input('image','');
        $signImage = input('signImage','');
        $orderId = input('orderId','');
        $info        = CommunityLogic::getInstance()->payOpOrder($orderNo, $type, $payPassword, $paySubType, $image, $signImage, $orderId);
        return $this->result($info);
    }

    /**
     * 社区订单详情
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function communityOrderDetail(): bool
    {
        $orderId = input('orderId', 0);
        $info    = CommunityLogic::getInstance()->getLastCommunityOrder($orderId);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DbException
     * @throws DataNotFoundException
     */
    public function communityOrderDetail1(): bool
    {
        $orderId = input('orderId', 0);
        $info    = CommunityLogic::getInstance()->getCommunityOrder($orderId);
        return $this->result($info);
    }

    /**
     * 任务
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function communityTask(): bool
    {
        $communityId = input('communityId', 0);
        $orderId = input('orderId', 0);
        $info        = CommunityLogic::getInstance()->communityTask($communityId,$orderId);
        return $this->result($info);
    }

    public function communityTaskForAdv(): bool
    {
        $communityId = input('communityId');
        $orderId = input('orderId');
        $info    = CommunityLogic::getInstance()->communityTaskForAdv($communityId,$orderId);
        return $this->result($info);
    }

    /**
     * 立即经营(锁定社区)
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function communityLock(): bool
    {
        $communityId = input('communityId', 0);
        $info        = CommunityLogic::getInstance()->communityLock($communityId);
        return $this->result($info);
    }

    /**
     * 兑换社区列表
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function communityExchangeList(): bool
    {
        $page     = input('page', 1);
        $pageSize = input('pageSize', 10);
        $name        = input('name',0);
        $provinceId  = input('provinceId',0);
        $cityId      = input('cityId',0);
        $countryId   = input('countryId',0);
        $streetId    = input('streetId',0);
        $commId      = input('commId',0);
        $communityId = input('communityId',0);
        $info        = CommunityLogic::getInstance()->communityExchangeList($page,$pageSize,$name,$provinceId,$cityId,$countryId,$streetId,$commId,$communityId);
        return $this->result($info);
    }

    /**
     * 兑换社区
     * @throws ModelNotFoundException
     * @throws DataNotFoundExceptionex
     * @throws DbException
     */
    public function communityExchange($oriCommunityId, $exCommunityId): bool
    {
        $oriCommunityId = input('oriCommunityId');
        $exCommunityId = input('exCommunityId');
        $info = CommunityLogic::getInstance()->communityExchange($oriCommunityId,$exCommunityId);
        return $this->result($info);
    }

    /**
     * 经营中转让
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function communityTransfer(): bool
    {
        $rate = input('rate');
        $communityId = input('communityId');
        $image = input('image');
        $signImage = input('signImage');
        $info = CommunityLogic::getInstance()->communityTransfer($rate,$image,$communityId,$signImage);
        return $this->result($info);
    }

    /**
     * 获取锁定/购买社区配置
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function getTransferRate(): bool
    {
        $info = CommunityLogic::getInstance()->getTransferRate();
        return $this->result($info);
    }

    /**
     * 大厅列表
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function hall(): bool
    {
        $name = input('name');
        $cateId = input('cateId');
        $provinceId  = input('provinceId',0);
        $cityId      = input('cityId',0);
        $countryId   = input('countryId',0);
        $streetId    = input('streetId',0);
        $page        = input('page',1);
        $pageSize    = input('pageSize',10);
        $info = CommunityLogic::getInstance()->hall($cateId,$name,$provinceId,$cityId,$countryId,$streetId,$page,$pageSize);
        return $this->result($info);
    }

    /**
     * 可入驻列表
     * @throws ModelNotFoundException
     * @throws DbException
     * @throws DataNotFoundException
     */
    public function allHall(): bool
    {
        $name        = input('name');
        $provinceId  = input('provinceId',0);
        $cityId      = input('cityId',0);
        $countryId   = input('countryId',0);
        $streetId    = input('streetId',0);
        $page        = input('page',1);
        $pageSize    = input('pageSize',10);
        $info = CommunityLogic::getInstance()->allHall($name, $provinceId, $cityId, $countryId, $streetId, $page, $pageSize);
        return $this->result($info);

    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function cateHall(): bool
    {
        $info = CommunityLogic::getInstance()->cateHall();
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DbException
     * @throws DataNotFoundException
     */
    public function myCommunityOpList(): bool
    {
        $name = input('name');
        $streetId = input('streetId');
        $page        = input('page',1);
        $pageSize    = input('pageSize',10);
        $info = CommunityLogic::getInstance()->myCommunityOpList($name,$streetId,$page,$pageSize);
        return $this->result($info);
    }

    /**
     * 招募大厅下单
     * @throws ModelNotFoundException
     * @throws DbException
     * @throws DataNotFoundException
     */
    public function createCommunityHallOrder(): bool
    {
        $communityId = input('communityId');
        $image = input('image');
        $signImage = input('signImage');
        $cateId = input('cateId');
        $info = CommunityLogic::getInstance()->createCommunityHallOrder($communityId,$image,$signImage,$cateId);
        return $this->result($info);
    }

    /**
     * 支付招募大厅订单
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function hallPayOrder(): bool
    {
        $orderNo     = input('orderNo', 0);
        $type        = input('type', 0);
        $isBag       = input('isBag', 0);
        $payPassword = input('payPassword', 0);
        $paySubType  = input('paySubType', 0);
        $info        = CommunityLogic::getInstance()->hallPayOrder($orderNo, $isBag, $type, $payPassword, $paySubType);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function communityHallOrderPreview(): bool
    {
        $communityId = input('communityId', 0);
        $cateId      = input('cateId');
        $info        = CommunityLogic::getInstance()->communityHallOrderPreview($communityId,$cateId);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function getLastCommunityAccount(): bool
    {
        $communityId = input('communityId', 0);
        $info        = CommunityLogic::getInstance()->getLastCommunityAccount($communityId);
        return $this->result($info);
    }

    /**
     * 创建预约社区
     * @throws ModelNotFoundException
     * @throws DbException
     * @throws DataNotFoundException
     */
    public function createPrepayOrder(): bool
    {
        $info        = CommunityLogic::getInstance()->createPrepayOrder();
        return $this->result($info);
    }

    /**
     * 支付预约社区
     * @return bool
     */
    public function prepayOrder(): bool
    {
        $orderNo     = input('orderNo', 0);
        $type        = input('type', 0);
        $isBag       = input('isBag', 0);
        $payPassword = input('payPassword', 0);
        $paySubType  = input('paySubType', 0);
        $info        = CommunityLogic::getInstance()->prepayOrder($orderNo, $isBag, $type, $payPassword, $paySubType);
        return $this->result($info);
    }


    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function prepayInfo(): bool
    {
        $info        = CommunityLogic::getInstance()->prepayInfo();
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function prepayList(): bool
    {
        $page     = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info        = CommunityLogic::getInstance()->prepayList($page,$pageSize);
        return $this->result($info);
    }

    public function prepayDel(): bool
    {
        $info        = CommunityLogic::getInstance()->prepayDel();
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function getCommunityOrderList(): bool
    {
        $startAt = input('startAt');
        $endAt = input('endAt');
        $page = input('page',1);
        $pageSize = input('pageSize',10);
        $logType = input('logType');
        $info        = CommunityLogic::getInstance()->getCommunityOrderList($startAt, $endAt, $page, $pageSize, $logType);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function inPrepayPayOrder(): bool
    {
        $info        = CommunityLogic::getInstance()->inPrepayPayOrder();
        return $this->result($info);
    }

    public function rankRule(): bool
    {
        $info = CommunityLogic::getInstance()->rankRule();
        return $this->result($info);
    }


}
