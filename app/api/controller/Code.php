<?php


namespace app\api\controller;

use AlibabaCloud\Client\Exception\ClientException;
use app\api\logic\CodeLogic;
use app\common\controller\RestController;

class Code extends RestController
{
    protected $noNeedLogin = ['*'];

    /**
     * @return bool
     * @throws ClientException
     */
    public function index(): bool
    {
        $phone = input('phone', '');
        $codeType = input('codeType', '');
        $return = CodeLogic::getInstance()->index($phone, $codeType);
        return $this->result($return);
    }
}