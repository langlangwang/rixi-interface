<?php


namespace app\api\controller;

use app\api\logic\CartLogic;
use app\common\controller\RestController;

class Cart extends RestController
{
    protected $noNeedLogin = [''];

    public function addCart(): bool
    {
        $goodsId = input('goodsId', 0);
        $quantity = input('quantity', 1);
        $skuId = input('skuId', 0);
        $info = CartLogic::getInstance()->addCart($goodsId, $quantity,$skuId);
        return $this->result($info);
    }

    /**
     * @return bool
     */
    public function turnCart(): bool
    {
        $goodsId = input('goodsId', 0);
        $quantity = input('quantity', 1);
        $skuId = input('skuId', 0);
        $cartId = input('cartId', 0);
        $info = CartLogic::getInstance()->turnCart($cartId, $goodsId, $quantity,$skuId);
        return $this->result($info);
    }


    public function delCart(): bool
    {
        $cartId = input('cartId', 0);

        $info = CartLogic::getInstance()->delCart($cartId);
        return $this->result($info);
    }

    public function cartList(): bool
    {
        $info = CartLogic::getInstance()->cartList();
        return $this->result($info);
    }
}