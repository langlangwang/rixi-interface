<?php
namespace app\api\controller;

use app\api\logic\CarefreeOrderLogic;
use app\api\logic\OrderLogic;
use app\api\logic\PayLogic;
use app\api\logic\ServiceLogic;
use app\common\controller\RestController;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class CarefreeOrder extends RestController
{
    protected $noNeedLogin = [
    ];

    public function orderExpress(): bool
    {
        $orderNo = input('orderNo', 0);
        $goodsId = input('goodsId', 0);
        $info    = OrderLogic::getInstance()->orderExpress($orderNo, $goodsId);
        return $this->result($info);
    }

    /**
     * @return bool
     */
    public function delOrder(): bool
    {
        $orderNo = input('orderNo', 0);
        $info    = CarefreeOrderLogic::getInstance()->delOrder($orderNo);
        return $this->result($info);
    }

    /**
     * 取消并退款
     * @param $orderNo
     * @return bool
     */
    public function cancelOrder(): bool
    {
        $orderNo = input('orderNo', 0);
        $info    = CarefreeOrderLogic::getInstance()->cancelOrder($orderNo);
        return $this->result($info);
    }

    /**
     * 取消不需要退款
     * @return bool
     */
    public function cancellationOrder(): bool
    {
        $orderNo = input('orderNo', 0);
        $info    = CarefreeOrderLogic::getInstance()->cancellationOrder($orderNo);
        return $this->result($info);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function orderList(): bool
    {
        $page      = input('page', 1);
        $pageSize  = input('pageSize', 10);
        $type      = input('type', 1);
        $info      = CarefreeOrderLogic::getInstance()->orderList($type, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * 订单预览
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function orderPreview(): bool
    {
        $goodsInfo = input('goodsInfo', '');
        $addressId = input('addressId', 0);
        $couponId  = input('couponId', '');
        $info      = CarefreeOrderLogic::getInstance()->orderPreview($goodsInfo, $couponId);
        //dump($info);die;
        /*$info['tips'] = CarefreeOrderLogic::getInstance()->estimatedDelivery(
            $info['rows'][0]['shopInfo']['sendCity'] ?? '',
            $addressId
        );*/
        return $this->result($info);
    }


    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function createOrder(): bool
    {
        $goodsInfo = input('goodsInfo', '');
        $addressId = input('addressId', 0);
        $couponId  = input('couponId', 0);
        $price     = input('price', 0);
        $amount    = input('amount', 0);
        $useSale   = input('useSale', '');
        $userNote = input('userNote', '');
        $deduction = input('deduction');
        $info = CarefreeOrderLogic::getInstance()->createOrder(
            $goodsInfo,
            $addressId,
            $price,
            $amount,
            $couponId,
            $userNote,
            [
                'useSale'    => $useSale,
            ],
            $deduction
        );
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function payOrder(): bool
    {
        $orderNo = input('orderNo', 0);
        $payType = input('payType', 1);
        $payPassword = input('payPassword', 0);
        $paySubType = input('paySubType', 0);
        $info = CarefreeOrderLogic::getInstance()->payOrder($orderNo, $payType, $payPassword,$paySubType);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function orderDetail(): bool
    {
        $orderNo = input('orderNo','');
        $info = CarefreeOrderLogic::getInstance()->orderDetail($orderNo);
        return $this->result($info);
    }


    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function taskOrder(): bool
    {
        $orderNo = input('orderNo', 0);
        $info    = CarefreeOrderLogic::getInstance()->taskOrder($orderNo);
        return $this->result($info);
    }


}
