<?php
namespace app\api\controller;

use app\api\logic\SupplyLogic;
use app\common\controller\RestController;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class Supply extends RestController
{
    protected $noNeedLogin = ['goodsCategory', 'goodsList', 'goodsDetail'];

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function goodsCategory(): bool
    {
        $categoryId = input('categoryId', 0);
        $info       = SupplyLogic::getInstance()->goodsCategory($categoryId);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DbException
     * @throws DataNotFoundException
     */
    public function goodsList(): bool
    {
        $page         = input('page', 1);
        $pageSize     = input('pageSize', 10);
        $keyword      = input('keyword', '');
        $categoryId1  = input('categoryId1', 0);
        $categoryId2  = input('categoryId2', 0);
        $categoryId3  = input('categoryId3', 0);
        $positionType = input('positionType', 0);
        $sort         = input('sort', 1);

        $info = SupplyLogic::getInstance()->goodsList(
            $page,
            $pageSize,
            $keyword,
            $categoryId1,
            $categoryId2,
            $categoryId3,
            $sort,
            $positionType
        );
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function goodsDetail(): bool
    {
        $id   = input('id', 0);
        $info = SupplyLogic::getInstance()->goodsDetail($id);
        return $this->result($info);
    }

    public function getRegionByCodeOpen(): bool
    {
        $regionCode = input('regionCode', 0);
        $info       = SupplyLogic::getInstance()->getRegionByCodeOpen($regionCode);
        return $this->result($info);
    }

    public function getOrderDetail(): bool
    {
        $orderSn = input('orderSn', 0);
        $info    = SupplyLogic::getInstance()->getOrderDetail($orderSn);
        return $this->result($info);
    }

    /**
     * 订单预览
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function orderPreview(): bool
    {
        $spuInfoList = input('spuInfoList', '');
        $addressId   = input('addressId', '');
        $info        = SupplyLogic::getInstance()->orderPreview(
            $spuInfoList,
            $addressId
        );
        return $this->result($info);
    }

    /**
     * 创建订单
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function createOrder(): bool
    {
        $goodsList = input('goodsList', '');
        $addressId = input('addressId', '');
        $userNote  = input('userNote', '');
        $couponId  = input('couponId', '');
        // 使用优惠类型：  coupon | discount | coupon_discount | '' 空字符串表示不需要优惠
        $useSale = input('useSale', '');

        $info = SupplyLogic::getInstance()->createOrder(
            $goodsList,
            $addressId,
            $couponId,
            $userNote,
            [
                'useSale'    => $useSale,
                'special_id' => intval(input('specialId', input('special_id', 0))),
            ],
        );
        return $this->result($info);
    }
}
