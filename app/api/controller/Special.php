<?php
namespace app\api\controller;

use app\api\logic\SpecialLogic;
use app\common\controller\RestController;

class Special extends RestController
{
    protected $noNeedLogin = [
        'index', 'goodsPage', 'articlePage', 'article', 'singlepage',
        // 'thumbup',
    ];

    /**
     * 专题主页
     * @return [type] [description]
     */
    public function index(): array
    {
        $id    = input('id', 0);
        $where = [
            'id' => (int) $id,
        ];
        $info = \think\facade\Db::table('special')
            ->where($where)
            ->find();
        $res = empty($info) || $info['deleted'] !== 0;
        if ($res) {
            \app\common\utils\CommonUtil::throwException(
                \app\api\consDir\ErrorConst::PARAM_ERROR,
                '专题不存在',
            );
        }
        if ($info['status'] == 2) {
            \app\common\utils\CommonUtil::throwException(
                \app\api\consDir\ErrorConst::PARAM_ERROR,
                '专题已下线',
            );
        }
        \think\facade\Db::table('special')
            ->where($where)
            ->inc('view_count')
            ->update();
        $menu  = empty($info['menu']) ? [] : json_decode($info['menu'], true);
        $menu2 = [];
        // "code": "pf618oqoy",
        // "sort": "100",
        // "status": 1,
        // "title": "莲荷产品",
        // "type": "special.goods"
        foreach ($menu as $m) {
            $url = '';
            switch ($m['type']) {
                case 'special.goods':
                    $url = '/api/special/goodsPage?code=' . $m['code'];
                    break;
                case 'special.article':
                    $url = '/api/special/articlePage?code=' . $m['code'];
                    break;
                case 'special.singlepage':
                    $url = '/api/special/singlepage?code=' . $m['code'];
                    break;
            }
            $menu2[] = [
                'code'  => $m['code'],
                'type'  => $m['type'],
                'title' => $m['title'],
                'url'   => $url,
            ];
        }

        if ( ! empty($obj)) {
            \think\facade\Db::table('special')
                ->where($where)
                ->inc('view_count')
                ->update();
        }

        // return $this->result($info);
        return $this->result([
            'id'          => $info['id'],
            'title'       => $info['title'], // 莲荷世界",
            'subtitle'    => $info['subtitle'], // 祖农为乐",
            'index_bg'    => $info['index_bg'], // http...
            'intro'       => $info['intro'], //
            'detail'      => $info['detail'], // html string,
            'banner'      => empty($info['banner']) ? [] : json_decode($info['banner'], true),
            'menu'        => $menu2,
            'tag'         => empty($info['tag']) ? [] : json_decode($info['tag'], true),
            'participant' => $info['participant'] + $info['participant_virtual'] + $info['view_count'], // 参与人数
            'goods_count' => $info['goods_count'],
            'view_count'  => $info['view_count'],
            'order_count' => $info['order_count'],
            'start_at'    => $info['start_at'], // 2024-05-01 00:00:00",
            'end_at'      => $info['end_at'], // 2024-05-01 00:00:00",
            'create_at'   => $info['create_at'], // 2024-05-20 18:43:51",
            'update_at'   => $info['update_at'], // 2024-05-25 09:43:04",
        ]);
    }

    /**
     * 专题商品列表
     * @return [type] [description]
     */
    public function goodsPage()
    {
        $where = [
            'code' => input('code', ''),
        ];
        $field = [
            'id',
            'special_id',
            'special_price',
            'goods_id',
            'shop_id',
            'code',
            'goods_title',
            'market_price',
            'sell_price',
            'cost_price',
            'goods_img',
            'goods_type',
            'goods_sales',
            'special_inventory',
            'category_id',
            'source',
            'sort',
        ];
        $page = SpecialLogic::getInstance()->goodsPage($where, $field);
        return $this->result($page);
    }

    /**
     * 专题文章列表
     * @return [type] [description]
     */
    public function articlePage()
    {
        $where = [
            'code' => input('code', ''),
        ];
        $field = [
            'id',
            'special_id',
            'code',
            'title',
            'author',
            'thumbnail',
            'description',
            'detail',
            'thumbup',
            'view_count',
            'sort',
            'publish_date',
        ];
        $page = SpecialLogic::getInstance()->articlePage($where, $field);
        return $this->result($page);
    }

    /**
     * 专题文章详情
     * @return [type] [description]
     */
    public function article()
    {
        $id    = intval(input('id', ''));
        $where = [
            'status' => 1,
            'id'     => $id,
        ];
        $field = [
            'id',
            'special_id',
            'title',
            'author',
            'thumbnail',
            'description',
            'detail',
            'thumbup',
            'view_count',
            'sort',
            'publish_date',
        ];
        $logic = SpecialLogic::getInstance();
        $obj   = $logic->articleDetail($where, $field);
        if ( ! empty($obj)) {
            \think\facade\Db::table('special_article')
                ->where($where)
                ->inc('view_count')
                ->update();
        }
        if (isset($obj['code'])) {
            unset($obj['code']);
        }

        $userinfo = \think\App::getInstance()->userinfo;
        if (empty($userinfo->id)) {
            $obj['isThumbup'] = false;
        } else {
            $obj['isThumbup'] = $logic->isThumb('special_article', $id, $userinfo->id);
        }
        // $userinfo->id   = 1000031;
        return $this->result($obj);
    }

    /**
     * 专题内容点赞
     * @return [type] [description]
     */
    public function thumbup()
    {
        $id    = (int) input('id');
        $table = '';
        $where = ['id' => $id];
        $type  = input('type', '');
        if ($type == 'article') {
            $table = 'special_article';
        } else if ($type == 'singlepage') {
            $table = 'special_singlepage';
        } else {
            \app\common\utils\CommonUtil::throwException(
                \app\api\consDir\ErrorConst::PARAM_ERROR,
                '不支持的类型',
            );
        }
        if ( ! empty($table)) {
            // 点赞类型 10 取消点赞  11 点赞;  20 取消踩一下 21 踩一下
            $action   = (int) input('action', 11);
            $userinfo = \think\App::getInstance()->userinfo;
            $logic    = SpecialLogic::getInstance();
            $logic->thumb($table, $id, $userinfo->id, $action);
        }
        return $this->result([]);
    }

    /**
     * 专题单页详情
     * @return [type] [description]
     */
    public function singlepage()
    {
        $where = [
            'status' => 1,
            'code'   => input('code', ''),
        ];
        $field = [
            'id',
            'special_id',
            'title',
            'description',
            'jump',
            'video',
            'thumbnail',
            'multi_img',
            'detail',
            'thumbup',
            'view_count',
            'sort',
        ];
        $logic = SpecialLogic::getInstance();
        $obj   = $logic->singlepageDetail($where, $field);
        if ( ! empty($obj)) {
            \think\facade\Db::table('special_singlepage')
                ->where($where)
                ->inc('view_count')
                ->update();
        }
        if (isset($obj['code'])) {
            unset($obj['code']);
        }
        $userinfo = \think\App::getInstance()->userinfo;
        // var_dump($userinfo->id);exit;
        if (empty($userinfo->id)) {
            $obj['isThumbup'] = false;
        } else {
            $obj['isThumbup'] = $logic->isThumb('special_article', $obj['id'], $userinfo->id);
        }
        return $this->result($obj);
    }
}
