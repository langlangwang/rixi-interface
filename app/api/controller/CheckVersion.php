<?php

namespace app\api\controller;

use app\api\cache\KeysUtil;
use app\api\cache\RedisCache;
use app\common\controller\RestController;
use app\common\models\Sys\ShopConfig;
use app\common\models\Sys\Version;

class CheckVersion extends RestController
{
    protected $noNeedLogin = ['*'];

    public function Check(): bool
    {
        /*$return = [
            'ios_online_version' => '1.0.0', //线上版本
            'ios_audit_version'  => '1.0.1',//审核版本
            'ios_down_url'       => 'https://rx-file.rxx1316.com/ios-rxx1.0.1.ipa',
            'android_online_version' => '1.0.0' ,
            'android_audit_version'  => '1.0.5'  ,
            'android_down_url'       => 'https://rx-file.rxx1316.com/ios-rxx1.0.1.apk' ,
            'is_android_compulsion'  => '0' ,//强制更新  1 强制  0非强制
            'h5_online_version'      => '1.0.0',
            'h5_audit_version'       => '1.0.1'
        ];*/
        $return = RedisCache::hGetAll(KeysUtil::getSystemConfigKey('version'));
        if (empty($return)) {
            $return = Version::getInstance()->column('value', 'key');
            RedisCache::hMSet(KeysUtil::getSystemConfigKey('version'), $return);
        }
        return $this->result($return);
    }

    /**
     * @return void
     * 检查支付通道
     */
    public function checkPayChannel()
    {
        /*$return = [
            "online" => ['alipay' => 1, 'wechat' => 0, 'purse' => 1, 'balance' => 1, 'greenpoints' => 0],
            "offline" => ['alipay' => 1, 'wechat' => 0, 'purse' => 1, 'balance' => 1, 'greenpoints' => 0],
            "packs" => ['alipay' => 1, 'wechat' => 0, 'purse' => 1, 'balance' => 1, 'greenpoints' => 0],
            "community" => ['alipay' => 0, 'wechat' => 0, 'purse' => 1, 'balance' => 0, 'greenpoints' => 0]
        ];*/
        $ret = RedisCache::hGetAll(KeysUtil::getSystemConfigKey('pay'));
        if (empty($ret)) {
            $config = ShopConfig::getInstance()->where('type', 'pay')->select();
            foreach ($config as $v) {
                RedisCache::hSet(KeysUtil::getSystemConfigKey('pay'), $v['configKey'], $v['configValue']);
                $ret[$v['configKey']] = $v['configValue'];
            }
        }
        foreach ($ret as $k => $v) {
            $ret[$k] = json_decode($v, true);
        }
        return $this->result($ret);
    }
}