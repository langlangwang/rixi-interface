<?php

namespace app\api\controller;

use app\api\logic\LotteryLogic;
use app\common\controller\RestController;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class Lottery extends RestController
{
    protected $noNeedLogin = [];

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function lotteryData()
    {
        $info = LotteryLogic::getInstance()->lotteryData();
        $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function awardList()
    {
        $info = LotteryLogic::getInstance()->awardList();
        $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function lottery()
    {
        $info = LotteryLogic::getInstance()->lottery();
        $this->result($info);
    }

    public function rule()
    {
        $info = LotteryLogic::getInstance()->rule();
        $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DbException
     * @throws DataNotFoundException
     */
    public function userLotteryList()
    {
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info = LotteryLogic::getInstance()->userLotteryList($page, $pageSize);
        $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function read(){
        $info = LotteryLogic::getInstance()->read();
        $this->result($info);
    }

    public function lotteryInfo(){
        $info = LotteryLogic::getInstance()->lotteryInfo();
        $this->result($info);
    }
}