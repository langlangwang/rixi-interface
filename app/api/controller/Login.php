<?php
namespace app\api\controller;

use app\api\logic\LoginLogic;
use app\common\controller\RestController;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class Login extends RestController
{
    protected $noNeedLogin = ['*'];

    /**
     * 合伙人登录
     *
     * @return [type] [description]
     */
    public function agency()
    {
        $phone    = input('phone', '');
        $code     = input('code', '');
        $type     = input('type', '');
        $password = input('password', '');
        $return   = LoginLogic::getInstance()->phoneLogin(
            $phone,
            $code,
            $type,
            $password,
            $prefix = 'agency');
        return $this->result($return);
    }

    /**
     * @return bool
     */
    public function login(): bool
    {
        $phone    = input('phone', '');
        $code     = input('code', '');
        $type     = input('type', '');
        $password = input('password', '');
        $return   = LoginLogic::getInstance()->phoneLogin(
            $phone,
            $code,
            $type,
            $password,
            $prefix = '');
        return $this->result($return);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function reg(): bool
    {
        $phone      = input('phone', '');
        $code       = input('code', '');
        $inviteCode = input('inviteCode', '');
        $provinceId = input('provinceId', '');
        $cityId     = input('cityId', '');
        $countryId  = input('countryId', '');
        $streetId   = input('streetId', '');
        $commId     = input('commId', '');
        $openId     = input('openId', '');
        $password   = input('password', '');
        $rePassword = input('rePassword', '');
        $return     = LoginLogic::getInstance()->reg($phone, $code, $inviteCode, $provinceId, $cityId, $countryId, $streetId, $commId, $openId, $password, $rePassword);
        return $this->result($return);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function codeLogin(): bool
    {
        $inviteCode    = input('inviteCode', '');
        $code          = input('code', '');
        $iv            = stripslashes(input('iv', ''));
        $encryptedData = stripslashes(input('encryptedData', ''));
        $return        = LoginLogic::getInstance()->codeLogin($code, $iv, $encryptedData, $inviteCode);
        return $this->result($return);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function bindPhone(): bool
    {
        $code          = input('code', '');
        $iv            = stripslashes(input('iv', ''));
        $encryptedData = stripslashes(input('encryptedData', ''));
        $info          = LoginLogic::getInstance()->bindPhone($code, $iv, $encryptedData);
        return $this->result($info);
    }

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function code(): bool
    {
        $code   = input('code', '');
        $return = LoginLogic::getInstance()->code($code);
        return $this->result($return);
    }

    /**
     * @return bool
     */
    public function appletLogin(): bool
    {
        $iv            = stripslashes(input('iv', ''));
        $sessionKey    = stripslashes(input('sessionKey', ''));
        $encryptedData = stripslashes(input('encryptedData', ''));

        $return = LoginLogic::getInstance()->appletLogin($iv, $sessionKey, $encryptedData);
        return $this->result($return);
    }

    /**
     * 找回密码
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function resetPwd()
    {
        $phone    = input('phone', '');
        $code     = input('code', '');
        $password = input('password', '');
        $return   = LoginLogic::getInstance()->resetPwd($phone, $code, $password);
        return $this->result($return);
    }
}
