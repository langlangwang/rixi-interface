<?php
namespace app\api\controller;

use app\api\logic\CouponLogic;
use app\common\controller\RestController;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class Coupon extends RestController
{
    protected $noNeedLogin = [
        'available',
    ];

    /**
     * 旧的方法，在优惠券功能上线前需要删除只
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function couponList(): bool
    {
        $type     = input('type', 1);
        $page     = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info     = CouponLogic::getInstance()->couponList($type, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * 我的优惠券
     * @throws ModelNotFoundException
     * @throws DbException
     * @throws DataNotFoundException
     */
    public function myCouponList(): bool
    {
        $status   = input('status', 1);
        $page     = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info     = CouponLogic::getInstance()->myCouponList($status, $page, $pageSize);
        return $this->result($info);
    }

    /**
     * 可用优惠券
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function available(): bool
    {
        $page     = input('page', 1);
        $pageSize = input('pageSize', 20);
        $shopId   = input('shopId', 0);
        $goodsId  = input('goodsId', 0);
        $source   = input('source', 0);
        $items    = CouponLogic::getInstance()->available(
            $shopId,
            $page,
            $pageSize,
            (int) $goodsId,
            (int) $source,
        );
        return $this->result($items);
    }

    /**
     * 领取优惠券
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function receive(): bool
    {
        $couponId = input('couponId', 0);
        $goodsId  = input('goodsId', 0);
        $source   = input('source', 0);
        $res      = CouponLogic::getInstance()->receive(
            $couponId,
            (int) $goodsId,
            (int) $source,
        );
        return $this->result($res);
    }

    /**
     * 已领取优惠券
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function myCouponMember(): bool
    {
        $page     = input('page', 1);
        $pageSize = input('pageSize', 20);
        $shopId   = input('shopId', 0);
        $goodsId   = input('goodsId', 0);
        $source   = input('source', 1);
        $res      = CouponLogic::getInstance()->myCouponMember($shopId, $page, $pageSize, $goodsId, $source);
        return $this->result($res);
    }
}
