<?php


namespace app\api\controller;

use app\api\logic\OrderLogic;
use app\api\logic\ServiceLogic;
use app\common\controller\RestController;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class Service extends RestController
{
    protected $noNeedLogin = [];


    /**
     * 服务中心列表
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function serviceList(): bool
    {
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $info = ServiceLogic::getInstance()->serviceList($page, $pageSize);
        return $this->result($info);
    }

    /**
     * 服务中心自提列表
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function serviceTaskList(): bool
    {
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $goodsId = input('goods_id');
        $info = ServiceLogic::getInstance()->serviceTaskList($page, $pageSize, $goodsId);
        return $this->result($info);
    }

    /**
     * 服务中心自提核销
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function check(): bool
    {
        $orderNo = input('orderNo');
        $serviceId = input('serviceId');
        $info = ServiceLogic::getInstance()->check($orderNo,$serviceId);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function orderDetail(): bool
    {
        $orderNo = input('orderNo');
        $serviceId = input('serviceId');
        $info = ServiceLogic::getInstance()->orderDetail($orderNo,$serviceId);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DbException
     * @throws DataNotFoundException
     */
    public function checkList(): bool
    {
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $serviceId = input('serviceId', 10);
        $info = ServiceLogic::getInstance()->checkList($page, $pageSize, $serviceId);
        return $this->result($info);
    }

    /**
     * @throws DbException
     */
    public function show(): bool
    {
        $info = ServiceLogic::getInstance()->show();
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DbException
     * @throws DataNotFoundException
     */
    public function serviceNearbyList(): bool
    {
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $lng = input('lng', '');
        $lat = input('lat', '');
        $info = ServiceLogic::getInstance()->serviceNearbyList($page, $pageSize, $lng, $lat);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function serviceDetail(): bool
    {
        $id = input('id');
        $info = ServiceLogic::getInstance()->serviceDetail($id);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function orderList(): bool
    {
        $page      = input('page', 1);
        $pageSize  = input('pageSize', 10);
        $serviceId = input('serviceId',0);
        $orderNo = input('orderNo', 2);
        $orderType = input('orderType','');
        $info      = ServiceLogic::getInstance()->orderList($page, $pageSize, $serviceId, $orderNo, $orderType);
        return $this->result($info);
    }


}