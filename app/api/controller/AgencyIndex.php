<?php
namespace app\api\controller;

use app\api\logic\AgencyLogic;
use app\common\controller\AgencyController;

class AgencyIndex extends AgencyController
{
    protected $noNeedLogin = [];

    public function index()
    {
        return $this->result([]);
    }

    /**
     * 合伙人-代理区域
     * @return [type] [description]
     */
    public function area()
    {
        // var_dump($this->app->userinfo->id);exit;
        // 状态     1正常   0取消
        $data = \app\common\models\Member\MemberAgency::where('status', 1)
            ->where('deleted', 0)
            ->where('user_id', $this->app->userinfo->id)
            ->order('id desc')
            ->find()->toArray();
        $data['agencyAreaId'] = $data['id'];
        $this->result($data);
    }

    /**
     * 合伙人-代理区域-所属社区分页列表
     * @return [type] [description]
     */
    public function community()
    {
        // $page     = input('page', 1);
        // $pageSize = input('pageSize', 10);
        $id = input('agencyAreaId');
        if (empty($id)) {
            \app\common\utils\CommonUtil::throwException(
                \app\api\consDir\ErrorConst::PARAM_ERROR,
                'agencyAreaId 必须'
            );
        }
        $vo = \app\common\models\Member\MemberAgency::where('status', 1)
            ->where('deleted', 0)
            ->where('id', $id)
            ->where('user_id', $this->app->userinfo->id)
            ->find();
        if (empty($vo)) {
            \app\common\utils\CommonUtil::throwException(
                \app\api\consDir\ErrorConst::PARAM_ERROR,
                \app\api\consDir\ErrorConst::PARAM_ERROR_MSG
            );
        }
        $pageData = null;
        if ($vo['agencyType'] == 1) {
            $pageData = AgencyLogic::getInstance()->communityPage([
                'province_id' => $vo['provinceId'],
            ]);
        } elseif ($vo['agencyType'] == 2) {
            $pageData = AgencyLogic::getInstance()->communityPage([
                'city_id' => $vo['cityId'],
            ]);
        } elseif ($vo['agencyType'] == 3) {
            $pageData = AgencyLogic::getInstance()->communityPage([
                'country_id' => $vo['countryId'],
            ]);
        }
        $this->result($pageData);
    }

    /**
     * 合伙人-代理区域-区域商家分页列表
     */
    public function shop()
    {
        // $page     = input('page', 1);
        // $pageSize = input('pageSize', 10);
        $id = input('agencyAreaId');
        if (empty($id)) {
            \app\common\utils\CommonUtil::throwException(
                \app\api\consDir\ErrorConst::PARAM_ERROR,
                'agencyAreaId 必须'
            );
        }
        $vo = \app\common\models\Member\MemberAgency::where('status', 1)
            ->where('deleted', 0)
            ->where('id', $id)
            ->where('user_id', $this->app->userinfo->id)
            ->find();
        if (empty($vo)) {
            \app\common\utils\CommonUtil::throwException(
                \app\api\consDir\ErrorConst::PARAM_ERROR,
                \app\api\consDir\ErrorConst::PARAM_ERROR_MSG
            );
        }
        $pageData = null;
        if ($vo['agencyType'] == 1) {
            $pageData = AgencyLogic::getInstance()->shopPage([
                'province_id' => $vo['provinceId'],
            ]);
        } elseif ($vo['agencyType'] == 2) {
            $pageData = AgencyLogic::getInstance()->shopPage([
                'city_id' => $vo['cityId'],
            ]);
        } elseif ($vo['agencyType'] == 3) {
            $pageData = AgencyLogic::getInstance()->shopPage([
                'country_id' => $vo['countryId'],
            ]);
        }
        $this->result($pageData);
    }

    /**
     * 合伙人-代理区域-区域会员分页列表
     */
    public function member()
    {
        // $page     = input('page', 1);
        // $pageSize = input('pageSize', 10);
        $id = input('agencyAreaId');
        if (empty($id)) {
            \app\common\utils\CommonUtil::throwException(
                \app\api\consDir\ErrorConst::PARAM_ERROR,
                'agencyAreaId 必须'
            );
        }
        $vo = \app\common\models\Member\MemberAgency::where('status', 1)
            ->where('deleted', 0)
            ->where('id', $id)
            ->where('user_id', $this->app->userinfo->id)
            ->find();
        if (empty($vo)) {
            \app\common\utils\CommonUtil::throwException(
                \app\api\consDir\ErrorConst::PARAM_ERROR,
                \app\api\consDir\ErrorConst::PARAM_ERROR_MSG
            );
        }
        $pageData = null;
        if ($vo['agencyType'] == 1) {
            $pageData = AgencyLogic::getInstance()->memberPage([
                'province_id' => $vo['provinceId'],
            ]);
        } elseif ($vo['agencyType'] == 2) {
            $pageData = AgencyLogic::getInstance()->memberPage([
                'city_id' => $vo['cityId'],
            ]);
        } elseif ($vo['agencyType'] == 3) {
            $pageData = AgencyLogic::getInstance()->memberPage([
                'country_id' => $vo['countryId'],
            ]);
        }
        $this->result($pageData);
    }

    /**
     * 合伙人-代理区域-子代理分页列表
     */
    public function children()
    {
        // $page     = input('page', 1);
        // $pageSize = input('pageSize', 10);
        $id = input('agencyAreaId');
        if (empty($id)) {
            \app\common\utils\CommonUtil::throwException(
                \app\api\consDir\ErrorConst::PARAM_ERROR,
                'agencyAreaId 必须'
            );
        }
        $vo = \app\common\models\Member\MemberAgency::where('status', 1)
            ->where('deleted', 0)
            ->where('id', $id)
            ->where('user_id', $this->app->userinfo->id)
            ->find();
        if (empty($vo)) {
            \app\common\utils\CommonUtil::throwException(
                \app\api\consDir\ErrorConst::PARAM_ERROR,
                \app\api\consDir\ErrorConst::PARAM_ERROR_MSG
            );
        }
        $pageData = null;
        if ($vo['agencyType'] == 1) {
            $pageData = AgencyLogic::getInstance()->memberPage([
                ['province_id', '=', $vo['provinceId']],
                ['is_agency', 'in', [2, 3]],
            ]);
        } elseif ($vo['agencyType'] == 2) {
            $pageData = AgencyLogic::getInstance()->memberPage([
                ['city_id', '=', $vo['cityId']],
                ['is_agency', '=', 3],
            ]);
        } elseif ($vo['agencyType'] == 3) {
            $pageData = [];
        }
        $this->result($pageData);
    }

    /**
     * 合伙人 用户信息
     * @return [type] [description]
     */
    public function memberInfo()
    {
        $uid = input('userId');
        // use index status_user_id_create_at
        $gxz = \think\facade\Db::table('member_gxz_log')
            ->where('user_id', $uid)
        // 1加 2减少
            ->where('status', 1)
            ->sum('amount');

        $field = [
            'id',
            'id user_id',
            'avatar',
            'user_name',
            'phone',
            'create_at', // 入驻时间
            // 'province',
            'province_id',
            // 'city',
            'city_id',
            // 'country',
            'country_id',
            // 'street',
            'street_id',
            // 'comm',
            'comm_id',
            'is_agency',
            'level',
        ];

        $vol = \app\common\models\Member\Member::field($field)
            ->where('id', $uid)
            ->find()->toArray();

        if (empty($vol)) {
            \app\common\utils\CommonUtil::throwException(
                \app\api\consDir\ErrorConst::PARAM_ERROR,
                \app\api\consDir\ErrorConst::PARAM_ERROR_MSG
            );
        }
        $vol = AgencyLogic::getInstance()->filterMemberInfo($vol);

        $vol['gxzAmount'] = $gxz; // 累计贡献收益
        $this->result($vol);
    }

    /**
     * 合伙人 商家信息
     * @return [type] [description]
     */
    public function shopInfo()
    {
        $shopId = input('shopId');

        $field = [
            'id',
            'id shop_id',
            'shop_logo',
            'shop_name',
            'business_name',
            'business_type',
            'create_at', // 入驻时间
            'shop_type',
            'province',
            'province_id',
            'city',
            'city_id',
            'country',
            'country_id',
            'street',
            'street_id',
            'comm',
            'comm_id',
            'latitude',
            'longitude',
            'address',
            'user_id',
        ];

        $vol = \app\common\models\Shop\Shop::field($field)
            ->where('id', $shopId)
            ->find()->toArray();

        if (empty($vol)) {
            \app\common\utils\CommonUtil::throwException(
                \app\api\consDir\ErrorConst::PARAM_ERROR,
                \app\api\consDir\ErrorConst::PARAM_ERROR_MSG
            );
        }
        // use index status_user_id_create_at
        $gxz = \think\facade\Db::table('member_gxz_log')
            ->where('user_id', $vol['userId'])
        // 1加 2减少
            ->where('status', 1)
            ->sum('amount');
        $vol2 = \think\facade\Db::table('order_shop')
            ->where('shop_id', $shopId)
            ->where('deleted', 0)
        // 0待支付 1待发货 2待收货 3已收货 4已完成 5退款中 6退款成功 7取消 8超时取消 9待退货
            ->whereIn('status', [1, 2, 3, 4, 5, 6])
            ->field([
                'sum(sell_price) revenue',
                'count(*) orderCount',
            ])->find();
        // echo \think\facade\Db::table('order_shop')->getlastsql();exit;
        // var_dump($vol2);exit;
        $vol['gxzAmount']  = $gxz; // 累计贡献收益
        $vol['revenue']    = $vol2['revenue'] ?? '0.00'; // 营业额
        $vol['orderCount'] = $vol2['orderCount'] ?? 0; // 订单数
        $this->result($vol);
    }

    /**
     * 合伙人 - 统计报表
     * @return [type] [description]
     */
    public function stats()
    {
        // $this->app->userinfo->id
    }
}
