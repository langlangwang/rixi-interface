<?php


namespace app\api\controller;

use app\api\logic\GoodsLogic;
use app\common\controller\RestController;

class Goods extends RestController
{
    protected $noNeedLogin = ['*'];

    public function search(): bool
    {
        $info = GoodsLogic::getInstance()->search();
        return $this->result($info);
    }

    /**
     * @return bool
     */
    public function goodsComment(): bool
    {
        $goodsId = input('goodsId',0);
        $page = input('page',1);
        $pageSize = input('pageSize',10);
        $isBad = input('isBad',0);
        $isImg = input('isImg',0);
        $info = GoodsLogic::getInstance()->goodsComment($goodsId,$page,$pageSize,$isBad,$isImg);
        return $this->result($info);
    }

    public function goodsDetail(): bool
    {
        $goodsId = input('goodsId',0);
        $info = GoodsLogic::getInstance()->goodsDetail($goodsId);
        return $this->result($info);
    }
    /**
     * @return bool
     */
    public function goodsList(): bool
    {
        $page = input('page',1);
        $pageSize = input('pageSize',10);
        $sort = input('sort',0);
        $keyword = input('keyword','');
        $categoryId = input('categoryId',0);
        $goodsType = input('goodsType',2);
        $shopId = input('shopId',0);
        $positionType = input('positionType',0);
        $info = GoodsLogic::getInstance()->goodsList($goodsType,$page,$pageSize,$sort,$keyword,$categoryId,$shopId,$positionType);
        return $this->result($info);
    }


    public function isLike(): bool
    {
        $page = input('page',1);
        $pageSize = input('pageSize',10);
        $info = GoodsLogic::getInstance()->isLike($page,$pageSize);
        return $this->result($info);
    }


    public function goodsCategory(): bool
    {
        $categoryType = input('categoryType',2);
        $info = GoodsLogic::getInstance()->goodsCategory($categoryType);
        return $this->result($info);
    }
}