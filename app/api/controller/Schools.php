<?php


namespace app\api\controller;

use app\api\logic\SchoolsLogic;
use app\common\controller\RestController;

class Schools extends RestController
{
    protected $noNeedLogin = [''];


    public function schoolsList(): bool
    {
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $searchKey= input('searchKey', "");
        $info = SchoolsLogic::getInstance()->schoolsList($page, $pageSize ,$searchKey);
        return $this->result($info);
    }
    public function schoolsDetail()
    {
        $shoolsid = input( 'schoolsId' ,0 );
        $info = SchoolsLogic::getInstance()->schoolsDetail( $shoolsid );
        return $this->result($info);
    }
}