<?php

namespace app\api\controller;

use app\api\logic\SandLogic;
use app\common\controller\RestController;

class Sand extends RestController
{
    protected $noNeedLogin = ['accountBalanceQuery', 'accountMemberQuery', 'accountOneKey', 'orderCreateScan', 'detailQuery'];

    public function accountBalanceQuery(): bool
    {
        $userId = input('userId');
        $info = SandLogic::getInstance()->accountBalanceQuery($userId);
        return $this->result($info);
    }

    public function accountMemberQuery(): bool
    {
        $userId = input('userId');
        $info = SandLogic::getInstance()->accountMemberQuery($userId);
        return $this->result($info);
    }

    public function accountOneKey(): bool
    {
        $userId = input('userId');
        $nickname = input('nickname');
        $name = input('name');
        $idCard = input('idCard');
        $phone = input('phone');
        $info = SandLogic::getInstance()->accountOneKey($nickname, $name, $idCard, $phone, $userId);
        return $this->result($info);
    }

    public function detailQuery(): bool
    {
        $userId = input('userId');
        $beginDate = input('beginDate');
        $endDate = input('endDate');
        $page = input('page',1);
        $pageSize = input('pageSize',10);
        $info = SandLogic::getInstance()->detailQuery($userId, $beginDate, $endDate, $page, $pageSize,);
        return $this->result($info);
    }

    public function paymentOrderCreate(): bool
    {
        $amount = input('amount');
        $name = input('name');
        $bankNo = input('bankNo');
        $info = SandLogic::getInstance()->paymentOrderCreate($bankNo, $name, $amount);
        return $this->result($info);
    }

    public function paymentOrderQuery(): bool
    {
        $orderNo = input('orderNo');
        $info = SandLogic::getInstance()->paymentOrderQuery($orderNo);
        return $this->result($info);
    }


    /**
     * 会员绑卡查询
     * @return bool
     */
    public function accountBindCardQuery(): bool
    {
        $userId = input('userId');
        $info = SandLogic::getInstance()->accountBindCardQuery($userId);
        return $this->result($info);
    }

    /**
     * 钱包
     * @return bool
     */
    public function account(): bool
    {
        $info = SandLogic::getInstance()->account();
        return $this->result($info);
    }

    public function orderQuery(): bool
    {
        $orderNo = input('orderNo');
        $info = SandLogic::getInstance()->orderQuery($orderNo);
        return $this->result($info);
    }

    public function walletOrderQuery(): bool
    {
        $orderNo = input('orderNo');
        $info = SandLogic::getInstance()->walletOrderQuery($orderNo);
        return $this->result($info);
    }

    /*
   public function transfer(): bool
   {
      $amount = input('amount');
       $payerBizUserNo = input('payerBizUserNo');
       $payeeBizUserNo = input('payeeBizUserNo');
       $bizUserFeeAmt = input('bizUserFeeAmt');
       $orderNo = input('orderNo');
       $transMode = input('transMode');
       $info = SandLogic::getInstance()->transfer($amount, $payerBizUserNo, $payeeBizUserNo, $bizUserFeeAmt, $orderNo, $transMode);
       return $this->result($info);
    }*/

    /*
    public function transferConfirm(): bool
    {
        $outOrderNo = input('outOrderNo');
        $oriOutOrderNo = input('oriOutOrderNo');
        $amount = input('amount');
        $operationType = input('operationType');
        $userId = input('userId');
        $info = SandLogic::getInstance()->transferConfirm($outOrderNo, $oriOutOrderNo, $amount, $operationType, $userId);
        return $this->result($info);
    }*/

    /**
     * 扫码下单
     * @return bool
     */
    public function orderCreateScan(): bool
    {
        $amount = input('amount');
        $orderNo = input('orderNo');
        $info = SandLogic::getInstance()->orderCreateScan($amount, $orderNo);
        return $this->result($info);
    }

    /*
    public function accountMemberStatus(): bool
    {
        $userId = input('userId');
        $operationType = input('operationType');
        $info = SandLogic::getInstance()->accountMemberStatus($userId, $operationType);
        return $this->result($info);
    }*/

    public function orderCreate(): bool
    {
        $bizUserNo = input('bizUserNo');
        $amount = input('amount');
        $orderNo = input('orderNo');
        $info = SandLogic::getInstance()->orderCreate($bizUserNo, $amount, $orderNo);
        return $this->result($info);
    }


    /*
    public function orderRefund(): bool
    {
        $outOrderNo = input('outOrderNo');
        $oriOutOrderNo = input('oriOutOrderNo');
        $amount = input('amount');
        $info = SandLogic::getInstance()->orderRefund($outOrderNo, $oriOutOrderNo, $amount);
        return $this->result($info);
    }
    public function freeze(): bool
    {
        $outOrderNo = input('outOrderNo');
        $money = input('money');
        $userId = input('userId');
        $info = SandLogic::getInstance()->freeze($outOrderNo, $money, $userId);
        return $this->result($info);
    }

    public function unfreeze(): bool
    {
        $outOperationOrderNo = input('outOperationOrderNo');
        $oriOutOrderNo = input('oriOutOrderNo');
        $money = input('money');
        $info = SandLogic::getInstance()->unfreeze($outOperationOrderNo, $oriOutOrderNo, $money);
        return $this->result($info);
    }*/


}