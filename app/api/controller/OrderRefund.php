<?php
namespace app\api\controller;

use app\api\logic\OrderLogic;
use app\api\logic\OrderRefundLogic;
use app\common\controller\RestController;
use app\common\models\Order\OrderRefund as OrderRefundModel;

class OrderRefund extends RestController
{
    protected $noNeedLogin = [
        'preview', '',
    ];

    /**
     * 售后申请获取数据
     * @return [type] [description]
     */
    public function preview()
    {
        return $this->result([
            'attach'          => json_encode(input('attach', []), 320),
            'refundTypeArr'   => OrderRefundModel::getInstance()->typeArr,
            'refundStatusArr' => OrderRefundModel::getInstance()->statusArr,
            // 'processes'       => [
            //     ['value' => -1, 'text' => '申请退款'],
            //     ['value' => 0, 'text' => '处理中'],
            //     ['value' => 1, 'text' => '财务退款'],
            //     ['value' => 1, 'text' => '已完成'],
            // ],
            'reasonArr'       => [
                ['id' => 1, 'title' => '发货时间问题'],
                ['id' => 2, 'title' => '不想要了'],
                ['id' => 3, 'title' => '商品错选/多选'],
                ['id' => 4, 'title' => '地址信息填写错误'],
                ['id' => 5, 'title' => '价格高于其他平台'],
                ['id' => 6, 'title' => '没用/错用优惠'],
            ],
        ]);
    }

    /**
     * 用户售后单列表
     * @param int $state
     * @return array
     * @throws \think\exception\DbException
     */
    public function lists($state = -1)
    {
        $userinfo = \think\App::getInstance()->userinfo;
        // $userinfo->id   = 1000031;
        $page = OrderRefundLogic::getInstance()->lists(
            $userinfo,
            input(''));
        $page = json_decode(json_encode($page), true);
        // echo OrderRefundModel::getInstance()->getlastsql();exit;
        // var_dump($page);exit;
        return $this->result($page);
    }

    /**
     * 售后单详情
     * @return array
     * @throws \think\exception\DbException
     */
    public function detail()
    {
        $userinfo        = \think\App::getInstance()->userinfo;
        $order_refund_id = input('orderRefundId', 0);
        // $userinfo->id    = 1000031;
        if (empty($order_refund_id)) {
            $orderNo = input('orderNo', '');
            // idx_UserId_OrdereNo_OrderGoodsId
            $order_refund_id = \think\facade\Db::table('order_refund')
                ->where('user_id', $userinfo->id)
                ->where('order_no', $orderNo)
                ->order('order_refund_id', 'desc')
                ->value('order_refund_id');
        }
        // 售后单详情
        $detail = OrderRefundModel::where([
            'user_id'         => $userinfo->id,
            'order_refund_id' => $order_refund_id,
        ])->find();
        // var_dump($detail);exit;
        if (empty($detail)) {
            \app\common\utils\CommonUtil::throwException(
                \app\api\consDir\ErrorConst::PARAM_ERROR,
                '售后单不存在',
            );
        }
        $info = OrderLogic::getInstance()->orderDetail($detail['order_no']);
        // var_dump($info);exit;
        // 售后单状态(0进行中 1 已拒绝 2 财务退款  3 已完成 4已取消)
        $refundStatus = $detail['status'] ?? null;
        // 物流公司列表
        // idx_Type_Recommended_Name
        $expressList = \app\common\models\Sys\ExpressCompany::getInstance()
            ->recommended();
        return $this->result([
            // 'processes'   => OrderRefundLogic::getInstance()->processe($refundStatus),
            'detail'      => $detail ?? (object) [],
            'orderInfo'   => $info['orderInfo'],
            // 'orderAddress' => $info['orderAddress'],
            // 'orderShop'   => $info['orderShop'],
            'serviceInfo' => $info['serviceInfo'],
            'supplyGoods' => $info['supplyGoods'],
            'expressList' => $expressList,
        ]);
    }

    /**
     * 售后申请
     * @return [type] [description]
     */
    public function apply(): bool
    {
        $order_goods_id = input('orderGoodsId', 0);
        $orderNo        = input('orderNo', 0);
        $type           = input('type', 10);
        // applyDesc, attach

        $userinfo = \think\App::getInstance()->userinfo;
        // $userinfo->id   = 1000031;
        $res = OrderRefundLogic::getInstance()->apply(
            $userinfo->id,
            $type,
            $orderNo,
            $order_goods_id,
            input(''),
        );
        if ($res) {
            return $this->result($res);
        } else {
            \app\common\utils\CommonUtil::throwException(
                \app\api\consDir\ErrorConst::BASE_ERROR,
                OrderRefundLogic::getInstance()->error,
            );
        }
    }

    /**
     * 用户取消售后申请
     * @param $order_refund_id
     * @return array
     * @throws \think\exception\DbException
     */
    public function cancel()
    {
        $userinfo        = \think\App::getInstance()->userinfo;
        $order_refund_id = input('orderRefundId', 0);
        if (empty($order_refund_id)) {
            $orderNo = input('orderNo', '');
            // idx_UserId_OrdereNo_OrderGoodsId
            $order_refund_id = \think\facade\Db::table('order_refund')
                ->where('user_id', $userinfo->id)
                ->where('order_no', $orderNo)
                ->order('order_refund_id', 'desc')
                ->value('order_refund_id');
        }
        // $userinfo->id   = 1000031;
        // 售后单详情
        $model = OrderRefundModel::where([
            'user_id'         => $userinfo->id,
            'order_refund_id' => $order_refund_id,
        ])->find();
        if (OrderRefundLogic::getInstance()->cancel($model, input(''))) {
            return $this->result([]);
        }
        \app\common\utils\CommonUtil::throwException(
            \app\api\consDir\ErrorConst::BASE_ERROR,
            OrderRefundLogic::getInstance()->error,
        );
    }

    /**
     * 售后单-用户发货
     * @param $order_refund_id
     * @return array
     * @throws \think\exception\DbException
     */
    public function delivery()
    {
        $userinfo        = \think\App::getInstance()->userinfo;
        $order_refund_id = input('orderRefundId', 0);
        if (empty($order_refund_id)) {
            $orderNo = input('orderNo', '');
            // idx_UserId_OrdereNo_OrderGoodsId
            $order_refund_id = \think\facade\Db::table('order_refund')
                ->where('user_id', $userinfo->id)
                ->where('order_no', $orderNo)
                ->order('order_refund_id', 'desc')
                ->value('order_refund_id');
        }
        // $userinfo->id   = 1000031;
        // 售后单详情
        $model = OrderRefundModel::where([
            'user_id'         => $userinfo->id,
            'order_refund_id' => $order_refund_id,
        ])->find();
        if (OrderRefundLogic::getInstance()->delivery($model, input(''))) {
            return $this->result([]);
        }
        \app\common\utils\CommonUtil::throwException(
            \app\api\consDir\ErrorConst::BASE_ERROR,
            OrderRefundLogic::getInstance()->error,
        );
    }

}
