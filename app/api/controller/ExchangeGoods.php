<?php
namespace app\api\controller;

use app\api\logic\ExchangeGoodsLogic;
use app\common\controller\RestController;

class ExchangeGoods extends RestController
{
    protected $noNeedLogin = ['*'];

    public function search(): bool
    {
        $info = ExchangeGoodsLogic::getInstance()->search();
        return $this->result($info);
    }

    /**
     * @return bool
     */
    public function goodsComment(): bool
    {
        $goodsId  = input('goodsId', 0);
        $page     = input('page', 1);
        $pageSize = input('pageSize', 10);
        $isBad    = input('isBad', 0);
        $isImg    = input('isImg', 0);
        $info     = ExchangeGoodsLogic::getInstance()->goodsComment(
            $goodsId,
            $page,
            $pageSize,
            $isBad,
            $isImg);
        return $this->result($info);
    }

    public function goodsDetail(): bool
    {
        $goodsId      = input('goodsId', 0);
        $exchangeType = input('exchangeType', 0);

        $info = ExchangeGoodsLogic::getInstance()->goodsDetail(
            $exchangeType,
            $goodsId);
        return $this->result($info);
    }

    /**
     * @return bool
     */
    public function goodsList(): bool
    {
        $page         = input('page', 1);
        $pageSize     = input('pageSize', 10);
        $sort         = input('sort', 0);
        $keyword      = input('keyword', '');
        $categoryId   = input('categoryId', 0);
        $goodsType    = input('goodsType', 0);
        $shopId       = input('shopId', 0);
        $positionType = input('positionType', 0);
        $exchangeType = input('exchangeType', 0);

        $page = ExchangeGoodsLogic::getInstance()->goodsList(
            $goodsType,
            $page,
            $pageSize,
            [
                'sort'         => $sort,
                'keyword'      => $keyword,
                'categoryId'   => $categoryId,
                'shopId'       => $shopId,
                'positionType' => $positionType,
                'exchangeType' => $exchangeType,
            ]);
        return $this->result($page);
    }

    /**
     * 猜你喜欢 / 推荐商品
     * @return boolean [description]
     */
    public function isLike(): bool
    {
        $page         = input('page', 1);
        $pageSize     = input('pageSize', 10);
        $exchangeType = input('exchangeType', 0);

        $page = ExchangeGoodsLogic::getInstance()->isLike(
            $page,
            $pageSize,
            ['exchangeType' => $exchangeType]
        );
        return $this->result($page);
    }

    /**
     *
     * @return [type] [description]
     */
    public function goodsCategory(): bool
    {
        $pid = input('pid', 0);

        $categoryType = input('categoryType', 2);

        $info = ExchangeGoodsLogic::getInstance()->goodsCategory(
            $categoryType,
            $pid
        );
        return $this->result($info);
    }
}
