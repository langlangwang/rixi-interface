<?php


namespace app\api\controller;

use app\api\logic\CardLogic;
use app\common\controller\RestController;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class Card extends RestController
{
    protected $noNeedLogin = [];

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function cardList(): bool
    {
        $info = CardLogic::getInstance()->cardList();
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function orderList(): bool
    {
        $page = input('page', 1);
        $pageSize = input('pageSize', 10);
        $status = input('status', 1);
        $info = CardLogic::getInstance()->orderList($status, $page, $pageSize);
        return $this->result($info);
    }


    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function createOrder(): bool
    {
        $cardId = input('cardId', '');
        $info = CardLogic::getInstance()->createOrder(
            $cardId
        );
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function payOrder(): bool
    {
        $orderNo = input('orderNo', 0);
        $payType = input('payType', 1);
        $payPassword = input('payPassword', 0);
        $paySubType = input('paySubType', 0);
        $info = CardLogic::getInstance()->payOrder($orderNo, $payType, $payPassword, $paySubType);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function orderDetail(): bool
    {
        $orderNo = input('orderNo', 0);
        $info = CardLogic::getInstance()->orderDetail($orderNo);
        return $this->result($info);
    }

    public function cancelOrder(): bool
    {
        $orderNo = input('orderNo', 0);
        $info = CardLogic::getInstance()->cancelOrder($orderNo);
        return $this->result($info);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DbException
     * @throws DataNotFoundException
     */
    public function cardUser(): bool
    {
        $info = CardLogic::getInstance()->cardUser();
        return $this->result($info);
    }

}