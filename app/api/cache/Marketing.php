<?php

namespace app\api\cache;

use app\common\models\Sys\ShopConfig;

class Marketing
{

    public static function getKeyData($type,$field)
    {
        $key = $type.':'.$field;
        $data = RedisCache::get(KeysUtil::getSystemConfigKey($key));
        if (empty($data)) {
            $where = [
                ['type', '=', $type],
                ['config_key', '=', $field],
            ];
            $data = ShopConfig::getInstance()->where($where)->value('config_value');
            RedisCache::set(KeysUtil::getSystemConfigKey($key), $data, 300);
        }
        return $data ? json_decode($data, true) : [];
    }
}