<?php

namespace app\api\cache;

use app\common\models\Carefree\CarefreeConfig;
use app\common\models\Sys\ShopConfig;

class CarefreeCache
{

    public static function getKeyData($type,$field)
    {
        $key = $type.':'.$field;
        $data = RedisCache::get(KeysUtil::getCarefreeConfigKey($key));
        if (empty($data)) {
            $where = [
                ['type', '=', $type],
                ['config_key', '=', $field],
            ];
            $data = CarefreeConfig::getInstance()->where($where)->value('config_value');
            RedisCache::set(KeysUtil::getCarefreeConfigKey($key), $data, 300);
        }
        return $data ? json_decode($data, true) : [];
    }
}