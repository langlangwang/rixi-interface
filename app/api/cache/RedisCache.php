<?php
namespace app\api\cache;

use Exception;
use think\facade\Cache;

/**
 * Class RedisCache
 * @package app\api\cache
 * @method static string get(string $key)
 * @method static bool set(string $key, string $data, int $ttl)
 * @method static bool incrby(string $key, int $number)
 * @method static bool expire(string $key, int $ttl)
 * @method static bool del(string $key)
 * @method static bool hSet(string $key, string $field, $value)
 * @method static bool hMSet(string $key, $array)
 * @method static array hGetAll(string $key)
 * @method static array lPush(string $key, ...$val)
 * @method static string rPop(string $key)
 * @method static int zAdd(string $key,int $score,string $member)
 * @method static int zRem(string $key,...$val)
 * @method static array zRange(string $key,int $start,int $end,bool $option)
 * @method static array zRevRange(string $key,int $start,int $end)
 * @method static string zScore(string $key,string $member)
 * @method static int zCard(string $key)
 * @method static String hGet(string $key,String $field)
 * @method static String setEx(string $key,int $tll,string $value)
 * @method static String lLen(string $key)
 */
class RedisCache
{
    /**
     * @var \Redis $redisCodisHandel
     */
    public static $redisCodisHandel;

    /**
     * 集群前缀
     */
    const CACHE_PRE = 'rx:';

    /**
     * 10秒
     */
    const TEN_SECOND = 10;

    /**
     * 1分钟
     */
    const ONE_MINUTE = 60;

    /**
     * 2分钟
     */
    const TWO_MINUTE = 120;

    /**
     * 5分钟
     */
    const FIVE_MINUTE = 300;
    /**
     * 10分钟
     */
    const TEN_MINUTE = 600;

    /**
     * 20分钟
     */
    const TWENTY_MINUTE = 1200;

    /**
     * 30分钟或者半个小时
     */
    const THIRTY_MINUTE = 1800;

    /**
     * 1小时
     */
    const ONE_HOUR = 3600;

    /**
     * 2小时
     */
    const TWO_HOUR = 7200;

    /**
     * 3小时
     */
    const THREE_HOUR = 10800;

    /**
     * 12小时或者半天
     */
    const TWELVE_HOUR = 43200;

    /**
     * 1天
     */
    const ONE_DAY = 86400;

    /**
     * 2天
     */
    const TWO_DAY = 172800;

    /**
     * 3天
     */
    const THREE_DAY = 259200;

    /**
     * 4天
     */
    const FOUR_DAY = 345600;

    /**
     * 5天
     */
    const FIVE_DAY = 432000;

    /**
     * 7天
     */
    const SEVEN_DAY = 604800;

    /**
     * 10天
     */
    const TEN_DAY = 864000;

    /**
     * 15天
     */
    const FIFTEEN_DAY = 1296000;
    /**
     * 30天或者1个月
     */
    const ONE_MONTH = 2592000;

    /**
     * 40天
     */
    const FORTY_DAY = 3456000;

    /**
     * 3个月或者90天
     */
    const THREE_MONTH = 7776000;

    /**
     * 6个月半年或180天
     */
    const SIX_MONTH = 15552000;

    /**
     * 一年365天
     */
    const ONE_YEAR = 31536000;

    /**
     * 获取Redis句柄
     * @return bool|mixed
     */
    protected static function _handle()
    {

        if (empty(self::$redisCodisHandel)) {
            try {
                self::$redisCodisHandel = Cache::store()->handler();
            } catch (Exception $e) {
                self::$redisCodisHandel = false;
            }
        }
        return;
    }

    public function __call($name, $arguments)
    {
        return self::__callStatic($name, $arguments);
    }

    public static function __callStatic($name, $arguments)
    {
        self::_handle();
        $reConnect = false;
        while (1) {
            if ( ! empty(self::$redisCodisHandel) && method_exists(self::$redisCodisHandel, $name)) {
                try {
                    $res = call_user_func_array([
                        self::$redisCodisHandel,
                        $name,
                    ], $arguments);

                    return $res;
                } catch (Exception $e) {
                    if ($reConnect) {
                        return false;
                    }
                    self::$redisCodisHandel = null;
                    self::_handle();
                    $reConnect = true;
                    continue;
                }
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * 获取键值,第一个参数是键值,其他参数跟随
     */
    public static function getKey()
    {
        $arr    = func_get_args();
        $arr[0] = self::CACHE_PRE . $arr[0];
        return call_user_func_array('sprintf', $arr);
    }
}
