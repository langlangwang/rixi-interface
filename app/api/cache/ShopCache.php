<?php


namespace app\api\cache;


class ShopCache extends RedisCache
{
    //店铺信息
    const SHOP_INFO = 'shop_info_%s';

    /**
     * 店铺信息
     * @param $id
     * @param $data
     * @param int $ttl
     * @return bool
     */
    public static function setShopInfo($id, $data, $ttl = self::ONE_DAY)
    {
        is_array($data) && $data = json_encode($data, true);
        $key = self::getKey(self::SHOP_INFO, $id);
        return self::set($key, $data, $ttl);
    }

    /**
     * 删除用户缓存
     * @param $id
     * @return bool
     */
    public static function delShopInfo($id)
    {
        $key = self::getKey(self::SHOP_INFO, $id);
        return self::del($key);
    }

    /**
     * 获取用户信息
     * @param $id
     * @return array|mixed|string
     */
    public static function getShopInfo($id)
    {
        $key = self::getKey(self::SHOP_INFO, $id);
        $data = self::get($key);
        return is_array($data) ? $data : json_decode($data, true);
    }


}