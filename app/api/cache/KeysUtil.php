<?php
namespace app\api\cache;

class KeysUtil
{
    static $project       = 'rx:';
    static $communityName = 'community';
    static $memberName    = 'member';

    static $carefreeName    = 'carefree';

    static $packsName = 'packs';

    static $OrderName = 'order';

    static $configName = 'config';

    public static function getCommunityPayListKey($type): string
    {
        return self::$project . self::$communityName . ":pay:list:type:" . $type;
    }

    public static function getCommunityInfo($communityId): string
    {
        return self::$project . self::$communityName . ":info:" . $communityId;
    }

    public static function getCommunityConfig($key): string
    {
        return self::$project . self::$communityName . ":config:" . $key;
    }

    public static function getCommunityActivityKey($key): string
    {
        return self::$project . self::$communityName . ":activity:" . $key;
    }

    public static function getMemberInfoKey($userId): string
    {
        return self::$project . self::$memberName . ":info:" . $userId;
    }

    public static function getCommunityActivityListKey($day): string
    {
        return self::$project . self::$communityName . ":activity:list:" . $day;
    }

    public static function getCommunityMeetListKey($activityId): string
    {
        return self::$project . self::$communityName . ":meet:list:" . $activityId;
    }

    public static function getCommunityMeetTimeKey(): string
    {
        return self::$project . self::$communityName . ":meet:time";
    }

    public static function getCommunityOrderPayingKey($userId): string
    {
        return self::$project . self::$communityName . ":order:paying:" . $userId;
    }

    public static function getCommunityOrderKey($orderNo): string
    {
        return self::$project . self::$communityName . ":order:info:" . $orderNo;
    }

    //社区存在未支付订单
    public static function getCommunityInPayOrderKey($communityId): string
    {
        return self::$project . self::$communityName . ":ordering:info:" . $communityId;
    }

    //用户存在未支付订单
    public static function getCommunityUserInPayOrderKey($userId): string
    {
        return self::$project . self::$communityName . ":ordering:user:" . $userId;
    }

    //占位中
    public static function getCommunitySeizeKey($communityId): string
    {
        return self::$project . self::$communityName . ":order:seize:" . $communityId;
    }

    //token
    public static function getMemberInfoTokenKey($userId, $prefix = ''): string
    {
        if (empty($prefix)) {
            return self::$project . self::$memberName . ":info:token" . $userId;
        } else {
            return self::$project . $prefix . ":info:token" . $userId;
        }
    }

    //社包分佣比例
    public static function getPacksConfigKey($key): string
    {
        return self::$project . self::$packsName . ":config:" . $key;
    }

    //回调订单
    public static function getOrderCallbackKey($orderNo): string
    {
        return self::$project . self::$OrderName . ":callback:" . $orderNo;
    }

    public static function getSystemConfigKey($key): string
    {
        return self::$project . self::$configName . ":" . $key;
    }

    //新人专区锁
    public static function getCommunityNewLockKey(): string
    {
        return self::$project . self::$communityName . ":new:lock";
    }

    //预约专区
    public static function getCommunityPrepayLockKey(): string
    {
        return self::$project . self::$communityName . ":prepay:lock";
    }

    //用户账号管理
    public static function getMemberAccountKey($userId): string
    {
        return self::$project . self::$memberName . ":account:" . $userId;
    }

    public static function getCommunityBusinessKey(): string
    {
        return self::$project . self::$communityName . "community:business";
    }

    //社区任务
    public static function getCommunityTaskKey($userId): string
    {
        return self::$project . self::$communityName . ":community:task:" . $userId;
    }

    public static function getCommunitySignLockKey($time): string
    {
        return self::$project . self::$communityName . ":community:sign:lock:".$time;
    }

    public static function getCommunitySignList(): string
    {
        return self::$project . self::$communityName . ":community:sign:list";
    }

    public static function getCommunitySignUserKey($userId): string
    {
        return self::$project . self::$communityName . ":community:sign:user:".$userId;
    }

    public static function getCommunitySignIncrKey($time): string
    {
        return self::$project . self::$communityName . ":community:sign:incr:".$time;
    }

    public static function getCarefreeConfigKey($key): string
    {
        return self::$project . self::$carefreeName . ":config:".$key;
    }

}
