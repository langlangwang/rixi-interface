<?php

namespace app\api\cache;

use app\common\models\Member\Account;
use app\common\utils\CommonUtil;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class MemberAccountCache extends RedisCache
{

    /**
     * @throws ModelNotFoundException
     * @throws DbException
     * @throws DataNotFoundException
     */
    public static function getKeyData($userId)
    {
        $data = self::get(KeysUtil::getMemberAccountKey($userId));
        if (empty($data)) {
            $where = [
                ['user_id', '=', $userId],
            ];
            $account = Account::getInstance()->where($where)->find();
            $data = $account ? CommonUtil::camelToUnderLine($account->toArray()) : [];
            if (!empty($data)) {
                $data = json_encode($data, 256);
                self::set(KeysUtil::getMemberAccountKey($userId), $data, 60 * 5);
            }
        }
        return $data ? json_decode($data, true) : [];
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public static function setKeyData($userId): bool
    {
        $where = [
            ['user_id', '=', $userId],
        ];
        $account = Account::getInstance()->where($where)->find();
        $data = $account ? CommonUtil::camelToUnderLine($account->toArray()) : [];
        if (!empty($data)) {
            $data = json_encode($data, 256);
            self::set(KeysUtil::getMemberAccountKey($userId), $data, 60 * 5);
        }
        return true;
    }
}