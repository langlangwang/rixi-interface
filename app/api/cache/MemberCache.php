<?php


namespace app\api\cache;


class MemberCache extends RedisCache
{
    //登录错误次数
    const LOGIN_ERROR_COUNT = 'login_error_count_%s';

    const USER_CODE = 'user:code:%s:%s';
    //用户信息
    const USER_INFO = 'user:info:%s';

    const TEST_CACHE = 'test_uid_%s';

    const     session_key = 'test_uid_%s';

    public static function setTest($userId)
    {
        $key = self::getKey(self::TEST_CACHE, $userId);
        return self::set($key, 1, self::ONE_MINUTE);
    }

    /**
     * 设置用户信息
     * @param $id
     * @param $data
     * @param int $ttl
     * @return bool
     */
    public static function setUserInfo($id, $data, int $ttl = self::TWO_HOUR): bool
    {
        is_array($data) && $data = json_encode($data, true);
        $key = self::getKey(self::USER_INFO, $id);
        return self::set($key, $data, $ttl);
    }

    /**
     * 删除用户缓存
     * @param $id
     * @return bool
     */
    public static function delUserInfo($id)
    {
        $key = self::getKey(self::USER_INFO, $id);
        return self::del($key);
    }

    /**
     * 获取用户信息
     * @param $id
     * @return array|mixed|string
     */
    public static function getUserInfo($id)
    {
        $key = self::getKey(self::USER_INFO, $id);
        $data = self::get($key);
        return is_array($data) ? $data : json_decode($data, true);
    }

    /**
     * 错误次数
     * @param $username
     * @param int $num
     * @return bool
     */
    public static function incrLoginErrorCount($username, $num = 1)
    {
        $key = self::getKey(self::LOGIN_ERROR_COUNT, $username);
        self::incrby($key, $num);
        return self::expire($key, self::ONE_DAY);
    }

    /**
     * 获取错误次数
     * @param $username
     * @return string
     */
    public static function getLoginErrorCount($username)
    {
        $key = self::getKey(self::LOGIN_ERROR_COUNT, $username);
        return self::get($key);
    }

    /**
     * 设置code
     * @param $phone
     * @param $type
     * @param $code
     * @return mixed
     */
    public static function setUserCode($phone, $type, $code)
    {
        return self::set(self::getKey(self::USER_CODE, $type, $phone), $code, self::FIVE_MINUTE);
    }

    /**
     * 获取code
     * @param $phone
     * @param $type
     * @param $code
     * @return mixed
     */
    public static function getUserCode($phone, $type)
    {
        return self::get(self::getKey(self::USER_CODE, $type, $phone));
    }

}