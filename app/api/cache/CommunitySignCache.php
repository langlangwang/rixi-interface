<?php


namespace app\api\cache;


class CommunitySignCache extends RedisCache
{
    const IS_SIGN = 'is:sign:%s';

    /**
     * @param $uid
     * @return string
     */
    public static function getSign($uid): string
    {
        $key = self::getKey(self::IS_SIGN, $uid);
        return self::get($key);
    }

    /**
     * @param $uid
     * @param int $ttl
     * @return string
     */
    public static function setSign($uid, int $ttl = self::FIVE_MINUTE): string
    {
        $key = self::getKey(self::IS_SIGN, $uid);
        return self::set($key, 1, $ttl);
    }
}