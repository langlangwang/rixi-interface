<?php


namespace app\api\cache;


class CommunityCache extends RedisCache
{
    const SYS_ARTICLE = 'sys:system:config:%s';

    const COMMUNITY_GOODS = 'community:goods';

    const COMMUNITY_LOCK = 'community:lock:%s';

    /**
     * 添加缓存
     * @param $id
     * @param $data
     * @param int $ttl
     * @return bool
     */
    public static function setCommunityLock($id, $data, int $ttl = self::TWO_MINUTE): bool
    {
        $key = self::getKey(self::COMMUNITY_LOCK, $id);
        return self::set($key, $data, $ttl);
    }

    /**
     * 删除用户缓存
     * @param $id
     * @return bool
     */
    public static function delCommunityLock($id)
    {
        $key = self::getKey(self::COMMUNITY_LOCK, $id);
        return self::del($key);
    }

    /**
     * 获取缓存
     * @param $id
     * @return string
     */
    public static function getCommunityLock($id)
    {
        $key = self::getKey(self::COMMUNITY_LOCK, $id);
        return self::get($key);
    }

    /**
     * 添加缓存
     * @param $id
     * @param $data
     * @param int $ttl
     * @return bool
     */
    public static function setConfigInfo($id, $data, int $ttl = self::ONE_MINUTE): bool
    {
        is_array($data) && $data = json_encode($data, true);
        $key = self::getKey(self::SYS_ARTICLE, $id);
        return self::set($key, $data, $ttl);
    }



    /**
     * 获取缓存
     * @param $id
     * @return array|mixed|string
     */
    public static function getConfigInfo($id)
    {
        $key = self::getKey(self::SYS_ARTICLE, $id);
        $data = self::get($key);
        return is_array($data) ? $data : json_decode($data, true);
    }


    /**
     * 添加缓存
     * @param $data
     * @param int $ttl
     * @return bool
     */
    public static function setGoodsInfo($data, int $ttl = self::ONE_MINUTE): bool
    {
        is_array($data) && $data = json_encode($data, true);
        $key = self::getKey(self::COMMUNITY_GOODS);
        return self::set($key, $data, $ttl);
    }

    /**
     * 获取缓存
     * @return array|mixed|string
     */
    public static function getGoodsInfo()
    {
        $key = self::getKey(self::COMMUNITY_GOODS);
        $data = self::get($key);
        return is_array($data) ? $data : json_decode($data, true);
    }

}