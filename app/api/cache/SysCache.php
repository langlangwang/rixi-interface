<?php
namespace app\api\cache;

class SysCache extends RedisCache
{

    const TAB_KEY = 'app_tab_menu';

    //文章详情
    const SYS_ARTICLE = 'sys_article_%s';

    /**
     * 添加缓存
     * @param $id
     * @param $data
     * @param int $ttl
     * @return bool
     */
    public static function setArticleInfo($id, $data, int $ttl = self::ONE_MINUTE): bool
    {
        is_array($data) && $data = json_encode($data, true);
        $key                     = self::getKey(self::SYS_ARTICLE, $id);
        return self::set($key, $data, $ttl);
    }

    /**
     * 删除缓存
     * @param $id
     * @return bool
     */
    public static function delArticleInfo($id): bool
    {
        $key = self::getKey(self::SYS_ARTICLE, $id);
        return self::del($key);
    }

    /**
     * 获取缓存
     * @param $id
     * @return array|mixed|string
     */
    public static function getArticleInfo($id)
    {
        $key  = self::getKey(self::SYS_ARTICLE, $id);
        $data = self::get($key);
        return is_array($data) ? $data : json_decode($data, true);
    }

    /**
     * [tabMenu description]
     * \app\api\cache\SysCache::tabMenu();
     * @return [type] [description]
     */
    public static function tabMenu()
    {
        $key = self::getKey(self::TAB_KEY);
        // var_dump($key);exit;
        $data = self::get($key);
        $data = is_array($data) ? $data : json_decode($data, true);
        if (empty($data)) {
            $val = \think\facade\Db::table('system_config')
                ->where('type', 'base')
                ->where('name', self::TAB_KEY)
                ->value('value');
            if ( ! empty($val)) {
                self::set($key, $val, 8640000);
            }
            return empty($val) ? [
                [
                    'key'              => 'index',
                    'pagePath'         => '/pages/index/index',
                    'iconPath'         => '/static/tabar/index.png',
                    'selectedIconPath' => '/static/tabar/index_xz.png',
                    'text'             => '首页',
                ],
                [
                    'key'              => 'community',
                    'pagePath'         => '/pages/community/index',
                    'iconPath'         => '/static/tabar/shequ.png',
                    'selectedIconPath' => '/static/tabar/shequ_xz.png',
                    'text'             => '社区',
                ],
                [
                    'key'              => 'commercial_college',
                    'pagePath'         => '/pages/business/index',
                    'iconPath'         => '/static/tabar/sxy.png',
                    'selectedIconPath' => '/static/tabar/sxy_xz.png',
                    'text'             => '商学院',
                ],
                [
                    'key'              => 'me',
                    'pagePath'         => '/pages/user/index',
                    'iconPath'         => '/static/tabar/user.png',
                    'selectedIconPath' => '/static/tabar/user_xz.png',
                    'text'             => '我的',
                ],
            ] : json_decode($val, true);
        }
        return $data;
    }
}
