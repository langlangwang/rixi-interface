<?php

namespace app\api\consDir;

class OrderPrefixConst
{
    //杉德转账
    const TR = 'TR';

    //杉德转账确认
    const TC = 'TC';

    //杉德消费退款
    const RF = 'RF';

    //充值
    const RC = 'RC';
}