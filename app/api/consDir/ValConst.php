<?php

namespace app\api\consDir;

/**
 * 错误类
 */
class ValConst
{
    const COMMUNITY_WITHDRAW_CHARGE_PERCENT = 8;

    const TASK_ORDER_SHOP_PERCENT = 0.02;
}
