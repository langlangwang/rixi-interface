<?php
namespace app\api\consDir;

class SupplyConst
{
    // https://sce-opc.changecloud.cn/#/passport/document
    const DOMAIN = 'https://sce-opz.changecloud.cn';
    //获取省市区地址
    const GET_REGION_BY_CODE_OPEN = '/scce/pbc/pbc/region/getRegionByCodeOpen';
    //删除消息
    const REMOVE_MESSAGE_POOL_BY_PARAM = '/scce/opm/opm/messagePool/removeMessagePoolByParam';
    //查询消息池表
    const MESSAGE_POOL_LIST = '/scce/opm/opm/messagePool/list/page';
    //获取品牌列表
    const GET_BRANDS = '/scce/cmc/cmc/spu/open/getBrands';
    //获取分类信息
    const GET_CATEGORYS = '/scce/cmc/cmc/spu/open/getCategorys';

    //查询商品库SPUID列表
    const GET_SPU_ID_LIST = '/scce/cmc/cmc/spu/open/getSpuIdList';
    //查询SPU商品详情
    const GET_SPU_BY_SPUIDS = '/scce/cmc/cmc/spu/open/getSpuBySpuIds';
    //查询SKU规格信息
    const LIST_SKU_BY_SPU_ID = '/scce/cmc/cmc/spu/open/listSkuBySpuId';
    //查询商品运费
    const FREIGHT_CALCULATE = '/scce/cmc/cmc/freight/calculate';
    //查询商品价格
    const FIND_SKU_SALE_PRICE = '/scce/cmc/cmc/spu/open/goods/findSkuSalePrice';
    //查询商品库存
    const FIND_SKU_STOCK = '/scce/cmc/cmc/spu/open/findSkuStock';

    //批量校验下单商品销售范围和库存
    const CHECK_SKU_STOCK = '/scce/cmc/cmc/spu/open/checkSkuStock';
    //创建订单
    const SUBMIT_ORDER = '/scce/ctc/ctc/reseller/order/submitOrder';
    //根据外部订单号查询订单详情列表
    const GET_ORDER_DETAIL_LIST = '/scce/ctc/openApi/reseller/order/getOrderDetailList';
    //查询订单详情
    const GET_ORDER_DETAIL = '/scce/ctc/ctc/reseller/order/getOrderDetail';
    //查询物流信息
    const GET_ORDER_EXPRESS_LIST = '/scce/ctc/ctc/tOrderExpress/getOrderExpressListByOs';
    //确认收货
    const CONFIRM_RECEIVE = '/scce/ctc/ctc/reseller/order/confirmReceive';
    //售后申请
    const APPLY_REFUND = '/scce/ctc/ctc/reseller/orderReturnGoods/applyRefund';
    //退货/换货邮寄卖家
    const SEND_RETURN_GOODS = '/scce/ctc/ctc/reseller/orderReturnGoods/sendReturnGoods';
    //换货确认收货
    const RECEIPT_REFUND = '/scce/ctc/ctc/reseller/orderReturnGoods/receiptRefund';
    //取消售后申请
    const CANCEL_REFUND = '/scce/ctc/ctc/reseller/orderReturnGoods/cancelRefund';
    //查询售后单详细信息
    const ORDER_RETURN_GOODS = '/scce/ctc/ctc/reseller/orderReturnGoods/detail';

}
