<?php

namespace app\api\consDir;

class SandConst
{
    const PREFIX = 'SD';

    const FRONT_URL = 'https://rx-h5.rxx1316.com/#/pages/sdCallback';
    const TEST_FRONT_URL = 'https://rx-test-h5.rxx1316.com/#/pages/sdCallback';

    const DOMAIN = 'https://openapi.sandpay.com.cn';
    const TEST_DOMAIN = 'https://openapi-uat01.sand.com.cn';

    //会员钱包管理页入口
    const ACCOUNT_ONE_KEY = '/v4/sd-wallet/api/m-wallet/account.onekey.access';

    //会员账户余额查询
    const ACCOUNT_BALANCE_QUERY = '/v4/sd-wallet/api/m-wallet/account.balance.query';

    //会员信息查询
    const ACCOUNT_MEMBER_QUERY = '/v4/sd-wallet/api/m-wallet/account.member.query';

    //会员状态管理
    const ACCOUNT_MEMBER_STATUS_MANAGE = '/v4/sd-wallet/api/m-wallet/account.member.status.manage';

    //转账
    const TRANS_TRANSFER = '/v4/sd-wallet/api/trans/trans.transfer';

    //转账确认
    const TRANS_TRANSFER_CONFIRM = '/v4/sd-wallet/api/trans/trans.transfer.confirm';

    //消费
    const TRANS_ORDER_CREATE = '/v4/sd-receipts/api/trans/trans.order.create';

    //会员绑卡查询
    const ACCOUNT_BIND_CARD_QUERY = '/v4/sd-wallet/api/m-wallet/account.bind.card.query';

    //提现
    const TRANS_WITHDRAW = '/v4/sd-wallet/api/trans/trans.withdraw';

    //统一下单接口（主扫）
    const TRANS_ORDER_CREATE_SCAN = '/v4/sd-receipts/api/trans/trans.order.create';

    //消费订单查询
    const TRANS_ORDER_QUERY = '/v4/sd-receipts/api/trans/trans.order.query';

    //订单查询
    const WALLET_ORDER_QUERY = '/v4/sd-wallet/api/trans/trans.order.query';

    //变动明细
    const DETAIL_QUERY = '/v4/sd-wallet/api/m-wallet/account.change.detail.query';

    //付款
    const PAYMENT_ORDER_CREATE = '/v4/sd-payment/api/trans/trans.payment.order.create';

    //付款订单查询
    const PAYMENT_ORDER_QUERY = '/v4/sd-payment/api/trans/trans.payment.order.query';

    //消费退款
    const TRANS_ORDER_REFUND = '/v4/sd-receipts/api/trans/trans.order.refund';

    //资金冻结
    const TRANS_FUND_FREEZE = '/v4/sd-wallet/api/trans/trans.fund.freeze';

    //资金解冻
    const TRANS_FUND_UNFREEZE = '/v4/sd-wallet/api/trans/trans.fund.unfreeze';
}