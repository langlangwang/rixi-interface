<?php

namespace app\api\consDir;

/**
 * 错误类
 */
class ErrorConst
{
    const BASE_ERROR = 200;
    const BASE_ERROR_MSG = '错误';

    const NOT_LOGIN_ERROR = 401;
    const NOT_LOGIN_ERROR_MSG = '未登录';

    const SUCCESS_CODE = 0;
    const SUCCESS_CODE_MSG = 'success';

    const PARAM_ERROR = 201;
    const PARAM_ERROR_MSG = '参数错误';

    const FREQUENT_ERROR = 202;
    const FREQUENT_ERROR_MSG = '请求频繁';


    const SYSTEM_ERROR = 203;
    const SYSTEM_ERROR_MSG = '系统错误';


    const QUERY_NOT_EXIST = 204;
    CONST QUERY_NOT_EXIST_MSG = '查询不存在';


    const LOGIN_ERROR = 10001;
    const LOGIN_ERROR_MSG = '请先注册';

    const LOGIN_ERROR_COUNT = 10002;
    const LOGIN_ERROR_COUNT_MSG = '输入错误过多,请明天再试';

    const COUPON_ERROR = 10003;
    const COUPON_ERROR_MSG = '优惠券错误';

    const COUPON_NO_ERROR = 10004;
    const COUPON_NO_ERROR_MSG = '优惠券未达到使用条件';


    const PHONE_NOT_EXIST = 20001;
    const PHONE_NOT_EXIST_MSG = '手机号码不存在';

    const CODE_ERROR = 20002;
    const CODE_ERROR_MSG = '验证码错误';

    const ACCOUNT_ERROR = 20003;
    const ACCOUNT_ERROR_MSG = '账号已被绑定';

    const IDC_ERROR = 20004;
    const IDC_ERROR_MSG = '身份证已被绑定';

    const ADD_SHOP_ERROR = 20005;
    const ADD_SHOP_ERROR_MSG = '您已经提交过店铺';

    const NO_SHOP_ERROR = 20006;
    const NO_SHOP_ERROR_MSG = '店铺不存在';

    const NO_GOODS_ERROR = 20007;
    const NO_GOODS_ERROR_MSG = '商品不存在或已下架';

    const NO_SKU_ERROR = 20008;
    const NO_SKU_ERROR_MSG = '规格不存在或已下架';

    const NO_ADDRESS_ERROR = 20009;
    const NO_ADDRESS_ERROR_MSG = '地址不存在';

    const NO_ORDER_ERROR = 20010;
    const NO_ORDER_ERROR_MSG = '订单不存在或者已过期';

    const ORDER_STATUS_ERROR = 20011;
    const ORDER_STATUS_ERROR_MSG = '订单状态错误';

    const WECHAT_CODE_ERROR = 20012;
    const WECHAT_CODE_ERROR_MSG = '微信获取code错误';

    const PRICE_ERROR = 20013;
    const PRICE_ERROR_MSG = '商品金额有变动，请刷新后再试';

    const NO_PRICE_ERROR = 20014;
    const NO_PRICE_ERROR_MSG = '余额不足';

    const ORDER_TIME_ERROR = 20014;
    const ORDER_TIME_ERROR_MSG = '订单超过30分钟无法取消';

    const ORDER_REFUND_ERROR = 20015;
    const ORDER_REFUND_ERROR_MSG = '退款失败，请联系客服处理';

    const WITHDRAW_ERROR = 20016;
    const WITHDRAW_ERROR_MSG = '提现失败';

    const COMMUNITY_IN_PAY_ERROR = 20017;
    const COMMUNITY_IN_PAY_ERROR_MSG = '社区正在被竞拍中，请更换社区尝试';

    const COMMUNITY_IN_NOT_ERROR = 20018;
    const COMMUNITY_IN_NOT_MSG = '社区抢购未开始';

    const HAVE_SIGN_ERROR = 20019;
    const HAVE_SIGN_ERROR_MSG = '请先使用红包之后再签到';

    const SIGN_IN_NOT_ERROR = 20020;
    const SIGN_IN_NOT_MSG = '签到未开始';

    const NO_SIGN_ERROR = 20021;
    const NO_SIGN_ERROR_MSG = '没有签到名额了';

    const ORDER_TIME_OUT_ERROR = 20022;
    const ORDER_TIME_OUT_ERROR_MSG = '订单超时';

    const IS_SERVICE_ERROR = 20023;
    const IS_SERVICE_ERROR_MSG = '您已经是服务商了，请勿重复支付';


    const PHONE_ERROR = 20024;
    const PHONE_ERROR_MSG = '手机号已注册，请直接登录';

    const COUPON_NOT_ERROR = 20025;
    const COUPON_NOT_ERROR_MSG = '优惠券失效或者已被使用请重新下单';

    const CAN_RUSH_ERROR = 20026;
    const CAN_RUSH_ERROR_MSG = '今日已无竞拍次数，可通过升级会员等级或推广会员获得更多竞拍次数';

    const RE_PASSWORD_ERROR = 20027;
    const RE_PASSWORD_ERROR_MSG = '两次输入密码不一致';

    const ACCOUNT_PASSWORD_ERROR = 20028;
    const ACCOUNT_PASSWORD_ERROR_MSG = '账号或密码错误';

    const PHONE_RESET_ERROR = 20029;
    const PHONE_RESET_ERROR_MSG = '重置密码失败';

    const PHONE_WRITE_ERROR = 20030;
    const PHONE_WRITE_ERROR_MSG = '手机号码填写错误';

    const EXSIT_ORDER_ERROR = 20031;
    const EXSIT_ORDER_ERROR_MSG = '订单已存在';

    const PAY_PASSWORD_ERROR = 20032;
    const PAY_PASSWORD_ERROR_MSG = '支付密码错误';

    const NO_PAY_PASSWORD_ERROR = 20033;
    const NO_PAY_PASSWORD_ERROR_MSG = '请先设置支付密码';

    const MEETING_ERROR = 20034;
    const MEETING_ERROR_MSG = '会议不存在';

    const PASSWORD_EMPTY_ERROR = 20035;
    const PASSWORD_EMPTY_ERROR_MSG = '密码不能为空';

    const NEW_COUNT_ERROR = '20036';
    const NEW_COUNT_ERROR_MSG = '只能新手抢购';

    const NO_COMMUNITY_ERROR = '20037';
    const NO_COMMUNITY_ERROR_MSG = '该社区不可抢购';

    const EXCEPTION_ERROR = '20038';
    const EXCEPTION_ERROR_MSG = '异常';

    const COMMUNITY_PAY_IN_ERROR = '20039';
    const COMMUNITY_PAY_IN_ERROR_MSG = '存在支付中的订单';

    const NO_BIND_ALIPAY_ERROR = '20040';
    const NO_BIND_ALIPAY_ERROR_MSG = '未绑定支付宝';

    const PASSWORD_ERROR = '20041';
    const PASSWORD_ERROR_MSG = '密码输入错误';

    const MEETING_NO_START_ERROR = '20042';
    const MEETING_NO_START_ERROR_MSG = '请稍后再试，会议还未开始';

    const MEETING_END_ERROR = '20043';
    const MEETING_END_ERROR_MSG = '该场会议已结束，请关注其它活动场';

    const MEETING_FINISH_ERROR = '20044';
    const MEETING_FINISH_ERROR_MSG = '该场会议所有社区已被竞拍完，请关注平台其它活动场';

    const MEETING_NO_TIME_ERROR = '20045';
    const MEETING_NO_TIME_ERROR_MSG = '未设置时间';

    const WITHDRAW_NUM_DAY_LIMIT_ERROR = '20046';
    const WITHDRAW_NUM_DAY_LIMIT_ERROR_MSG = '每日提现次数限制';

    const WITHDRAW_MAX_MONEY_LIMIT_ERROR = '20047';
    const WITHDRAW_MAX_MONEY_LIMIT_ERROR_MSG = '每次最大提现金额';

    const WITHDRAW_MIN_MONEY_LIMIT_ERROR = '20048';
    const WITHDRAW_MIN_MONEY_LIMIT_ERROR_MSG = '每次最小提现金额';

    const WITHDRAW_MAX_CAN_DAY_ERROR = '20049';
    const WITHDRAW_MAX_CAN_DAY_ERROR_MSG = '每日最多可提现金额';

    const PACKS_NO_EXIST_ERROR = '20050';
    const PACKS_NO_EXIST_ERROR_MSG = '礼包不存在';

    const PACKS_NO_ADDRESS_ERROR = '20051';
    const PACKS_NO_ADDRESS_ERROR_MSG = '快递信息不存在';

    const PACKS_CONFIRM_ERROR = '20052';
    const PACKS_CONFIRM_ERROR_MSG = '确认收货失败';

    const REG_ERROR = '20053';
    const REG_ERROR_MSG = '注册失败';

    const PHONE_EXIST_ERROR = 20054;
    const PHONE_EXIST_ERROR_MSG = '该手机号已注册';

    const REAL_NAME_ERROR = 20055;
    const REAL_NAME_ERROR_MSG = '未实名认证，请先实名认证';

    const REAL_NAME_NO_ERROR = 20056;
    const REAL_NAME_NO_ERROR_MSG = '支付宝真实姓名与实名认证实名不一致';

    const TRANSFER_NOT_TIME_ERROR = 20057;
    const TRANSFER_NOT_TIME_ERROR_MSG = '未到转让时间';

    const BUY_NOT_MYSELF_ERROR = 20058;
    const BUY_NOT_MYSELF_ERROR_MSG = '不能买自己的社区';

    const SAND_NO_BIND_ERROR = 20059;
    const SAND_NO_BIND_ERROR_MSG = '未开通电子钱包';

    const TASK_GET_GXZ_ERROR = 20060;
    const TASK_GET_GXZ_ERROR_MSG = '已达发放次数';

    const TASK_SIGN_IN_ERROR = 20061;
    const TASK_SIGN_IN_ERROR_MSG = '今天已签到';

    const STATUS_ERROR = 20062;
    const STATUS_ERROR_MSG = '社区状态错误';

    const OP_PAY_LIMIT_ERROR = 20070;
    const OP_PAY_LIMIT_ERROR_MSG = '不能频繁支付';

}
