<?php

namespace app\api\consDir;

class CommunityConst
{
    /**
     * community_status 社区状态 0可抢购 1已释放  3被抢占 4待经营  6考核中 7被领取 8永久社区
     */

    /**
     * 可抢购
     */
    const CAN_STATUS = 0;

    /**
     * 已释放
     */
    const RELEASE_STATUS = 1;

    /**
     * 被抢占
     */
    const BUY_STATUS = 3;

    /**
     * 待经营
     */
    const OP_STATUS = 4;

    /**
     * 考核中
     */
    const CHECK_STATUS = 6;

    /**
     * 被领取
     */
    const GET_STATUS = 7;

    /**
     * 永久社区
     */
    const PERMANENT_STATUS = 8;


    /**
     *  lock_status 1锁定 2解锁
     */

    /**
     * 锁定
     */
    const LOCK = 1;

    /**
     * 解锁
     */
    const UNLOCK = 2;



    /**
     * 是否支付服务费 1是 0否
     */

    /**
     * 已支付服务费
     */
    const PAY_SERVICE = 1;

    /**
     * 未支付服务费
     */
    const NO_PAY_SERVICE = 0;


    /**
     * 是否是vip
     */

    /**
     * 是VIP
     */
    const VIP_YES = 1;

    /**
     * 不是VIP
     * */
    const VIP_NO = 0;

    /**
     * 签到状态 1未使用 2入驻已使用 3已返还到余额
     */

    /**
     * 未使用
     */
    const SIGN_UNUSERED = 1;

    /**
     * 入驻已使用
     */
    const SIGN_USERED = 2;

    /**
     * 已返还到余额
     */
    const SIGN_RETURN = 3;


    /**
     * 订单状态 1未支付 2已支付 3取消
     */

    /**
     * 订单未支付
     */
    const ORDER_NO_PAY = 1;

    /**
     * 订单已支付
     */
    const ORDER_PAY = 2;

    /**
     * 订单取消
     */
    const ORDER_CANCEL = 3;

    /**
     * 广场专区
     */
    const TYPE_ZERO = 0;

    /**
     * 新人专区
     */
    const TYPE_ONE = 1;

    /**
     * 会议专区
     */
    const TYPE_TWO = 2;


}