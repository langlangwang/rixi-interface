<?php

namespace app\api\logic;

use app\api\consDir\ErrorConst;
use app\api\consDir\UrlConst;
use app\api\services\GoodsService;
use app\api\services\MemberService;
use app\api\services\StoreService;
use app\common\libs\Singleton;
use app\common\models\Shop\GoodsComment;
use app\common\utils\CommonUtil;

/**
 * 门店模块
 * Class MemberLogic
 * @package app\api\logic
 */
class ShopLogic1 extends BaseLogic
{
    use Singleton;

    public function shopArea($pId){

    }

    public function shopdetail($shopId){
        $shopInfo = StoreService::getInstance()->shopDetail($shopId);

    }

    public function searchHistory(): array
    {
        $data['hot'] = [
            '女装',
            '男装',
            '保健品',
        ];
        return $data;
    }

    public function addComment($content,$goodsId){
        $userInfo = MemberService::getInstance()->getUserInfo($this->userinfo->id);
        //nickname,headimgurl,content,createtime

        $goodsInfo = GoodsComment::getInstance()
            ->where(['goodsid' => $goodsId,'openid' => $userInfo['openid']])
            ->find();
        if(empty($goodsInfo)){
            $data = [
                'uniacid' => 2,
                'goodsid' => $goodsId,
                'openid' => $userInfo['openid'],
                'createtime' => time(),
                'nickname' => $userInfo['nickname'],
                'headimgurl' => $userInfo['headimgurl'],
                'content' => $content,
            ];
            GoodsComment::getInstance()->insert($data);
        }else{
            $data = [
                'nickname' => $userInfo['nickname'],
                'headimgurl' => $userInfo['headimgurl'],
                'content' => $content,
            ];
            GoodsComment::getInstance()->where(['id' => $goodsInfo['id']])->update($data);
        }
        return true;
    }

    /**
     * 商品评论列表
     * @param $goodsId
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function goodsComment($goodsId, $page, $pageSize){
        $info['rows'] = GoodsService::getInstance()->goodsComment($goodsId, $page, $pageSize);
        return $info;
    }

    /**
     * 商品详情
     * @param $goodsId
     * @return array
     */
    public function goodsDetail($goodsId): array
    {
        $field = 'id,title,content,thumb,status,productprice,marketprice,productsn,
        issendfree,total,thumb_url,issendfree,iscomment,province,city,merchid,video,
        dispatchid,hasoption,goodssn,weight,productsn,total';
        $info = StoreService::getInstance()->goodsDetail($goodsId,$field);
        if(empty($info)){
            CommonUtil::throwException(ErrorConst::QUERY_NOT_EXIST, ErrorConst::QUERY_NOT_EXIST_MSG);
        }
        $info['comment'] = GoodsService::getInstance()->goodsComment($goodsId);
        if($info['hasoption']){
            $info['spec'] = StoreService::getInstance()->goodsSpec($goodsId);
            $info['option'] = StoreService::getInstance()->goodsSku($goodsId);
        }

        return $info;
    }

    /**
     * 商品列表
     * @param $pcate
     * @param $ccate
     * @param $tcate
     * @param $title
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function goodsList($pcate, $ccate, $tcate, $title, $page, $pageSize): array
    {
        $field = 'id,title,productprice,marketprice,thumb';
        $list = StoreService::getInstance()->goodsList($pcate, $ccate, $tcate, $title, $page, $pageSize,$field);
        foreach ($list as &$val) {
            $val['thumb'] = UrlConst::IMG_URL.$val['thumb'];
        }
        return ['rows' => $list];
    }

    /**
     * 分类列表
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function categoryList($page,$pageSize): array
    {
        $field = 'id,name';
        $list = StoreService::getInstance()->categoryList($page,$pageSize,$field);
        return ['rows' => $list];
    }

    /**
     * 门店列表
     * @param $lat
     * @param $lng
     * @param $city
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function shopList($lat,$lng,$city,$page, $pageSize,$recommend,$hot,$groupId){
        $field = 'id,merchname,address,lat,lng';
        $list = StoreService::getInstance()->storeList($city,$page,$pageSize,$field,$recommend,$hot,$groupId);
        foreach ($list as &$val){
            $val['km'] = intval(StoreService::getInstance()->getS($lat,$val['lat'],$lng,$val['lng']));
            $val['star'] = 5;
            $val['amount'] = 100;

        }
        return ['rows' => $list];
    }
}