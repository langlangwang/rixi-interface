<?php

namespace app\api\logic;

use app\api\consDir\ErrorConst;
use app\api\services\CollectService;
use app\api\services\GoodsService;
use app\api\services\ShopService;
use app\api\services\SupplyService;
use app\common\libs\Singleton;
use app\common\models\Member\Collect;
use app\common\models\Supply\SupplyGoods;
use app\common\utils\CommonUtil;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

/**
 * 收藏模块
 * Class MemberLogic
 * @package app\api\logic
 */
class CollectLogic extends BaseLogic
{
    use Singleton;

    /**
     * @param $type
     * @param $page
     * @param $pageSize
     * @param $lat
     * @param $lng
     * @return array
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function collectList($type, $page, $pageSize, $lat, $lng): array
    {
        $collectList = CollectService::getInstance()
            ->getUserCollectList($this->userinfo->id, $type, $page, $pageSize);
        if (empty($collectList)) {
            return [];
        }
        $ids = array_column($collectList, 'collectId');

        switch ($type) {
            //1线下店 2线上店 3商品
            case 1:
                $list = ShopService::getInstance()
                    ->shopList(1, 1, $pageSize, $lat, $lng, '', '', '', '', '', '', '', '', '', '', '', '', '', '', $ids);
                break;
            case 2:
                $list = ShopService::getInstance()
                    ->shopList(2, 1, $pageSize, $lat, $lng, '', '', '', '', '', '', '', '', '', '', '', '', '', '', $ids);
                break;
            case 4:
                $list = SupplyService::getInstance()->getCollectList($ids);
                break;
            default:
                $list = GoodsService::getInstance()
                    ->goodsListByIds($ids,'id,goods_title,goods_img,market_price,sell_price');
                $list = empty($list) ? [] : array_values($list);
        }
        return ['rows' => $list];
    }

    /**
     * @param $type
     * @param $collectId
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function delCollect($type, $collectId): bool
    {
        $data = [
            'user_id' => $this->userinfo->id,
            'type' => $type,
            'collect_id' => $collectId,
        ];
        $data  = [ ['user_id' ,'=' ,$this->userinfo->id] , ['type' ,'=' ,$type] ];
        if( $collectId ) $data[] = ['collect_id' ,'in' ,$collectId]; //explode(',' ,$collectId);

        $info = Collect::getInstance()->where($data)->find();
        if (!empty($info)) {
            Collect::getInstance()->where($data)->delete();
        }
        return true;
    }

    /**
     * @param $type
     * @param $collectId
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function addCollect($type, $collectId): bool
    {
        switch ($type) {
            //1线下店 2线上店 3商品 4供应链
            case 2:
            case 1:
                $info = ShopService::getInstance()->getShopInfoById($collectId);
                break;
            case 4:
                $info = SupplyService::getInstance()->goodsDetail($collectId);
                break;
            default:
                $info = GoodsService::getInstance()->goodsDetail($collectId);
        }
        if (empty($info)) {
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, ErrorConst::PARAM_ERROR_MSG);
        }

        $data = [
            'user_id' => $this->userinfo->id,
            'type' => $type,
            'collect_id' => $collectId,
        ];

        $info = Collect::getInstance()->where($data)->find();
        if (empty($info)) {
            Collect::getInstance()->insert($data);
        }
        return true;
    }
}