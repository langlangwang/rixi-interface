<?php

namespace app\api\logic;

use app\api\consDir\ErrorConst;
use app\api\services\AlipayService;
use app\api\services\CartService;
use app\api\services\GoodsService;
use app\api\services\PayService;
use app\api\services\ShopService;
use app\api\services\WechatPayService;
use app\common\libs\Singleton;
use app\common\models\cart\Cart;
use app\common\models\Order\OrderError;
use app\common\utils\CommonUtil;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

/**
 * 回调模块
 * Class MemberLogic
 * @package app\api\logic
 */
class CallbackLogic extends BaseLogic
{
    use Singleton;

    /**
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function callback(): bool
    {
        $data = WechatPayService::getInstance()->callback();
        return $this->extracted($data);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function aliCallback(): bool
    {
        return AlipayService::getInstance()->aliCallback();
    }

    /**
     * @param $data
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function extracted($data): bool
    {
        if (!$data) {
            return false;
        }
        $info = [
            'pay_status' => 1,
            'pay_no' => $data['out_trade_no'],
            'money' => bcmul($data['total_fee'], 0.01),
            'trade_no' => $data['transaction_id'],
            'receipt_data' => json_encode($data),
        ];

        PayService::getInstance()->payFormOrder($info);
        OrderError::getInstance()->insert(['text' => json_encode($data)]);
        return true;
    }

}