<?php

namespace app\api\logic;

use app\api\consDir\ErrorConst;
use app\api\services\CommunityService;
use app\api\services\MemberService;
use app\api\services\PayService;
use app\api\services\ServiceTeamService;
use app\common\libs\Singleton;
use app\common\models\Order\ServiceOrder;
use app\common\utils\CommonUtil;
use app\common\utils\RedLock;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

/**
 * 服务商模块
 * Class MemberLogic
 * @package app\api\logic
 */
class ServiceTeamLogic extends BaseLogic
{
    use Singleton;

    /**
     * @param $orderNo
     * @param $payType
     * @param $couponAmount
     * @return array|string
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function payOrder($orderNo, $payType, $couponAmount){
        if (!RedLock::getInstance()->lock('payOrder_' . $this->userinfo->id, 1)) {
            CommonUtil::throwException(ErrorConst::FREQUENT_ERROR, ErrorConst::FREQUENT_ERROR_MSG);
        }
        $orderInfo = ServiceTeamService::getInstance()->getServiceOrder($orderNo);
        if (empty($orderInfo) || $orderInfo['userId'] != $this->userinfo->id) {
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, ErrorConst::PARAM_ERROR_MSG);
        }
        if ($payType != 1) {
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, ErrorConst::PARAM_ERROR_MSG);
        }
        if ($orderInfo['status'] != 1) {
            CommonUtil::throwException(ErrorConst::ORDER_STATUS_ERROR, ErrorConst::ORDER_STATUS_ERROR_MSG);
        }

        $userInfo = MemberService::getInstance()->getUserInfo($this->userinfo->id);

        $payInfo = ServiceTeamService::getInstance()->payOrder($this->userinfo->id, $orderInfo, $payType);
        if(!$payInfo){
            CommonUtil::throwException(ErrorConst::SYSTEM_ERROR, ErrorConst::SYSTEM_ERROR_MSG);
        }

        return PayService::getInstance()->toPay($payInfo, $userInfo);
    }

    /**
     * @param $orderNo
     * @return array
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function orderInfo($orderNo): array
    {
        $orderInfo = ServiceTeamService::getInstance()->getServiceOrder($orderNo);
        if (empty($orderInfo) || $orderInfo['userId'] != $this->userinfo->id) {
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, ErrorConst::PARAM_ERROR_MSG);
        }
        return ['price' => $orderInfo['price'], 'couponAmount' => 0, 'orderNo' => $orderNo];
    }

    /**
     * @return array
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function serviceInfo(): array
    {
        $userInfo = MemberService::getInstance()->getUserInfo($this->userinfo->id);
        //获取服务商文案
        $article = SysLogic::getInstance()->article(10);
        $payServicePrice = CommunityService::getInstance()->getCommunityConfig('pay_service_price');

        return ['article' => $article, 'payServicePrice' => $payServicePrice, 'isPartner' => $userInfo['isPartner']];
    }

    /**
     * @return array
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function createOrder(): array
    {
        if (!RedLock::getInstance()->lock('createOrder_' . $this->userinfo->id, 1)) {
            CommonUtil::throwException(ErrorConst::FREQUENT_ERROR, ErrorConst::FREQUENT_ERROR_MSG);
        }
        $userInfo = MemberService::getInstance()->getUserInfo($this->userinfo->id);
        if (empty($userInfo)) {
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, ErrorConst::PARAM_ERROR_MSG);
        }
        if ($userInfo['isPartner'] > 0) {
            CommonUtil::throwException(ErrorConst::IS_SERVICE_ERROR, ErrorConst::IS_SERVICE_ERROR_MSG);
        }

        $payServicePrice = CommunityService::getInstance()->getCommunityConfig('pay_service_price');

        $expireAt = date('Y-m-d H:i:s', strtotime('+ 5 minutes'));

        $orderNo = getNo('ST');

        $data = [
            'user_id' => $this->userinfo->id,
            'price' => $payServicePrice,
            'expire_at' => $expireAt,
            'order_no' => $orderNo,

        ];

        ServiceOrder::getInstance()->insert($data);
        return ['orderNo' => $orderNo];
    }
}