<?php
namespace app\api\logic;

use app\api\consDir\ErrorConst;
use app\api\services\CollectService;
use app\api\services\GoodsService;
use app\api\services\ShopService;
use app\common\libs\Singleton;
use app\common\models\Shop\GoodsComment;
use app\common\utils\CommonUtil;

/**
 * 商品模块
 * Class MemberLogic
 * @package app\api\logic
 */
class GoodsLogic extends BaseLogic
{
    use Singleton;

    public function search(): array
    {
        $arr = [
            '男装', '女装', '中山装',
        ];
        return ['rows' => $arr];
    }

    public function goodsComment($goodsId, $page, $pageSize, $isBad, $isImg): array
    {
        // ALTER TABLE `goods_comment` ADD INDEX `idx_GoodsId_Del` (`goods_id`, `deleted`);
        $goodsCommentCount = \think\facade\Db::table('goods_comment')->field([
            'COUNT(*) AS allCount',
            'SUM(CASE WHEN is_img = 1 THEN 1 ELSE 0 END) AS imgCount',
            'SUM(CASE WHEN is_bad = 1 THEN 1 ELSE 0 END) AS badCount',
        ])->where(['goods_id' => $goodsId, 'deleted' => 0])->find();
        $userComment = GoodsService::getInstance()->getComment(
            $goodsId,
            $isImg,
            $isBad,
            $page,
            $pageSize
        );
        return [
            'rows'              => $userComment,
            'goodsCommentCount' => $goodsCommentCount,
        ];
    }

    public function goodsDetail($goodsId): array
    {
        $goodsInfo = GoodsService::getInstance()->goodsDetail($goodsId);
        if (empty($goodsInfo)) {
            CommonUtil::throwException(ErrorConst::NO_GOODS_ERROR, ErrorConst::NO_GOODS_ERROR_MSG);
        }
        $goodsInfo['isCollect']    = CollectService::getInstance()->getUserCollectStatus($this->userinfo->id, 3, $goodsId);
        $goodsInfo['commission']   = bcsub($goodsInfo['sellPrice'], $goodsInfo['costPrice'], 2);
        $shopInfo                  = ShopService::getInstance()->shopInfo($goodsInfo['shopId']);
        $shopInfo['openStatus']    = 1;
        $shopInfo['openWeek']      = '周一到周日';
        $shopInfo['openTimeStart'] = '00:00';
        $shopInfo['openTimeEnd']   = '23:00';
        $shopInfo['goodsCount']    = ShopService::getInstance()->shopGoodsCount($goodsInfo['shopId']);
        // ALTER TABLE `goods_comment` ADD INDEX `idx_GoodsId_Del` (`goods_id`, `deleted`);
        $goodsCommentCount = \think\facade\Db::table('goods_comment')->field([
            'COUNT(*) AS allCount',
            'SUM(CASE WHEN is_img = 1 THEN 1 ELSE 0 END) AS imgCount',
            'SUM(CASE WHEN is_bad = 1 THEN 1 ELSE 0 END) AS badCount',
        ])->where(['goods_id' => $goodsId, 'deleted' => 0])->find();

        $userComment = GoodsService::getInstance()->getComment($goodsId, 0, 0, 1, 3);

        $goodsSku = GoodsService::getInstance()->goodsSku($goodsId);

        $spec = GoodsService::getInstance()->specInfo($goodsId);

        $ruleSwitch = $goodsInfo['ruleSwitch'] ?? -1;
        if ($ruleSwitch == 0) {
            $parameter     = null;
            $outLineDetail = null;
        } else {
            $parameter     = GoodsService::getInstance()->parameter($goodsId);
            $outLineDetail = GoodsService::getInstance()->goodsOutlineDetail($goodsId);
        }
        // 累计商品浏览量
        \think\facade\Db::table('goods')
            ->where('id', $goodsId)
            ->inc('view_count')
            ->update();

        [$level_discount, $coupon_discount_both] = \app\api\services\UserLevelService::getInstance()->levelDiscount(
            $this->userinfo->id
        ); // [0, 1)
        return [
            'levelDiscount'      => $level_discount,
            'couponDiscountBoth' => $coupon_discount_both,
            'outLineDetail'      => empty($outLineDetail) ? null : $outLineDetail,
            'parameter'          => empty($parameter) ? null : $parameter,
            'spec'               => $spec,
            'shopInfo'           => $shopInfo,
            'goodsSku'           => $goodsSku,
            'goodsInfo'          => $goodsInfo,
            'goodsCommentCount'  => $goodsCommentCount,
            'userComment'        => $userComment,
        ];
    }

    public function goodsList($goodsType, $page, $pageSize, $sort, $keyword, $categoryId, $shopId, $positionType): array
    {
        $field = [
            'id',
            'sell_price',
            'market_price',
            'goods_img',
            'goods_title',
            'goods_sales',
            'goods_inventory',
            'gxz_rate',
            'shop_id',
            'goods_tag',
            'goods_service',
        ];
        $info = GoodsService::getInstance()->goodsInfo(
            $shopId,
            $page,
            $pageSize,
            $goodsType,
            $sort,
            $keyword,
            $categoryId,
            $field,
            0,
            $positionType);
        foreach ($info as &$v) {
            $v['gxz']          = GoodsService::getInstance()->getGxz($v['shopId'], $v['gxzRate'], $v['sellPrice']);
            $v['goodsTag']     = empty($v['goodsTag']) ? [] : json_decode($v['goodsTag'], true);
            $v['goodsService'] = empty($v['goodsService']) ? [] : json_decode($v['goodsService'], true);
        }
        return ['rows' => $info];
    }

    public function isLike($page, $pageSize): array
    {
        $field = 'id,sell_price,market_price,goods_img,goods_title';
        $info  = GoodsService::getInstance()->goodsInfo(0, $page, $pageSize, 2, 0, '', 0, $field, 1);
        return ['rows' => $info];
    }

    public function goodsCategory($categoryType): array
    {
        $info = GoodsService::getInstance()->goodsCategory($categoryType);
        return ['rows' => $info];
    }
}
