<?php

namespace app\api\logic;

use app\api\consDir\ErrorConst;
use app\api\consDir\OrderPrefixConst;
use app\api\consDir\SandConst;
use app\api\services\MemberService;
use app\api\services\OrderService;
use app\api\services\SandService;
use app\api\services\ServiceCenterService;
use app\api\services\ShopService;
use app\common\libs\Singleton;
use app\common\models\Order\Order;
use app\common\models\Order\OrderGoods;
use app\common\models\Order\OrderPay;
use app\common\models\Order\OrderShop;
use app\common\models\Service\Service;
use app\common\models\Service\ServiceGoods;
use app\common\models\Service\ServiceTag;
use app\common\models\Shop\Goods;
use app\common\models\Shop\Shop;
use app\common\utils\CommonUtil;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\facade\Log;

class ServiceLogic extends BaseLogic
{
    use Singleton;

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function serviceList($page, $pageSize): array
    {
        return ServiceCenterService::getInstance()->serviceList($page, $pageSize, $this->userinfo->id);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function serviceTaskList($page, $pageSize, $goodsId): array
    {
        if(empty($goodsId)){
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, '商品ID不能为空');
        }
        $applyType = Goods::getInstance()->where('id',$goodsId)->value('apply_type');
        if($applyType == 0){
            $where = [
                ['deleted', '=', 0],
                ['status','=',0],
            ];
            $list = Service::getInstance()
                ->where($where)
                ->field('*,id as service_id')
                ->page($page, $pageSize)
                ->select();
        }else{
            $where = [
                ['a.goods_id', '=', $goodsId],
                ['a.deleted', '=', 0],
                ['s.status','=',0],
                ['s.deleted','=',0]
            ];
            $list = ServiceGoods::getInstance()
                ->alias('a')
                ->leftJoin('service s', 'a.service_id=s.id')
                ->field('a.*,s.service_province,s.service_city,s.service_country,s.detail_address,s.name,s.contact_name,s.opening_hours')
                ->where($where)
                ->page($page, $pageSize)
                ->order('id desc')
                ->select();
        }

        if (empty($list)) {
            return [];
        }
        foreach ($list as &$v) {
            $v['allAddress'] = $v['service_province'] . $v['service_city'] . $v['service_country'] . $v['detail_address'];
        }
        return $list->toArray();
    }

    /**
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    public function check($orderNo,$serviceId): bool
    {
        if(empty($orderNo)){
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, '商品ID不能为空');
        }
        if(empty($serviceId)){
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, '服务中心ID不能为空');
        }
        $where = [
            ['order_no', '=', $orderNo],
            ['service_id','=',$serviceId]
        ];
        $order = Order::getInstance()->where($where)->find();
        if (empty($order)) {
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, '订单不存在');
        }
        $where = [
            ['id', '=', $serviceId],
            ['user_id', '=', $this->userinfo->id],
            ['deleted', '=', 0]
        ];
        $service = Service::getInstance()->where($where)->find();
        if (empty($service)) {
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, '该服务中心暂无权限');
        }
        //0待支付 1待发货 2待收款 3已收货 4已完成 5退款中 6退款成功 7取消 8超时取消 9待退货
        if ($order['status'] != 10) {
            CommonUtil::throwException(ErrorConst::ORDER_STATUS_ERROR, ErrorConst::ORDER_STATUS_ERROR_MSG);
        }
        $task = OrderService::getInstance()->taskOrder($order, $this->userinfo->id);
        if (empty($task)) {
            CommonUtil::throwException(ErrorConst::SYSTEM_ERROR, ErrorConst::SYSTEM_ERROR_MSG);
        }
        // 检查会员等级升级
        try {
            \app\api\services\UserLevelService::getInstance()->checkUpgrade(
                $this->userinfo->id
            );
        } catch (\Exception $e) {
            \think\facade\Log::error($e->getMessage());
        }
        return true;
    }

    /**
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    public function orderDetail($orderNo,$serviceId): array
    {
        if(empty($orderNo)){
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, ErrorConst::PARAM_ERROR_MSG);
        }
        if(empty($serviceId)){
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, '服务中心ID不能为空');
        }
        $order = ServiceCenterService::getInstance()->orderDetail($orderNo,$serviceId,$this->userinfo->id);
        if(empty($order)){
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, '订单不存在');
        }
        return $order;
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function checkList($page, $pageSize, $serviceId): array
    {
        return ServiceCenterService::getInstance()->checkList($this->userinfo->id,$page,$pageSize,$serviceId);
    }

    /**
     * @throws DbException
     */
    public function show(): array
    {
        $where = [
            ['user_id','=',$this->userinfo->id],
            ['deleted','=',0]
        ];
        $count = Service::getInstance()->where($where)->count();
        return ['status' => $count > 0 ? 1 : 0];
    }

    /**
     * 附近服务中心列表
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function serviceNearbyList($page, $pageSize, $lng, $lat): array
    {
        return ServiceCenterService::getInstance()->serviceNearbyList($page, $pageSize,$lng,$lat);
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function serviceDetail($id): array
    {
        $ret = Service::getInstance()->where('id',$id)->find();
        if(empty($ret)){
            return [];
        }
        $ret['allAddress'] = $ret['serviceProvince'].$ret['serviceCity'].$ret['serviceCountry'].$ret['detailAddress'];
        $ret['tagList'] = ServiceTag::getInstance()->where('id','in',$ret['tag'])->field('id,name')->select();
        return $ret->toArray();
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function orderList($page, $pageSize, $serviceId, $orderNo, $orderType): array
    {
        return ServiceCenterService::getInstance()->orderList($page, $pageSize, $serviceId, $orderNo, $orderType,$this->userinfo->id);
    }
}