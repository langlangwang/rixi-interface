<?php

namespace app\api\logic;

use app\api\consDir\ErrorConst;
use app\api\services\SchoolsService;
use app\common\libs\Singleton;
use app\common\models\Cart\MemberCart;
use app\common\utils\CommonUtil;

/**
 * 购物车模块
 * Class MemberLogic
 * @package app\api\logic
 */
class SchoolsLogic extends BaseLogic
{
    use Singleton;

    /**
     * 列表
     * @param $cartId
     * @param $goodsId
     * @param $quantity
     * @param $skuId
     * @return bool
     */

    public function schoolsList($page, $pageSize ,$searchKey=""): array
    {
        $list = SchoolsService::getInstance()->schoolsList($page, $pageSize ,$searchKey);
        return ['rows' => $list];
    }

    public function schoolsDetail( $id )
    {
        $schools = SchoolsService::getInstance()->getInfo($id);
        if (empty($schools)) {
            CommonUtil::throwException(ErrorConst::QUERY_NOT_EXIST, ErrorConst::QUERY_NOT_EXIST_MSG);
        }
        //更新次数
        SchoolsService::getInstance()->updateNumber($id);
        return $schools;
    }

}