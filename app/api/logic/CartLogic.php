<?php

namespace app\api\logic;

use app\api\consDir\ErrorConst;
use app\api\services\CartService;
use app\api\services\GoodsService;
use app\api\services\MemberService;
use app\api\services\ShopService;
use app\common\libs\Singleton;
use app\common\models\Cart\MemberCart;
use app\common\utils\CommonUtil;

/**
 * 购物车模块
 * Class MemberLogic
 * @package app\api\logic
 */
class CartLogic extends BaseLogic
{
    use Singleton;

    /**
     * 改购物车
     * @param $cartId
     * @param $goodsId
     * @param $quantity
     * @param $skuId
     * @return bool
     */
    public function turnCart($cartId, $goodsId, $quantity, $skuId): bool
    {
        $this->delCart($cartId);
        $this->addCart($goodsId, $quantity, $skuId);
        return true;
    }


    public function cartList(): array
    {
        $cartList = CartService::getInstance()->cartList($this->userinfo->id);

        if (empty($cartList)) {
            return [];
        }
        //获取所有店铺
        $shopInfo = ShopService::getInstance()->shopListByIds(array_column($cartList, 'shopId'), 'id,shop_name');
        if (empty($shopInfo)) {
            return [];
        }
        //获取所有商品
        $goodsInfo = GoodsService::getInstance()->goodsListByIds(array_column($cartList, 'goodsId'), 'id,goods_img,sell_price,goods_title,is_sku');
        if (empty($goodsInfo)) {
            return [];
        }

        $skuInfo = GoodsService::getInstance()->goodsSkuListByIds(array_column($cartList, 'skuId'));
        $cartShopInfo = [];
        foreach ($cartList as $k => $val) {
            $shop = $shopInfo[$val['shopId']] ?? [];
            if (empty($shop)) {
                unset($cartList[$k]);
                continue;
            }

            $goods = $goodsInfo[$val['goodsId']] ?? [];
            if (empty($goods)) {
                unset($cartList[$k]);
                continue;

            }
            $sku = $skuInfo[$val['skuId']] ?? [];
            if ($goods['is_sku'] == 1 && empty($sku)) {
                unset($cartList[$k]);
                continue;
            }

            $goodsList = [
                'goods' => $goods,
                'sku' => $sku,
                'cart' => $val,
            ];
            if (!isset($cartShopInfo[$val['shopId']])) {
                $cartShopInfo[$val['shopId']] = $shop;
            }
            $cartShopInfo[$val['shopId']]['goodsInfo'][] = $goodsList;
//            if($goods['is_sku'] == 1){
//                $val['allSpecInfo'] = GoodsService::getInstance()->specInfo($val['goodsId']);
//                $val['allSkuInfo'] = GoodsService::getInstance()->goodsSku($val['goodsId']);
//            }
        }
        $cartShopInfo = empty($cartShopInfo) ? [] : array_values($cartShopInfo);

        $addressList = MemberService::getInstance()->addressList($this->userinfo->id, 1, 1);
        $address = $addressList[0] ?? [];
        return ['rows' => $cartShopInfo, 'address' => $address];
    }

    public function delCart($cartId): bool
    {
        $where = [
            ['user_id', '=', $this->userinfo->id],
            ['id', '=', $cartId],
        ];
        MemberCart::getInstance()->where($where)->update(['deleted' => 1]);
        return true;
    }

    public function addCart($goodsId, $quantity, $skuId): bool
    {
        //判断商品是否存在
        $goodsInfo = GoodsService::getInstance()->goodsDetail($goodsId);
        if (empty($goodsInfo)) {
            CommonUtil::throwException(ErrorConst::NO_GOODS_ERROR, ErrorConst::NO_GOODS_ERROR_MSG);
        }
        if ($skuId > 0) {
            $skuInfo = GoodsService::getInstance()->skuInfo($goodsId, $skuId);
            if (empty($skuInfo)) {
                CommonUtil::throwException(ErrorConst::NO_SKU_ERROR, ErrorConst::NO_SKU_ERROR_MSG);
            }
        }

        $cartInfo = CartService::getInstance()->cartInfo($this->userinfo->id, $goodsId, $skuId);

        $data = [
            'user_id' => $this->userinfo->id,
            'shop_id' => $goodsInfo['shopId'],
            'goods_id' => $goodsId,
            'sku_id' => $skuId,
            'quantity' => $quantity,
            'update_at' => date('Y-m-d H:i:s'),
        ];
        if (empty($cartInfo)) {
            MemberCart::getInstance()->insert($data);

        } else {
            MemberCart::getInstance()->where(['id' => $cartInfo['id']])->update($data);
        }
        return true;
    }

}