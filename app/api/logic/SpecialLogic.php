<?php
namespace app\api\logic;

use app\common\libs\Singleton;

/**
 * Class SpecialLogic
 * @package app\api\logic
 */
class SpecialLogic extends BaseLogic
{
    use Singleton;

    public function goodsPage($where, $field = [])
    {
        $where = array_merge($where, ['deleted' => 0]);
        //
        $page = \think\facade\Db::table('special_goods')
            ->field($field)
            ->where($where)
            ->order('sort desc, id desc')
            ->paginate(input('pageSize', 10));
        return empty($page) ? [] : $page->toArray();
    }

    public function articlePage($where, $field = [])
    {
        $where = array_merge($where, ['deleted' => 0]);
        //
        $page = \think\facade\Db::table('special_article')
            ->field($field)
            ->where($where)
            ->order('sort desc, id desc')
            ->paginate(input('pageSize', 10));
        return empty($page) ? [] : $page->toArray();
    }

    public function articleDetail($where, $field = [])
    {
        $where = array_merge($where, ['deleted' => 0]);
        //
        $obj = \think\facade\Db::table('special_article')
            ->field($field)
            ->where($where)
            ->find();
        // var_dump($obj);exit;
        return $obj;
    }

    public function singlepageDetail($where, $field = [])
    {
        $where = array_merge($where, ['deleted' => 0]);
        //
        $obj = \think\facade\Db::table('special_singlepage')
            ->field($field)
            ->where($where)
            ->find();
        return $obj;
    }

    /**
     *
     * @param  [type]  $tb   [description]
     * @param  [type]  $id   [description]
     * @param  [type]  $uid  [description]
     * @param  integer $type 点赞类型 10 取消点赞  11 点赞;  20 取消踩一下 21 踩一下
     * @return boolean       [description]
     */
    public function isThumb($tb, $id, $uid, $type = 11)
    {
        $where = [
            'deleted' => 0,
            'tb'      => $tb,
            'user_id' => $uid,
            'tb_id'   => $id,
            'type'    => $type,
        ];
        // use index idx_Del_Tb_Uid_TbId_Type
        $count = \think\facade\Db::table('special_thumb')
            ->where($where)
            ->count();
        return $count > 0 ? true : false;
    }

    public function thumb($tb, $id, $uid, $type = 11)
    {

        $where = [
            'deleted' => 0,
            'tb'      => $tb,
            'user_id' => $uid,
            'tb_id'   => $id,
        ];
        // use index idx_Del_Tb_Uid_TbId_Type
        $oldType = \think\facade\Db::table('special_thumb')
            ->where($where)
            ->value('type');
        // echo \think\facade\Db::table('special_thumb')->getlastsql();
        // var_dump($oldType);exit;
        if (empty($oldType)) {
            $data = [
                'tb'      => $tb,
                'user_id' => $uid,
                'tb_id'   => $id,
                'type'    => $type,
            ];
            \think\facade\Db::table('special_thumb')->insert($data);
            \think\facade\Db::table($tb)
                ->where('id', $id)
                ->inc('thumbup')
                ->update();
        }
        // var_dump($oldType);exit;
        if ($oldType == 10 && $type == 11) {
            \think\facade\Db::table('special_thumb')
                ->where($where)
                ->update([
                    'update_at' => date("Y-m-d H:i:s"),
                    'type'      => 11,
                ]);
            \think\facade\Db::table($tb)
                ->where('id', $id)
                ->inc('thumbup')
                ->update();
        } else if ($oldType == 11 && $type == 10) {
            \think\facade\Db::table('special_thumb')
                ->where($where)
                ->update([
                    'update_at' => date("Y-m-d H:i:s"),
                    'type'      => 10,
                ]);
            \think\facade\Db::table($tb)
                ->where('id', $id)
                ->dec('thumbup')
                ->update();
        }

    }
}
