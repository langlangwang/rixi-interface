<?php

namespace app\api\logic;


use app\api\consDir\ErrorConst;
use app\api\services\CardService;
use app\api\services\MemberService;
use app\api\services\PayService;
use app\api\services\RabbitMqService;
use app\common\libs\Singleton;
use app\common\models\Card\Card;
use app\common\models\Card\CardOrder;
use app\common\models\Card\CardUser;
use app\common\utils\CommonUtil;
use app\common\utils\RedLock;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\facade\Log;

/**
 * 订单模块
 * Class MemberLogic
 * @package app\api\logic
 */
class CardLogic extends BaseLogic
{
    use Singleton;


    /**
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    public function cardList(): array
    {
        $cardTimeType = CardUser::getInstance()->where('user_id',$this->userinfo->id)->order('card_time_type desc')->value('card_time_type');
        $where = [
            ['deleted', '=', 0],
            ['status', '=', 1],
        ];
        if($cardTimeType){
            $where[] = ['time_type','>=',$cardTimeType];
        }
        $logList = Card::getInstance()
            ->where($where)
            ->order('sort desc,id desc')
            ->select();
        return empty($logList) ? [] : $logList->toArray();
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function orderList($status, $page, $pageSize): array
    {
        $list = CardService::getInstance()->orderList($this->userinfo->id, $status, $page, $pageSize);
        return ['rows' => $list];
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function createOrder($cardId): array
    {
        if (empty($cardId)) {
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, ErrorConst::PARAM_ERROR_MSG);
        }
        $where = [
            ['id', '=', $cardId],
            ['deleted', '=', 0],
        ];
        $card = Card::getInstance()->where($where)->find();
        if (empty($card)) {
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, '会员卡不存在');
        }
        if ($card['status'] == 0) {
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, '该会员卡已停售');
        }
        if ($card['price'] <= 0) {
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, '价格异常');
        }
        $cardUser = CardUser::getInstance()->where('user_id',$this->userinfo->id)->order('card_time_type desc')->find();
        if($cardUser){
            if($card['timeType'] < $cardUser['cardTimeType']){
                CommonUtil::throwException(ErrorConst::PARAM_ERROR, '不能降级购买会员卡');
            }
        }

        $orderNo = getNo('CD');
        $order = CardService::getInstance()->createOrder($this->userinfo->id, $card, $orderNo);
        if (!$order) {
            CommonUtil::throwException(ErrorConst::SYSTEM_ERROR, ErrorConst::SYSTEM_ERROR_MSG);
        }

        $expireAt = date('Y-m-d H:i:s', strtotime('+5 minutes'));
        return ['orderNo' => $orderNo, 'expireAt' => $expireAt,'payPrice' => $card['price']];
    }

    /**
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    public function payOrder($orderNo, $payType, $payPassword, $paySubType): array
    {
        if ( ! RedLock::getInstance()->lock('cardPayOrder_' . $this->userinfo->id, 3)) {
            CommonUtil::throwException(ErrorConst::FREQUENT_ERROR, ErrorConst::FREQUENT_ERROR_MSG);
        }

        if ($payType == 3) {
            $account = MemberService::getInstance()->account($this->userinfo->id);
            if (empty($account)) {
                CommonUtil::throwException(ErrorConst::NO_PAY_PASSWORD_ERROR, ErrorConst::NO_PAY_PASSWORD_ERROR_MSG);
            }

            //验证支付密码
            if ($account['payPwd'] != $payPassword) {
                CommonUtil::throwException(ErrorConst::PAY_PASSWORD_ERROR, ErrorConst::PAY_PASSWORD_ERROR_MSG);
            }
        }

        $order = CardService::getInstance()->getOrderInfo($orderNo, $this->userinfo->id);
        if (empty($order)) {
            CommonUtil::throwException(ErrorConst::NO_ORDER_ERROR, ErrorConst::NO_ORDER_ERROR_MSG);
        }
        if ($order['status'] != 0) {
            CommonUtil::throwException(ErrorConst::ORDER_STATUS_ERROR, ErrorConst::ORDER_STATUS_ERROR_MSG);
        }

        $where = [
            ['deleted','=',0],
            ['id','=',$order['cardId']]
        ];
        $card = Card::getInstance()->where($where)->find();
        if(empty($card)){
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, '会员卡不存在');
        }
        if($card['status'] == 0){
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, '会员卡已停售');
        }
        if($card['stock'] <= 0){
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, '库存不足');
        }

        $cardUser = CardUser::getInstance()->where('user_id',$this->userinfo->id)->order('card_time_type desc')->find();
        if($cardUser){
            if($card['timeType'] < $cardUser['cardTimeType']){
                CommonUtil::throwException(ErrorConst::PARAM_ERROR, '不能降级购买会员卡');
            }
        }

        $isBalance = 0;

        CardOrder::getInstance()->where('order_no', $orderNo)->update([
            'pay_type' => $payType,
        ]);
        $payInfo = PayService::getInstance()->payOrder($this->userinfo->id, $order, $payType);
        if ($payType == 3) {
            $userBalance = MemberService::getInstance()->finance($this->userinfo->id);
            if ($payInfo['pay_price'] > $userBalance['amount']) {
                CommonUtil::throwException(ErrorConst::ORDER_STATUS_ERROR, '余额不足');
            }
            $isBalance = 1;
        }

        $userInfo = MemberService::getInstance()->getUserInfo($this->userinfo->id);
        if ($payType == 3) {
            //余额全付
            $info = [
                'pay_status' => 3,
                'pay_no' => $payInfo['pay_no'],
                'money' => $payInfo['pay_price'],
                'trade_no' => '',
                'receipt_data' => '',
            ];
            $event["exchange"] = config('rabbitmq.order_callback_queue');
            RabbitMqService::send($event, $info);
            $pay = [];
        } elseif ($payType == 4) {
            $pay = SandLogic::getInstance()->orderCreate($this->userinfo->id, $payInfo['pay_price'], $payInfo['pay_no']);
        } elseif ($payType == 5) {
            $pay = SandLogic::getInstance()->orderCreateScan($payInfo['pay_price'], $payInfo['pay_no']);
        } else {
            $pay = PayService::getInstance()->toPay($payInfo, $userInfo, $payType, $paySubType);
        }
        return ['payInfo' => $pay, 'isBalance' => $isBalance];
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function orderDetail($orderNo): array
    {
        if(empty($orderNo)){
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, '订单号不能为空');
        }
        $order = CardService::getInstance()->getOrderInfo($orderNo,$this->userinfo->id);
        if($order){
            //客服电话
            $order['tel'] = '18938533289';
        }
        return $order;
    }

    public function cancelOrder($orderNo): bool
    {
        if ( ! RedLock::getInstance()->lock('cardCancelOrder_' . $this->userinfo->id, 3)) {
            CommonUtil::throwException(ErrorConst::FREQUENT_ERROR, ErrorConst::FREQUENT_ERROR_MSG);
        }
        if(empty($orderNo)){
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, '订单号不能为空');
        }
        $where = [
            ['order_no','=',$orderNo],
            ['status','=',0]
        ];
        CardOrder::getInstance()->where($where)->update(['update_at' => date('Y-m-d H:i:s'),'status' => 2]);
        return true;
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function cardUser(): array
    {
        $where = [
            ['user_id','=',$this->userinfo->id],
            ['status','=',1],
        ];
        $cardUser = CardUser::getInstance()->where($where)->order('card_time_type desc')->find();
        if(empty($cardUser)){
            return [];
        }
        $isEnd = 1;
        if(date('Y-m-d H:i:s') > $cardUser['expireAt']){
            $isEnd = 0;
        }
        $cardUser->isEnd = $isEnd;
        return $cardUser->toArray();
    }

}
