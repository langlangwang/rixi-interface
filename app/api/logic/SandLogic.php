<?php

namespace app\api\logic;

use app\api\cache\KeysUtil;
use app\api\cache\RedisCache;
use app\api\consDir\ErrorConst;
use app\api\services\MemberService;
use app\api\services\SandService;
use app\common\libs\Singleton;
use app\common\models\Member\Member;
use app\common\utils\CommonUtil;
use think\facade\Log;

class SandLogic extends BaseLogic
{
    use Singleton;

    /**
     * 查询余额
     * @param $userId
     * @return array
     */
    public function accountBalanceQuery($userId): array
    {
        $ret = SandService::getInstance()->accountBalanceQuery($userId);
        if (isset($ret['resultStatus']) && $ret['resultStatus'] != 'success') {
            //CommonUtil::throwException(ErrorConst::PARAM_ERROR, $ret['errorDesc']);
            Log::error(json_encode($ret, 256));
            return [];
        }
        return $ret;
    }

    public function accountMemberQuery($userId): array
    {
        $ret = SandService::getInstance()->accountMemberQuery($userId);
        /*if ($ret['resultStatus'] != 'success') {
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, $ret['errorDesc']);
        }*/
        return $ret;
    }

    public function accountOneKey($nickname, $name, $idCard, $phone, $userId): array
    {
        $ret = SandService::getInstance()->accountOneKey($nickname, $name, $idCard, $phone, $userId);
        if ($ret['resultStatus'] != 'success') {
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, $ret['errorDesc']);
        }
        return $ret;
    }

    public function accountMemberStatus($userId, $operationType): array
    {
        $ret = SandService::getInstance()->accountMemberStatus($userId, $operationType);
        if ($ret['resultStatus'] != 'success') {
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, $ret['errorDesc']);
        }
        return $ret;
    }

    /**
     * 消费
     * @param $bizUserNo
     * @param $amount
     * @param $orderNo
     * @return array
     */
    public function orderCreate($bizUserNo, $amount, $orderNo): array
    {
        $ret = SandService::getInstance()->orderCreate($bizUserNo, $amount, $orderNo);
        if ($ret['resultStatus'] != 'accept') {
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, json_encode($ret));
        }
        $ret['url'] = $ret['credential']['cashierUrl'];
        return $ret;
    }

    public function orderRefund($outOrderNo, $oriOutOrderNo, $amount): array
    {
        return SandService::getInstance()->orderRefund($outOrderNo, $oriOutOrderNo, $amount);
    }

    public function orderQuery($orderNo): array
    {
        return SandService::getInstance()->orderQuery($orderNo);
    }

    public function walletOrderQuery($orderNo): array
    {
        return SandService::getInstance()->walletOrderQuery($orderNo);
    }

    /**
     * 扫码下单
     * @param $amount
     * @param $orderNo
     * @return array
     */
    public function orderCreateScan($amount, $orderNo): array
    {
        $ret = SandService::getInstance()->orderCreateScan($amount, $orderNo);
        if ($ret['resultStatus'] != 'accept') {
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, $ret['errorDesc']);
        }
        $ret['qrCode'] = $ret['credential']['qrCode'];
        return $ret;
    }

    /**
     * 转账
     * @param $amount
     * @param $payerBizUserNo
     * @param $payeeBizUserNo
     * @param $bizUserFeeAmt
     * @param $orderNo
     * @param string $transMode
     * @return array
     */
    public function transfer($amount, $payerBizUserNo, $payeeBizUserNo, $bizUserFeeAmt, $orderNo, string $transMode = 'rt'): array
    {
        $ret = SandService::getInstance()->transfer($amount, $payerBizUserNo, $payeeBizUserNo, $bizUserFeeAmt, $orderNo, $transMode);
        if ($ret['code'] == 0) {
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, $ret['data']);
        }
        return $ret['data'];
    }

    /**
     * 转账确认
     * @param $outOrderNo
     * @param $oriOutOrderNo
     * @param $amount
     * @param $operationType
     * @param $userId
     * @return mixed
     */
    public function transferConfirm($outOrderNo, $oriOutOrderNo, $amount, $operationType, $userId)
    {
        return SandService::getInstance()->transferConfirm($outOrderNo, $oriOutOrderNo, $amount, $operationType, $userId);
    }

    /**
     * 会员绑卡查询
     * @param $userId
     * @return array
     */
    public function accountBindCardQuery($userId): array
    {
        $ret = SandService::getInstance()->accountBindCardQuery($userId);
        if ($ret['resultStatus'] != 'success') {
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, $ret['errorDesc']);
        }
        return $ret;
    }

    /**
     * 提现
     * @param $amount
     * @param $orderNo
     * @param $cardNo
     * @return array
     */
    public function withdraw($amount, $orderNo, $cardNo): array
    {
        $ret = SandService::getInstance()->withdraw($this->userinfo->id, $amount, $orderNo, $cardNo);
        if ($ret['resultStatus'] != 'success') {
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, $ret['errorDesc']);
        }
        return $ret;
    }

    /**
     * 账户变动明细
     * @param $userId
     * @param $beginDate
     * @param $endDate
     * @return array
     */
    public function detailQuery($userId, $beginDate, $endDate, $page, $pageSize): array
    {
        $ret = SandService::getInstance()->detailQuery($userId, $beginDate, $endDate, $page, $pageSize);
        if ($ret['resultStatus'] != 'success') {
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, $ret['errorDesc']);
        }
        return $ret;
    }

    public function paymentOrderCreate($bankNo, $name, $amount): array
    {
        return SandService::getInstance()->paymentOrderCreate($bankNo, $name, $amount);
    }

    public function paymentOrderQuery($orderNo): array
    {
        return SandService::getInstance()->paymentOrderQuery($orderNo);
    }

    /**
     * 个人钱包
     * @return array
     */
    public function account(): array
    {
        $phone = RedisCache::hGet(KeysUtil::getMemberInfoKey($this->userinfo->id), 'phone');
        if (empty($phone)) {
            $member = Member::getInstance()->where('id', $this->userinfo->id)->find();
            if ($member) {
                RedisCache::hMSet(KeysUtil::getMemberInfoKey($this->userinfo->id), CommonUtil::camelToUnderLine($member->toArray()));
            }
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, ErrorConst::PARAM_ERROR_MSG);
        }
        $account = MemberService::getInstance()->account($this->userinfo->id);
        if (empty($account['realName']) || empty($account['idc'])) {
            CommonUtil::throwException(ErrorConst::REAL_NAME_ERROR, ErrorConst::REAL_NAME_ERROR_MSG);
        }
        $ret = SandService::getInstance()->accountOneKey($account['realName'], $account['realName'], $account['idc'], $phone, $account['userId']);
        if ($ret['resultStatus'] != 'success') {
            CommonUtil::throwException(ErrorConst::PARAM_ERROR, $ret['errorDesc']);
        }
        return $ret;
    }

    public function freeze($outOrderNo,$money,$userId): array
    {
        return SandService::getInstance()->freeze($outOrderNo,$money,$userId);
    }

    public function unfreeze($outOperationOrderNo,$oriOutOrderNo,$money): array
    {
        return SandService::getInstance()->unfreeze($outOperationOrderNo,$oriOutOrderNo,$money);
    }
}