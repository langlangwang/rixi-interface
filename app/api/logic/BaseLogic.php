<?php

namespace app\api\logic;

use app\api\provider\Userinfo;
use think\App;

class BaseLogic
{
    /**
     * @var Userinfo
     */
    public $userinfo = null;

    public function __construct()
    {
        $this->userinfo = App::getInstance()->userinfo;
    }
}
