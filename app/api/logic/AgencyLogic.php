<?php
namespace app\api\logic;

use app\common\libs\Singleton;

/**
 * Class AgencyLogic
 * @package app\api\logic
 */
class AgencyLogic extends BaseLogic
{
    use Singleton;

    /**
     * 合伙人-代理区域-区域会员分页列表
     * @param  [type] $where [description]
     * @param  array  $field [description]
     * @return [type]        [description]
     */
    public function memberPage($where, $field = [])
    {
        if (empty($field)) {
            $field = [
                'id',
                'id user_id',
                'avatar',
                'user_name',
                'phone',
                'create_at', // 入驻时间
                // 'province',
                'province_id',
                // 'city',
                'city_id',
                // 'country',
                'country_id',
                // 'street',
                'street_id',
                // 'comm',
                'comm_id',
                'is_agency',
                'level',
            ];
        }
        $where = array_merge($where, ['deleted' => 0]);
        //
        $page = \app\common\models\Member\Member::field($field)
            ->where($where)
            ->order('id desc')
            ->paginate(input('pageSize', 10));
        $page = empty($page) ? [] : $page->toArray();
        if ( ! empty($page['rows'])) {
            $comm_ids = array_column($page['rows'], 'commId');
            // var_dump($comm_ids);exit;
            $map = \think\facade\Db::table('community')
                ->whereIn('comm_id', $comm_ids)
                ->column('*', 'comm_id');
            // var_dump($map);exit;
            foreach ($page['rows'] as &$vol) {
                $area = $map[$vol['commId']] ?? [];
                $vol  = $this->filterMemberInfo($vol, $area);
            }
        }
        return $page;
    }

    public function filterMemberInfo($vol, $area = null)
    {
        if (empty($area)) {
            $area = \think\facade\Db::table('community')
                ->where('comm_id', $vol['commId'])
                ->find();
        }

        $vol['province'] = $area['province'] ?? '';
        $vol['city']     = $area['city'] ?? '';
        $vol['country']  = $area['country'] ?? '';
        $vol['street']   = $area['street'] ?? '';
        $vol['comm']     = $area['comm'] ?? '';
        $vol['phone']    = empty($vol['phone']) ? '' : substr_replace($vol['phone'], '****', 3, 4);
        // var_dump($vol['phone']);exit();
        return $vol;
    }

    /**
     * 合伙人-代理区域-区域商家分页列表
     * @param  [type] $where [description]
     * @param  array  $field [description]
     * @return [type]        [description]
     */
    public function shopPage($where, $field = [])
    {
        if (empty($field)) {
            $field = [
                'id',
                'id shop_id',
                'shop_logo',
                'shop_name',
                'business_name',
                'business_type',
                'create_at', // 入驻时间
                'shop_type',
                'province',
                'province_id',
                'city',
                'city_id',
                'country',
                'country_id',
                'street',
                'street_id',
                'comm',
                'comm_id',
                'latitude',
                'longitude',
                'address',
            ];
        }
        $where = array_merge($where, ['deleted' => 0]);
        //
        $page = \app\common\models\Shop\Shop::field($field)
            ->where($where)
            ->order('sort desc, id desc')
            ->paginate(input('pageSize', 10));
        $page = empty($page) ? [] : $page->toArray();
        return $page;
    }

    /**
     * 合伙人-代理区域-所属社区分页列表
     * @param  [type] $where [description]
     * @return [type]        [description]
     */
    public function communityPage($where)
    {
        $where['deleted'] = 0;

        $field = [
            "id",
            "name",
            "province",
            "province_id",
            "city",
            "city_id",
            "country",
            "country_id",
            "street",
            "street_id",
            "comm",
            "comm_id",
            "latitude",
            "longitude",
        ];
        // 这里不用关心锁定状态
        $page = \app\common\models\Community\Community::field($field)
            ->where($where)
            ->order('id desc')
            ->paginate(input('pageSize', 10));
        return empty($page) ? [] : $page->toArray();
    }
}
