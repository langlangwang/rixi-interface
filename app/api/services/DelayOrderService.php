<?php

namespace app\api\services;

use app\common\libs\Singleton;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\facade\Log;

class DelayOrderService
{
    use Singleton;

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function order($data){
        if(empty($data)){
            return false;
        }
        try{
            switch ($data['type']){
                case 'community_order':
                    CommunityService::getInstance()->orderTimeout($data['order_id']);
                    break;
                case 'community_new':
                    CommunityService::getInstance()->newTimeout();
                    break;
                case 'community_prepay':
                    //预约支付转到社区广场
                    CommunityService::getInstance()->prepayTimeout();
                    break;
                case 'community_prepay_order':
                    CommunityService::getInstance()->prepayOrderTimeout($data['order_id']);
                    break;
                case 'community_business':
                    CommunityService::getInstance()->communityBusiness($data['order_no']);
                    break;
            }
        }catch (\Exception $e){
            Log::error('[DelayOrderService/order]'.$e->getMessage().$e->getLine().$e->getFile());
        }
    }
}