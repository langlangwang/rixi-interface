<?php


namespace app\api\services;

use app\common\libs\Singleton;
use app\common\models\Member\Collect;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class CollectService
{
    use Singleton;

    public function getUserCollectCount($userId): int
    {
        $where = [
            ['user_id','=',$userId],
            ['deleted','=',0],
        ];
        return Collect::getInstance()->where($where)->count('id');
    }

    /**
     * 判断收藏状态
     * @param $userId
     * @param $type
     * @param $collectId
     * @return bool
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function getUserCollectStatus($userId, $type, $collectId): bool
    {
        $where = [
            ['user_id','=',$userId],
            ['type','=',$type],
            ['collect_id','=',$collectId],
        ];
        $info = Collect::getInstance()->where($where)->find();
        if(empty($info)){
            return false;
        }
        return true;
    }

    /**
     * @param $userId
     * @param $type
     * @param $page
     * @param $pageSize
     * @return array
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function getUserCollectList($userId, $type, $page, $pageSize): array
    {

        $where = [
            ['user_id', '=', $userId],
            ['type', '=', $type],
        ];

        $list = Collect::getInstance()
            ->where($where)
            ->limit(($page - 1) * $pageSize, $pageSize)
            ->select();

        return empty($list) ? [] : $list->toArray();
    }
}