<?php


namespace app\api\services;

use app\api\cache\ShopCache;
use app\common\libs\Singleton;
use app\common\models\Shop\Shop;

class ShopService1
{
    use Singleton;

    public function shopInfo($shopId){

    }

    /**
     * 获取用户店铺信息
     * @param $userId
     * @return array|mixed|string
     */
    public function getShopInfo($userId)
    {
        $shopData = ShopCache::getShopInfo($userId);
        if ($shopData) {
            return $shopData;
        }
        $shopData = Shop::getInstance()->where('user_id', '=', $userId)->field('id as shop_id,shop_name')->find();
        if (empty($shopData)) {
            return [];
        }
        $shopData = $shopData->toArray();
        ShopCache::setShopInfo($userId, $shopData);
        return $shopData;
    }

    /**
     * 更新店铺信息
     * @param $id
     * @param $data
     */
    public function updateUser($id, $data)
    {
        Shop::update($data, ['id' => $id]);
    }
}