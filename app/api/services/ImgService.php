<?php


namespace app\api\services;


use app\common\libs\Singleton;
use Kkokk\Poster\PosterManager;

class ImgService
{
    use Singleton;

    public function getImgInfo($src, $w, $l, $qrCode, $qrW, $qrL)
    {
        $img = md5(time()) . rand(100, 999);
        $poster = PosterManager::Poster('img_' . $img);
        $poster->buildImDst($src, $w, $l);
        $poster->buildImage($qrCode, $qrW, $qrL);
        return $poster->getPoster()['url'];
    }

    public function getQr($src,$w, $l)
    {
        $img = md5(time()) . rand(100, 999);
        $poster = PosterManager::Poster('img_' . $img);
//        $poster->buildImDst($src, $w, $l);
        $poster->buildIm($w, $l);
        $poster->buildQr($src,'center','center',0,0,$w,$l,4,1);
        return $poster->getPoster()['url'];
    }
}