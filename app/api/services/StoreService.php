<?php

namespace app\api\services;

use app\api\consDir\UrlConst;
use app\common\libs\Singleton;
use app\common\models\Shop\Category;
use app\common\models\Shop\Goods;
use app\common\models\Shop\GoodsOption;
use app\common\models\Shop\GoodsSpec;
use app\common\models\Shop\GoodsSpecItem;
use app\common\models\Shop\Merch;
use app\common\models\Shop\Store;

class StoreService
{
    use Singleton;

    /**
     * @param $goodsId
     * @return array
     */
    public function goodsSpec($goodsId): array
    {
        $where = [
            ['goodsid', '=', $goodsId],
        ];
        $spec = GoodsSpec::getInstance()
            ->where($where)
            ->column('id,content');
        foreach ($spec as &$val) {
            $val['content'] = unserialize($val['content']);
            $val['specInfo'] = GoodsSpecItem::getInstance()
                ->where(['specid' => $val['id'], 'show' => 1])
                ->order('displayorder asc')
                ->column('id,title');
        }
        return $spec;
    }

    public function goodsSku($goodsId)
    {
        $field = 'title,stock,marketprice,productprice,costprice,goodssn,productsn,weight';
        return GoodsOption::getInstance()->where(['goodsid' => $goodsId])->column($field);
    }

    /**
     * 商品详情
     * @param $goodsId
     * @param $field
     * @return array
     */
    public function goodsDetail($goodsId, $field): array
    {
        $where = [
            ['id', '=', $goodsId],
            ['uniacid', '=', 2],
            ['status', '=', 1],
            ['deleted', '=', 0],
        ];
        $info = Goods::getInstance()
            ->where($where)
            ->field($field)
            ->find();
        if (empty($info)) {
            return [];
        }
        $info = $info->toArray();
        $info['video'] = UrlConst::IMG_URL . $info['video'];
        $info['thumbUrl'] = unserialize($info['thumbUrl']);
        foreach ($info['thumbUrl'] as &$val) {
            $val = UrlConst::IMG_URL . $val;
        }
        return $info;
    }

    /**
     * 商品列表
     * @param $pcate
     * @param $ccate
     * @param $tcate
     * @param $title
     * @param $page
     * @param $pageSize
     * @param $field
     * @return array
     */
    public function goodsList($pcate, $ccate, $tcate, $title, $page, $pageSize, $field): array
    {
        $where = [
            ['uniacid', '=', 2],
            ['status', '=', 1],
            ['deleted', '=', 0],
        ];
        if ($pcate > 0) {
            $where[] = ['pcate', '=', $pcate];
        }
        if ($ccate > 0) {
            $where[] = ['ccate', '=', $ccate];
        }
        if ($tcate > 0) {
            $where[] = ['tcate', '=', $tcate];
        }
        if (!empty($title)) {
            $where[] = ['title', 'like', '%' . $title . '%'];
        }
        return Goods::getInstance()
            ->where($where)
            ->order('displayorder desc')
            ->limit(($page - 1) * $pageSize, $pageSize)
            ->column($field);
    }

    /**
     * 分类列表
     * @param $page
     * @param $pageSize
     * @param string $field
     * @return array
     */
    public function categoryList($page, $pageSize, string $field = '*'): array
    {
        $where = [
            ['uniacid', '=', 2],
            ['parentid', '=', 0],
            ['enabled', '=', 1]
        ];
        return Category::getInstance()
            ->where($where)
            ->order('displayorder desc')
            ->limit(($page - 1) * $pageSize, $pageSize)
            ->column($field);
    }

    /**
     * 商店列表
     * @param $city
     * @param $page
     * @param $pageSize
     * @param string $field
     * @return array
     */
    public function storeList($city, $page, $pageSize, string $field = '*', $recommend = 0, $hot = 0, $groupId = 0): array
    {
        $where = [
            ['uniacid', '=', 2],
            ['status', 'in', [1, 3]],
        ];
        if ($recommend) {
            $where[] = ['isrecommand', '=', 1];
        }
        if ($hot) {
            $where[] = ['ishot', '=', 1];
        }
        if($groupId){
            $where[] = ['groupid', '=', $groupId];
        }
        return Merch::getInstance()
            ->where($where)
            ->limit(($page - 1) * $pageSize, $pageSize)
            ->column($field);
    }


    /**
     * @param $shopId
     * @return array
     */
    public function shopdetail($shopId): array
    {
        $store = Store::getInstance()->where(['id' => $shopId])->find();
        return empty($store) ? [] : $store->toArray();
    }
}