<?php


namespace app\api\services;

use app\api\cache\ShopCache;
use app\common\libs\Singleton;
use app\common\models\Sys\SystemUser;
use think\Collection;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class SystemUserService
{
    use Singleton;


    /**
     * 更新系统用户信息
     * @param $shopId
     * @param $shopData
     */
    public function updateUser($shopId, $shopData)
    {
        SystemUser::getInstance()->update($shopData, ['shop_id' => $shopId]);
    }
}