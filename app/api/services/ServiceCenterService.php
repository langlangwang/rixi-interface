<?php

namespace app\api\services;

use app\common\libs\Singleton;
use app\common\models\Member\Member;
use app\common\models\Order\Order;
use app\common\models\Order\OrderAddress;
use app\common\models\Order\OrderGoods;
use app\common\models\Order\OrderShop;
use app\common\models\Service\Service;
use app\common\models\Service\ServiceTag;
use app\common\models\Shop\Shop;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class ServiceCenterService
{
    use Singleton;

    /**
     * @throws ModelNotFoundException
     * @throws DbException
     * @throws DataNotFoundException
     */
    public function serviceList($page, $pageSize, $userId): array
    {
        $where = [
            ['deleted', '=', 0],
            ['user_id', '=', $userId]
        ];
        $list = Service::getInstance()
            ->where($where)
            ->page($page, $pageSize)
            ->order('id desc')
            ->select();
        if (empty($list)) {
            return [];
        }
        foreach($list as &$v){
            $v['allAddress'] = $v['service_province'].$v['service_city'].$v['service_country'].$v['detail_address'];
        }
        return $list->toArray();
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function serviceNearbyList($page, $pageSize, $lng, $lat): array
    {
        $where = [
            ['status', '=', 0],
            ['deleted', '=', 0],
        ];

        $juli = "ROUND(
        6378.138 * 2 * ASIN(
            SQRT(
                POW(
                    SIN(
                        (
                            " . $lat . " * PI() / 180 - lat * PI() / 180
                        ) / 2
                    ),
                    2
                ) + COS(" . $lat . " * PI() / 180) * COS(lat * PI() / 180) * POW(
                    SIN(
                        (
                            " . $lng . " * PI() / 180 - lng * PI() / 180
                        ) / 2
                    ),
                    2
                )
            )
        ) * 1000
    )";
        //$km = 1000000;
        //$where[] = [\think\facade\Db::raw($juli), '<=', (int)$km];

        $list = Service::getInstance()
            ->where($where)
            ->field('*,' . $juli . '  AS km')
            ->page($page, $pageSize)
            ->order('km asc')
            ->select();
        if (empty($list)) {
            return [];
        }
        foreach ($list as $k => &$val) {
            $val['km'] = $val['km'] / 1000;
            $val['allAddress'] = $val['serviceProvince'].$val['serviceCity'].$val['serviceCountry'].$val['detailAddress'];
            $val['tagList'] = ServiceTag::getInstance()->where('id','in',$val['tag'])->field('id,name')->select();
            /*if ($km > 0 && $km < $val['km']) {
                unset($list[$k]);
            }*/
        }
        return $list->toArray();
    }

    /**
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    public function orderDetail($orderNo,$serviceId,$userId): array
    {
        $where = [
            ['order_no', '=', $orderNo],
            ['service_id', '=', $serviceId],
        ];
        $order = Order::getInstance()
            ->where($where)
            //->field('id,status,order_no,user_id,sell_price,order_type,pay_price,postage,sale,pay_at,create_at,expire_at,is_comment,completion_at,check_user_id,service_id,take_time,take_mobile,check_user_id')
            ->order('id desc')
            ->find();
        if (empty($order)) {
            return [];
        }
        $order = $order->toArray();
        $map = [
            ['user_id','=',$userId],
            ['id','=',$serviceId],
            ['deleted','=',0]
        ];
        $service = Service::getInstance()->where($map)->count();
        if(empty($service)){
            return [];
        }
        $where = [
            ['order_no', '=', $orderNo],
        ];
        $orderShop = OrderShop::getInstance()->where($where)->find();
        // 供应商订单 没有 orderShop 数据，没有 shop_id
        if (empty($orderShop['shopId'])) {
            $order['shopInfo'] = [];
        } else {
            $order['shopInfo'] = ShopService::getInstance()->getShopInfoById(
                $orderShop['shopId'],
                'id,shop_logo,shop_name,kf_phone');
        }
        $orderInfo = OrderGoods::getInstance()->where($where)->field('is_freight_insurance,is_seven,status,quantity,sell_price,goods_id,goods_title,sku_title,goods_img')->select();
        $order['goodsInfo'] = $orderInfo;
        //核销信息
        $checkInfo = Member::getInstance()->where('id',$order['checkUserId'])->field('id,user_name,phone')->find();
        $checkInfo = !empty($checkInfo) ? $checkInfo : [];
        $order['checkInfo'] = $checkInfo;

        //买家信息
        $buyInfo = Member::getInstance()->where('id',$order['userId'])->field('id,user_name,phone')->find();
        $buyInfo = !empty($buyInfo) ? $buyInfo : [];
        $order['buyInfo'] = $buyInfo;
        $addressInfo = OrderAddress::getInstance()->where('order_no',$order['orderNo'])->field('province,city,country,address,real_name,phone')->find();
        $addressInfo = !empty($addressInfo) ? $addressInfo : [];
        $order['addressInfo'] = $addressInfo;
        return $order;
    }

    /**
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    public function checkList($userId, $page, $pageSize,$serviceId): array
    {
        $where = [
            ['check_user_id', '=', $userId],
            ['service_id','=',$serviceId]
        ];
        $list = Order::getInstance()
            ->where($where)
            ->page($page, $pageSize)
            ->field('id,status,order_no,user_id,sell_price,order_type,pay_price,postage,sale,pay_at,create_at,expire_at,is_comment,completion_at,check_user_id')
            ->order('completion_at desc,id desc')
            ->select();
        if (empty($list)) {
            return [];
        }
        $list = $list->toArray();

        foreach ($list as &$val) {
            $where = [
                ['order_no', '=', $val['orderNo']],
            ];
            $orderShop = OrderShop::getInstance()->where($where)->find();
            // 供应商订单 没有 orderShop 数据，没有 shop_id
            if (empty($orderShop['shopId'])) {
                $val['shopInfo'] = [];
            } else {
                $val['shopInfo'] = ShopService::getInstance()->getShopInfoById(
                    $orderShop['shopId'],
                    'id,shop_logo,shop_name,kf_phone');
            }

            $orderInfo = OrderGoods::getInstance()->where($where)->field('is_freight_insurance,is_seven,status,quantity,sell_price,goods_id,goods_title,sku_title,goods_img')->select();
            $val['goodsInfo'] = $orderInfo;
            $val['checkInfo'] = Member::getInstance()->where('id', $val['checkUserId'])->field('phone,user_name')->find();
        }
        return $list;
    }

    /**
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    public function orderList($page, $pageSize, $serviceId, $orderNo, $orderType, $userId): array
    {
        $where = [
            ['o.deleted', '=', 0],
        ];

        if(empty($serviceId)){
            $whereShop = [
                ['status','=',1],
                ['user_id','=',$userId]
            ];
            $shop = Shop::getInstance()->where($whereShop)->find();
            if(empty($shop)){
                return [];
            }
            $where[] = ['s.shop_id','=',$shop['id']];
        }

        if($serviceId){
            $where[] = ['o.service_id', '=', $serviceId];
        }
        /*if($orderType == 1){
            $where[] = ['order_type','in',[1,3]];
        }elseif($orderType == 2){
            $where[] = ['order_type','in',[2,4]];
        }*/

        $list = Order::getInstance()
            ->alias('o')
            ->leftJoin('order_shop s','o.order_no=s.order_no')
            ->leftJoin('member m','o.user_id=m.id')
            ->where($where)
            ->where('o.pay_at', 'not null')
            ->where(function ($query) use ($orderNo) {
                if(!empty($orderNo)){
                    $query->whereOr('m.phone','like','%'.$orderNo.'%');
                    $query->whereOr('o.take_mobile','like','%'.$orderNo.'%');
                    $query->whereOr('o.order_no','like','%'.$orderNo.'%');
                }
            })
            ->page($page,$pageSize)
            ->field('o.id,o.status,o.order_no,o.user_id,o.sell_price,o.order_type,o.pay_price,o.postage,o.sale,o.pay_at,o.create_at,o.expire_at,o.is_comment,o.service_id,o.check_user_id')
            ->order('o.id desc')
            ->select();

        if (empty($list)) {
            return [];
        }
        $list = $list->toArray();
        //echo Order::getInstance()->getLastSql();die;
        foreach ($list as &$val) {
            $where = [
                ['order_no', '=', $val['orderNo']],
            ];
            $orderShop = OrderShop::getInstance()->where($where)->find();
            // 供应商订单 没有 orderShop 数据，没有 shop_id
            if (empty($orderShop['shopId'])) {
                $val['shopInfo'] = (object) [];
            } else {
                $val['shopInfo'] = ShopService::getInstance()->getShopInfoById(
                    $orderShop['shopId'],
                    'id,shop_logo,shop_name,kf_phone');
            }

            $orderInfo         = OrderGoods::getInstance()->where($where)->field('is_freight_insurance,is_seven,status,quantity,sell_price,goods_id,goods_title,sku_title,goods_img')->select();
            $val['createAt']   = date('Y-m-d H:i', strtotime($val['createAt']));
            $expireTime        = strtotime($val['expireAt']) - time();
            $val['expireTime'] = max($expireTime, 0);
            $val['goodsInfo']  = empty($orderInfo) ? [] : $orderInfo->toArray();
        }
        return $list;
    }

}