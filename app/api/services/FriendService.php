<?php
namespace app\api\services;

use app\common\libs\Singleton;
use app\common\models\Member\Member;
use app\common\models\Member\MemberInviteFundLog;
use app\common\models\Member\MemberShopInviteFundLog;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class FriendService
{
    use Singleton;

    public function getUserShopAmount($inviteId, $userId): float
    {
        $where = [
            ['user_id', '=', $inviteId],
            ['status', '=', 1],
            ['from_user_id', 'in', $userId],
        ];

        return MemberShopInviteFundLog::getInstance()
            ->where($where)
            ->sum('money');
    }

    /**
     * @param $uid
     * @param $ids
     * @return mixed
     */
    public function getUserShopFundSumIds($uid, $ids)
    {
        $where = [
            ['user_id', '=', $uid],
            ['status', '=', 1],
            ['from_user_id', 'in', $ids],
        ];

        return MemberShopInviteFundLog::getInstance()
            ->where($where)
            ->group('from_user_id')
            ->column('id,sum(money) as su');
    }

    /**
     * @param $uid
     * @return int
     */
    public function friendShopListCount($uid): int
    {
        $where = [
            ['m.invite_id', '=', $uid],
            ['s.id', '>', 0],
        ];
        return Member::getInstance()->alias('m')
            ->leftJoin('member_shop s', 'm.id=s.user_id')
            ->field('s.shop_logo,s.shop_name,s.user_id,s.create_at,s.shop_type,m.invite_id')
            ->where($where)
            ->count('s.id');
    }

    /**
     * @param $uid
     * @param $page
     * @param $pageSize
     * @return array
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function friendShopList($uid, $page, $pageSize): array
    {
        $where = [
            ['m.invite_id', '=', $uid],
            ['s.id', '>', 0],
        ];
        $field = [
            'm.id',
            'm.invite_id',
            's.shop_logo',
            's.shop_name',
            's.user_id',
            's.create_at',
            's.shop_type',
            's.province', // 省id
            's.city', // 市id
            's.country', // 县id
            's.street', // 镇id
            's.comm', // 居委会ID
        ];
        $list = Member::getInstance()->alias('m')
            ->leftJoin('member_shop s', 'm.id=s.user_id')
            ->field($field)
            ->where($where)
            ->order('s.create_at desc')
            ->limit(($page - 1) * $pageSize, $pageSize)
            ->select();

        if (empty($list)) {
            return [];
        }
        return $list->toArray();
    }

    public function getUserFriendCountTeam($userId): int
    {
        $where = [
            ['invite_ids', 'like', '%-' . $userId . '-%'],
            ['invite_third_id', '<>', $userId],
            ['invite_id', '<>', $userId],
        ];
        return Member::getInstance()->where($where)->count('id');
    }

    public function getUserFriendThirdCount($userId): int
    {
        $where = [
            ['invite_third_id', '=', $userId],
        ];
        return Member::getInstance()->where($where)->count('id');
    }

    public function getUserFriendCount($userId): int
    {
        $where = [
            ['invite_id', '=', $userId],
        ];
        return Member::getInstance()->where($where)->count('id');
    }

    public function getUserSumAmount($userId, $inviteId): float
    {
        $where = [
            ['user_id', '=', $inviteId],
            ['from_user_id', '=', $userId],
            ['status', '=', 1],
        ];
        return MemberInviteFundLog::getInstance()->where($where)->sum('money');
    }

    /**
     * @param $uid
     * @param $ids
     * @return mixed
     */
    public function getUserFundSumIds($uid, $ids)
    {
        $where = [
            ['user_id', '=', $uid],
            ['status', '=', 1],
            ['from_user_id', 'in', $ids],
        ];

        return MemberInviteFundLog::getInstance()
            ->where($where)
            ->group('from_user_id')
            ->column('id,sum(money) as su');
    }

    /**
     * @param $uid
     * @param $page
     * @param $pageSize
     * @return array
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function friendList($uid, $page, $pageSize): array
    {
        $where = [
            ['invite_id', '=', $uid],
        ];
        return $this->getList($where, $page, $pageSize);
    }

    /**
     * @param $uid
     * @param $page
     * @param $pageSize
     * @return array
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function friendListThird($uid, $page, $pageSize): array
    {
        $where = [
            ['invite_third_id', '=', $uid],
        ];
        return $this->getList($where, $page, $pageSize);
    }

    /**
     * @param $uid
     * @param $page
     * @param $pageSize
     * @return array
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function friendListTeam($uid, $page, $pageSize): array
    {
        $where = [
            ['invite_ids', 'like', '%-' . $uid . '-%'],
            ['invite_third_id', '<>', $uid],
            ['invite_id', '<>', $uid],
        ];
        return $this->getList($where, $page, $pageSize);
    }

    private function getList($where, $page, $pageSize)
    {
        $field = [
            'id',
            'user_name',
            'avatar',
            'phone',
            'create_at',
            'province_id', // 省id
            'city_id', // 市id
            'country_id', // 县id
            'street_id', // 镇id
            'comm_id', // 居委会ID
        ];
        $list = Member::getInstance()
            ->where($where)
            ->field($field)
            ->order('id desc')
            ->limit(($page - 1) * $pageSize, $pageSize)
            ->select();
        if (empty($list)) {
            return [];
        }
        return $list->toArray();
    }
}
