<?php

namespace app\api\services;

use app\common\libs\Singleton;
use app\common\models\Member\MemberClickLog;
use app\common\models\Member\MemberNum;
use app\Request;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class MemberInfoService
{
    use Singleton;

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function memberClick($userId,$device,$phone): bool
    {
        //$device = request()->header('romName');
        $where = [
            ['user_id','=',$userId],
            ['create_at','>=',date('Y-m-d')]
        ];
        $ret = MemberClickLog::getInstance()->where($where)->find();
        if(!empty($ret)){
            $ret->update_at = date('Y-m-d H:i:s');
            $ret->save();
        }else{
            $data = [
                'user_id' => $userId,
                'device' => $device,
                'phone' => $phone,
                'update_at' =>  date('Y-m-d H:i:s'),
            ];
            MemberClickLog::getInstance()->insert($data);
        }
        return true;
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function memberNum($userId): array
    {
        $memberNum = MemberNum::getInstance()->where('user_id',$userId)->find();
        if(empty($memberNum)){
            $insertData = [
                'user_id' => $userId
            ];
            $ret = MemberNum::getInstance()->insert($insertData);
            if(!$ret){
                return [];
            }
            $memberNum = MemberNum::getInstance()->where('user_id',$userId)->find();
        }
        return !empty($memberNum) ? $memberNum->toArray() : [];
    }
}