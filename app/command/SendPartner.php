<?php
declare (strict_types = 1);

namespace app\command;

use app\api\services\CommunityService;
use app\api\services\PacksService;
use app\common\models\Community\CommunityOrder;
use app\common\models\Community\CommunityUser;
use app\common\models\Member\MemberCommunityAmountLog;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class SendPartner extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('send:partner')
            ->setDescription('the sendpartner command');
    }

    /**
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    protected function execute(Input $input, Output $output)
    {
        $output->writeln('开始：'.date('Y-m-d H:i:s'));
        $where = [
            ['order_no','like','%HA%'],
            ['status','=',2],
            ['create_at','between',['2025-02-24 00:00:00','2025-02-25 23:59:59']]
        ];
        $count = CommunityOrder::getInstance()->where($where)->count();
        dump('数量：'.$count);
        die;
        $list = CommunityOrder::getInstance()->where($where)->select();
        if($list){
            foreach($list as $v){
                $find = MemberCommunityAmountLog::getInstance()->where('order_no',$v['orderNo'])->find();
                if(!empty($find)){
                    continue;
                }
                MemberCommunityAmountLog::getInstance()->startTrans();
                try{
                    $communityUser = CommunityUser::getInstance()->where('community_id',$v['communityId'])->where('community_status',8)->find();
                    CommunityService::getInstance()->dividendHall($communityUser,$v['orderNo']);
                    MemberCommunityAmountLog::getInstance()->commit();
                }catch (\Exception $e){
                    MemberCommunityAmountLog::getInstance()->rollback();
                    dump($e->getMessage().$e->getLine());
                }
            }
        }
        $output->writeln('结束：'.date('Y-m-d H:i:s'));
    }
}
