<?php
declare (strict_types = 1);

namespace app\command;

use app\api\services\MemberService;
use app\common\models\Community\CommunityOrder;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

class ProfitPoolReissue extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('profit:pool:reissue')
            ->setDescription('分润池存入记录补发');
    }

    protected function execute(Input $input, Output $output)
    {
        $where = [
            ['create_at', '>=', '2024-06-21'],
            ['status', '=', 2]
        ];
        $order = CommunityOrder::getInstance()->where($where)->select();
        foreach ($order as $v){
            //MemberService::getInstance()->profitPoolLog($v['userId'], $v['price'], $v['order_no'], $v['premium'], 4);
        }

        // 指令输出
        $output->writeln('profit:pool:reissue');
    }
}
