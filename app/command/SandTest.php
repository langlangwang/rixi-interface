<?php
declare (strict_types = 1);

namespace app\command;

use app\api\cache\CarefreeCache;
use app\api\services\CarefreeInfoService;
use app\common\models\Carefree\CarefreeMemberFinance;
use app\common\models\Carefree\CarefreeRedAmountLog;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\App;

class SandTest extends Command
{

    protected function configure()
    {
        // 指令配置
        $this->setName('sand:test')
            ->setDescription('the sandtest command');
    }

    protected function execute(Input $input, Output $output)
    {
        $config = CarefreeCache::getKeyData('carefree','base');
        $val['userId'] = 1002654;
        $where = [
            ['user_id','=',$val['userId']],
            ['status','=',1]
        ];
        $redAmount = CarefreeRedAmountLog::getInstance()->where($where)->sum('amount') ?: 0;
        if($redAmount >= $config['fy_unit']){
            $reduce = bcdiv((string)$redAmount,$config['fy_unit']);
            $reduce = bcmul((string)$reduce, $config['base_rate'],2);
            //分红金额/1680 * 0.4
            $speedRate = CarefreeMemberFinance::getInstance()->where('user_id',$val['userId'])->value('speed_rate');
            $userSpeedRate = $speedRate;
            $speedRate = bcsub($speedRate,$reduce,2);
            if($speedRate >= $config['base_rate']){
                $msg = '积分分红达到临界点，扣减速度';
                dump('reduce:'.$reduce);
            }else{
                if($userSpeedRate > $config['base_rate']){
                    $reduce = bcsub($userSpeedRate,$config['base_rate'],2);
                    dump('reduce'.$reduce);
                }
            }
        }
        die;
        try{
            $publicKeyPath = App::getRootPath() . 'cert/sand-test.cer';
            $privateKeyPath =  App::getRootPath() . 'cert/sanduatprikey.pfx';
            $privateKeyPwd = "123456";
            //$URL = "https://openapi.sandpay.com.cn/v4/sd-wallet/api/m-wallet/account.onekey.access";
            $URL = "https://openapi-uat01.sand.com.cn/v4/sd-wallet/api/m-wallet/account.onekey.access";
            $publickey = $this->loadX509Cert($publicKeyPath);
            $prikey= $this->loadPk12Cert($privateKeyPath, $privateKeyPwd);
            //通用公共报文
            $publicData=[
                "accessMid"     => "HP200515",
                "timestamp"     => date('Y-m-d H:i:s'),
                "version"       => "4.0.0",
                "signType"      => "RSA",
                "sign"          => "",
                "encryptType"   => "AES",   //AES加密
                "encryptKey"    => "",
                "bizData"       => ""
            ];
            $time = date("Y-m-d H:i:s");
            $time = str_replace(' ', '', $time);
            $time = str_replace('-', '', $time);
            $time = str_replace(':', '', $time);
            $order= "Sandtest".$time.rand(6,999);
            //var_dump($order);
            $params =[
                "outOrderNo"    => $order,
                "bizUserNo"     => "pence01",
                "notifyUrl"     => "https://rx-test-api.rxx1316.com/api/callback/sandCallback",
                "userInfo"      =>
                    [
                        "nickName"      => "黎聚",
                        "name"          => "黎聚",
                        "idcardType"    => "01",
                        //"idcardNumber"  => "430525199701035317",
                        "idcardNumber"  => "431226199809270627",
                        "phone"         => "15821766024",
                        "lockFlag"     => "00", //01 不能修改开户实名信息， 默认00 可以修改

                    ]
            ];

            // 商户签名流程
            // 1. 生成一个16位的随机字符串aseKey，该字符串仅包含大小写字母及数字。
            // 2. 将随机字符串转为byte数组aesKeyBytes，编码格式为UTF_8。
            // 3. 将请求报文中的bizData域转为byte数组，编码格式为UTF_8，并用aesKeyBytes用AES算法对其加密，并对结果进行base64转码，得到加密报文体encryptValueBytes。
            // 4. 将随机字符串byte数组aesKeyBytes使用杉德公钥进行RSA算法加密，并将结果进行base64转码即得到得到sandEncryptKey。
            // 5. 将加密报文体使用商户私钥进行RSA算法签名，得到sign。

            if($publicData['encryptType']=='AES'){
                //step1 生成随机数
                $AESKey = $this->aes_generate(16);
                // step2、3: 使用AESKey加密报文 通过aesKeyBytes对Json进行AES加密生成data
                $publicData['bizData'] = $this->AESEncrypt($params, $AESKey);   //  AES 模式
                // step4: 把aesKeyBytes通过杉德公钥加密生成encryptKey，encryptType为"AES"
                $publicData['encryptKey'] = $this->RSAEncryptByPub($AESKey, $publickey); //  AES 模式
            }

            // step5: 将加密后的data，通过商户私钥进行签名生成sign，signType为"SHA256WithRSA"
            $publicData['sign'] = $this->sign($publicData['bizData'], $prikey);
            dump('请求地址:'.$URL);

            dump('请求参数：'.json_encode($publicData));
            // step6: post请求
            $result = $this->http_post_json($URL, $publicData);
            dump('返回参数：'.$result);
            $arr= json_decode($result,true);
            if($arr['respCode'] == 'fail'){
                echo $arr['respDesc'];
                return false;
            }
            //请求错误,没返回成功,打印错误
            if($publicData['encryptType']=='AES'){
                // step6: 使用公钥验签报文$decryptPlainText
                $verify = $this->verify($arr['bizData'], $arr['sign'], $publickey);
                // step7: 使用私钥解密AESKey
                $decryptAESKey = $this->RSADecryptByPri($arr['encryptKey'], $prikey);
                // step8: 使用解密后的AESKey解密报文
                $decryptPlainText = json_decode($this->AESDecrypt($arr['bizData'], $decryptAESKey));
            }


            $respon=  [
                'verify'    => $verify==1?'验签成功!':'验签失败!',
                'params'    => json_encode($params,JSON_UNESCAPED_UNICODE),
                'publicdata'    => json_encode($publicData),
                'response'     =>  $decryptPlainText,
            ];
            var_dump($respon);
        }catch (\Exception $e){
            echo $e->getLine();
            echo $e->getMessage();
        }


        // 指令输出
       //$output->writeln('sandtest');
    }

    /**
     * 生成AESKey
     * @param $size
     * @return string
     */
    function aes_generate($size)
    {
        $str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $arr = array();
        for ($i = 0; $i < $size; $i++) {
            $arr[] = $str[mt_rand(0, 61)];
        }

        return implode('', $arr);
    }
    /**
     * AES加密
     * @param $plainText
     * @param $key
     * @return string
     * @throws \Exception
     */
    function AESEncrypt($plainText, $key)
    {
        ksort($plainText);
        $plainText = json_encode($plainText,JSON_UNESCAPED_UNICODE);
        //var_dump($plainText);
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-ECB");
        $iv = !$ivlen?"": openssl_random_pseudo_bytes($ivlen);
        //$iv = openssl_random_pseudo_bytes($ivlen);
        $result = openssl_encrypt($plainText, 'AES-128-ECB', $key,OPENSSL_RAW_DATA,$iv);
        //var_dump($iv);
        if (!$result) {
            throw new \Exception('报文加密错误');
        }
        return base64_encode($result);
    }
    /**
     * AES解密
     * @param $cipherText
     * @param $key
     * @return string
     * @throws \Exception
     */
    function AESDecrypt($cipherText, $key)
    {
        $result = openssl_decrypt(base64_decode($cipherText), 'AES-128-ECB', $key, 1);

        if (!$result) {
            throw new \Exception('报文解密错误', 2003);
        }

        return $result;
    }

    function RSAEncryptByPub($plainText, $puk)
    {
        if (!openssl_public_encrypt($plainText, $cipherText, $puk, OPENSSL_PKCS1_PADDING)) {
            throw new \Exception('AESKey 加密错误');
        }

        return base64_encode($cipherText);
    }

    /**
     * 公钥验签
     * @param $plainText
     * @param $sign
     * @param $path
     * @return int
     * @throws Exception
     */
    function verify($plainText, $sign, $path)
    {
        $resource = openssl_pkey_get_public($path);
        $result = openssl_verify($plainText, base64_decode($sign), $resource,'SHA256');
        //在PHP8.0以后，即时编译（JIT）模式, 性能已经提高了90%, 你不再需要手动释放openssl_free_key()资源。
        //PHP的垃圾回收机制会自动处理这些内存管理问题。所以，你不需要担心内存占用的问题。
        openssl_free_key($resource);

        if (!$result) {
            throw new \Exception('签名验证未通过,plainText:' . $plainText . '。sign:' . $sign, '02002');
        }

        return $result;
    }

    /**
     * 私钥解密AESKey
     * @param $cipherText
     * @param $prk
     * @return string
     * @throws Exception
     */
    function RSADecryptByPri($cipherText, $prk)
    {
        if (!openssl_private_decrypt(base64_decode($cipherText), $plainText, $prk, OPENSSL_PKCS1_PADDING)) {
            throw new \Exception('AESKey 解密错误');
        }

        return (string)$plainText;
    }



    /**
     * 获取公钥
     * @param $path
     * @return mixed
     * @throws Exception
     */
    function loadX509Cert($path)
    {
        try {
            $file = file_get_contents($path);
            if (!$file) {
                throw new \Exception('loadx509Cert::file_get_contents ERROR');
            }
            $cert = chunk_split(base64_encode($file), 64, "\n");
            $cert = "-----BEGIN CERTIFICATE-----\n" . $cert . "-----END CERTIFICATE-----\n";
            $res = openssl_pkey_get_public($cert);
            $detail = openssl_pkey_get_details($res);
            openssl_free_key($res);

            if (!$detail) {
                throw new \Exception('loadX509Cert::openssl_pkey_get_details ERROR');
            }

            return $detail['key'];
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * 获取私钥
     * @param $path
     * @param $pwd
     * @return mixed
     * @throws Exception
     */
    function loadPk12Cert($path, $pwd)
    {
        try {
            $file = file_get_contents($path);
            if (!$file) {
                throw new \Exception('loadPk12Cert::file
                        _get_contents');
            }

            if (!openssl_pkcs12_read($file, $cert, $pwd)) {
                throw new \Exception('loadPk12Cert::openssl_pkcs12_read ERROR');
            }
            return $cert['pkey'];
        } catch (\Exception $e) {
            throw $e;
        }
    }
    /**
     * 私钥签名
     * @param $plainText
     * @param $path
     * @return string
     * @throws Exception
     */
    function sign($plainText, $path)
    {
        //$plainText = json_encode($plainText);
        try {
            $resource = openssl_pkey_get_private($path);
            $result = openssl_sign($plainText, $sign, $resource,OPENSSL_ALGO_SHA256);
            //在PHP8.0以后，即时编译（JIT）模式, 性能已经提高了90%, 你不再需要手动释放openssl_free_key()资源。
            //PHP的垃圾回收机制会自动处理这些内存管理问题。所以，你不需要担心内存占用的问题。
            openssl_free_key($resource);

            if (!$result) {
                throw new \Exception('签名出错' . $plainText);
            }

            return base64_encode($sign);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * 发送请求
     * @param $url
     * @param $param
     * @return bool|mixed
     * @throws Exception
     */
    function http_post_json($url, $param)
    {
        if (empty($url) || empty($param)) {
            return false;
        }
        //NONE模式
        if($param['encryptType']=='NONE'){
            $param['bizData']=json_decode($param['bizData']);  //字符串转对象
        }
        $param = json_encode($param,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        try {

            $ch = curl_init();//初始化curl
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //正式环境时解开注释
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            $data = curl_exec($ch);//运行curl
            curl_close($ch);

            if (!$data) {
                throw new \Exception('请求出错');
            }
            return $data;
        } catch (\Exception $e) {
            throw $e;
        }

    }
}
