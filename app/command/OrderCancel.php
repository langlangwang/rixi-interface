<?php


namespace app\command;


use app\common\models\Order\Order;
use app\common\models\Order\OrderGoods;
use app\common\models\Order\OrderShop;
use think\console\Command;
use think\console\Input;
use think\console\Output;

class OrderCancel extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('order:cancel')
            ->setDescription('订单过期');
    }

    protected function execute(Input $input, Output $output)
    {
        $output->info("开始:");
        $order = Order::getInstance()->where('status', '=',0)->where('expire_at', '<', date('Y-m-d H:i:s'))->select();
        foreach ($order as $val){
            Order::getInstance()->startTrans();
            try {
                $where = [
                    ['order_no','=', $val['orderNo']],
                    ['status','=', 0],
                ];
                $update = [
                    'status' => 8,
                    'refund_at' => date('Y-m-d H:i:s')
                ];
                Order::getInstance()->where($where)->update($update);
                OrderShop::getInstance()->where($where)->update($update);
                OrderGoods::getInstance()->where($where)->update($update);

            } catch (\Exception $e) {
                Order::getInstance()->rollback();
            }
            Order::getInstance()->commit();
        }


        $output->info("结束:");
    }
}