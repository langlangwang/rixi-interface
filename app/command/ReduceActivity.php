<?php
declare (strict_types = 1);

namespace app\command;

use app\api\services\MemberService;
use app\common\models\Member\Member;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\facade\Log;

class ReduceActivity extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('reduce:activity')
            ->setDescription('连续不签到不消费');
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    protected function execute(Input $input, Output $output)
    {
        $where = [
            ['status','=',1],
            ['is_logout','=',0],
            ['deleted','=',0],
        ];
        $page = 1;
        $pageSize = 100;
        $count = Member::getInstance()->where($where)->count();
        //总页数
        $total = ceil($count / $pageSize);
        //统计数量
        $num = 0;
        $output->writeln('开始：' . date('Y-m-d H:i:s'));
        while ($page <= $total) {
            $list = Member::getInstance()->where($where)->page($page, $pageSize)->select();
            foreach($list as $v){
                try{
                    //连续不签到
                    MemberService::getInstance()->setActive($v['id'],1,2);
                    //连续不消费
                    MemberService::getInstance()->setActive($v['id'],2,2);
                }catch (\Exception $e){
                    Log::error($e->getMessage());
                }
                $num++;
                $output->writeln('当前活跃值数量：' . $num);
            }
            $page++;
        }
        $output->writeln('结束：' . date('Y-m-d H:i:s'));
        $output->writeln('处理活跃值数量：' . $num);
    }
}
