<?php
declare (strict_types = 1);

namespace app\command;

use app\api\services\SupplyService;
use app\common\models\Order\Order;
use app\common\models\Order\OrderGoods;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class SupplyOrder extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('supply:order')
            ->setDescription('the supplyorder command');
    }

    /**
     * 处理供应链订单
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    protected function execute(Input $input, Output $output)
    {
        $this->checkRefundOrder();

        $where = [
            'status'     => 1,
            'order_type' => 4,
        ];
        $orderList = \app\common\models\ExchangeOrder::getInstance()
            ->where($where)
            ->select();
        if ( ! empty($orderList)) {
            foreach ($orderList as $v) {
                SupplyService::getInstance()->changeExchangeOrder($v);
            }
        }

        //
        $where = [
            'status'     => 1,
            'order_type' => 4,
        ];
        $orderList = Order::getInstance()->where($where)->select();
        if (empty($orderList)) {
            return false;
        }
        foreach ($orderList as $v) {
            SupplyService::getInstance()->changeOrder($v);
        }

        $where = [
            'status' => 1,
            'shop_id' => 12,
        ];
        $orderGoods = OrderGoods::getInstance()->where($where)->select();
        if (empty($orderGoods)) {
            return false;
        }
        foreach ($orderGoods as $v) {
            SupplyService::getInstance()->changeOnlineOrder($v);
        }
        // 指令输出
        $output->writeln('supplyorder');
    }

    /**
     * 查询售后单详细信息
     *
     * @return [type] [description]
     */
    private function checkRefundOrder()
    {
        // 售后单状态(0进行中 1 已拒绝 2 审核通过  3 已退货 4已取消 5 已完成)
        $where = [
            ['status', 'in', [0, 2]],
            ['supply_refund_no', '<>', ''],
        ];
        $orderList = \app\common\models\Order\OrderRefund::getInstance()
            ->field(['order_refund_id', 'supply_refund_no', 'send_time'])
            ->where($where)
            ->select();
        // var_dump($orderList);
        // echo \app\common\models\Order\OrderRefund::getlastsql();
        // exit;
        if ( ! empty($orderList)) {
            $refuse_desc_map = [
                1  => '待卖家审核',
                2  => '卖家拒绝退款',
                3  => '退款成功',
                4  => '卖家拒绝退货退款',
                5  => '待买家退货',
                6  => ',买家已退货，待卖家收货',
                7  => '买家已退货，卖家拒绝收货',
                8  => '卖家已收货,待确认退款',
                9  => '退货退款成功',
                10 => '卖家拒绝换货',
                11 => '买家已退货，待卖家换货',
                12 => '卖家已换货，待买家收货',
                13 => '换货成功',
                14 => '已关闭',
                15 => '卖家同意退款',
                16 => '卖家同意退款',
                17 => '卖家拒绝退款',
                18 => '退款成功',
            ];
            foreach ($orderList as $v) {
                // var_dump($v['supply_refund_no']);exit;
                $ret = SupplyService::getInstance()->orderRefundDetail(
                    $v['supply_refund_no']
                );
                if ($ret['status'] != 200 || empty($ret['data'])) {
                    continue;
                }
                // 售后状态（1,待卖家审核2,卖家拒绝退款：3,退款成功：4,卖家拒绝退货退款：5,待买家退货：6,买家已退货，待卖家收货：7,买家已退货，卖家拒绝收货：8,卖家已收货,待确认退款：9,退货退款成功：10,卖家拒绝换货：11,买家已退货，待卖家换货：12,卖家已换货，待买家收货：13,换货成功：14,已关闭：15,卖家同意退款,16：卖家同意退款, 17：卖家拒绝退款，18：退款成功）
                $returnStatus = $ret['data']['returnStatus'];
                if (in_array($returnStatus, [3, 6, 8, 9, 11, 12, 13, 15, 16, 18])) {
                    // 这些状态不需要处理
                    // \app\common\models\Order\OrderRefund::getInstance()
                    //     ->where('order_refund_id', $v['order_refund_id'])
                    //     ->save([
                    //         // 售后单状态(0进行中 1 已拒绝 2 审核通过  3 已退货 4已取消 5 已完成)
                    //         'status'    => 5,
                    //         'update_at' => date('Y-m-d H:i:s'),
                    //     ]);
                } elseif (in_array($returnStatus, [5])) {
                    \app\common\models\Order\OrderRefund::getInstance()
                        ->where('order_refund_id', $v['order_refund_id'])
                        ->save([
                            // 售后单状态(0进行中 1 已拒绝 2 审核通过  3 已退货 4已取消 5 已完成)
                            'status'    => 2,
                            // 商家审核状态(0待审核 10已同意 20已拒绝)
                            'is_agree'  => 10,
                            'update_at' => date('Y-m-d H:i:s'),
                            'auth_time' => empty($v['modifyTime']) ? date('Y-m-d H:i:s') : $v['modifyTime'],
                        ]);
                } elseif (in_array($returnStatus, [2, 4, 7, 10, 17])) {
                    \app\common\models\Order\OrderRefund::getInstance()
                        ->where('order_refund_id', $v['order_refund_id'])
                        ->save([
                            // 售后单状态(0进行中 1 已拒绝 2 审核通过  3 已退货 4已取消 5 已完成)
                            'status'      => 1,
                            // 商家审核状态(0待审核 10已同意 20已拒绝)
                            'is_agree'    => 20,
                            'update_at'   => date('Y-m-d H:i:s'),
                            'refuse_desc' => $refuse_desc_map[$returnStatus] ?? '',
                            'auth_time'   => empty($v['modifyTime']) ? date('Y-m-d H:i:s') : $v['modifyTime'],
                        ]);
                }
            }
        }
    }
}
