<?php
declare (strict_types=1);

namespace app\command;

use app\api\consDir\SupplyConst;
use app\api\services\SupplyService;
use app\common\models\Supply\SupplyCate;
use app\common\models\Supply\SupplyGoods;
use app\common\models\Supply\SupplySku;
use app\common\utils\CommonUtil;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\db\exception\DbException;

class SupplyPullGoods extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('supply:pull:goods')
            ->setDescription('the supply:pull:goods command');
    }

    /**
     * @throws DbException
     */
    protected function execute(Input $input, Output $output)
    {
        /*$ret = SupplyService::getInstance()->getData(SupplyConst::GET_CATEGORYS,['id'=>0]);
        if($ret['status'] == 200 && !empty($ret['data'])){
            foreach($ret['data'] as $v){
                SupplyCate::getInstance()->insert($v);
                if($v['id']){
                    $ret = SupplyService::getInstance()->getData(SupplyConst::GET_CATEGORYS,['id'=>$v['id']]);
                    if($ret['status'] == 200 && !empty($ret['data'])){
                        foreach($ret['data'] as $vv){
                            SupplyCate::getInstance()->insert($vv);
                            $ret = SupplyService::getInstance()->getData(SupplyConst::GET_CATEGORYS,['id'=>$vv['id']]);
                            if($ret['status'] == 200 && !empty($ret['data'])){
                                foreach($ret['data'] as $vvv){
                                    SupplyCate::getInstance()->insert($vvv);
                                }
                            }
                        }
                    }
                }
            }
        }*/
        $page = 1;
        $num = 1;
        while (true) {
            $data = [
                "hasVideo" => -1,
                "pageIndex" => $page,
                "pageSize" => 100,
                "status" => -1,
            ];
            $ret = SupplyService::getInstance()->getData(SupplyConst::GET_SPU_ID_LIST, $data);
            if ($ret['status'] != 200 || empty($ret['data'])) {
                break;
            }
            //SupplyService::getInstance()->updateGoods($ret['data']['spuIdList']);

            $ret = SupplyService::getInstance()->getData(SupplyConst::GET_SPU_BY_SPUIDS, ['spuIdList' => $ret['data']['spuIdList']]);
            if ($ret['status'] != 200 || empty($ret['data'])) {
                break;
            }
            foreach ($ret['data'] as $v) {
                $output->writeln('更新当前商品数量：' . $num);
                $num++;
                if (!empty($v['carouselImgList'])) {
                    $v['coverImg'] = $v['carouselImgList'][0];
                    $v['carouselImgList'] = implode(',',$v['carouselImgList']);
                }
                foreach ($v as $kk => $vv) {
                    if (is_array($vv)) $v[$kk] = json_encode($vv, 256);
                }
                $v = CommonUtil::camelToUnderLine($v);
                unset($v['lib_name'],$v['lib_type']);
                $count = SupplyGoods::getInstance()->where('spu_id', $v['spu_id'])->count();
                if ($count == 0) {
                    SupplyGoods::getInstance()->insert($v);
                } else {
                    SupplyGoods::getInstance()->where('spu_id', $v['spu_id'])->update($v);
                }
                $skuList = SupplyService::getInstance()->getData(SupplyConst::LIST_SKU_BY_SPU_ID, ['spuId' => $v['spu_id']]);
                if ($skuList['status'] != 200 || empty($skuList['data'])) {
                    continue;
                }
                //sku
                $marketPrice = 0;
                $sellPrice = 0;
                foreach ($skuList['data'] as $skuK => $skuV) {
                    foreach ($skuV as $kkk => $vvv) {
                        if (is_array($vvv)) $skuV[$kkk] = json_encode($vvv, 256);
                    }
                    if ($skuK == 0) {
                        $marketPrice = $skuV['officialDistriPrice'];
                        $sellPrice = $skuV['suggestPrice'];
                    }
                    $marketPrice = min($marketPrice, $skuV['officialDistriPrice']);
                    $sellPrice = min($sellPrice, $skuV['suggestPrice']);

                    $skuV = CommonUtil::camelToUnderLine($skuV);
                    $count = SupplySku::getInstance()->where('sku_id', $skuV['sku_id'])->count();
                    $skuV['spu_id'] = $v['spu_id'];
                    if ($count == 0) {
                        SupplySku::getInstance()->insert($skuV);
                    } else {
                        SupplySku::getInstance()->where('sku_id', $skuV['sku_id'])->update($skuV);
                    }
                }
                SupplyGoods::getInstance()->where('spu_id', $v['spu_id'])->update(['market_price' => $marketPrice, 'sell_price' => $sellPrice]);
            }
            $page++;
        }
        $output->writeln('更新全部商品数量：' . $num);
        // 指令输出
        $output->writeln('supplypullgoods');
    }
}
