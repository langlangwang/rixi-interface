<?php


namespace app\command;


use app\api\cache\DialCache;
use app\api\consDir\CallConst;
use app\api\services\DialService;
use app\api\services\RabbitMqService;
use app\common\models\Community\Community;
use app\common\models\Dial;
use app\common\models\Sys\Area;
use think\console\Command;
use think\console\Input;
use think\console\Output;

class Test extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('test:test')
            ->setDescription('the  test command');
    }

    protected function execute(Input $input, Output $output)
    {
        $output->info("开始:");
        $true = true;
        $i = 0;
        while ($true){
            $co = Community::getInstance()->where('province_id',65)->order('id asc')->limit(($i*100),100)->column('*');
            if(empty($co)){
                $true = false;
                continue;
            }
            $output->info("开始处理:".($i*100+100));
            $model = Area::getInstance();
            foreach ($co as $val){
                //1省 2市 3区 4街道 5社区
                $data = [
                    'this_id' => $val['province_id'],
                    'name' => $val['province'],
                    'type' => 1
                ];
                $info = $model->where($data)->find();
                if(empty($info)){
                    $model->insert($data);
                }

                //市
                $data = [
                    'p_id' => $val['province_id'],
                    'this_id' => $val['city_id'],
                    'name' => $val['city'],
                    'type' => 2
                ];
                $info = $model->where($data)->find();
                if(empty($info)){
                    $model->insert($data);
                }
                //区
                $data = [
                    'p_id' => $val['city_id'],
                    'this_id' => $val['country_id'],
                    'name' => $val['country'],
                    'type' => 3
                ];
                $info = $model->where($data)->find();
                if(empty($info)){
                    $model->insert($data);
                }
                //街道
                $data = [
                    'p_id' => $val['country_id'],
                    'this_id' => $val['street_id'],
                    'name' => $val['street'],
                    'type' => 4
                ];
                $info = $model->where($data)->find();
                if(empty($info)){
                    $model->insert($data);
                }

                //社区
                $data = [
                    'p_id' => $val['street_id'],
                    'this_id' => $val['comm_id'],
                    'name' => $val['comm'],
                    'type' => 5
                ];
                $info = $model->where($data)->find();
                if(empty($info)){
                    $model->insert($data);
                }
            }
            $i++;
        }
        $output->info("结束:");
    }
}