<?php
declare (strict_types=1);

namespace app\command;

use app\api\cache\Marketing;
use app\api\cache\RedisCache;
use app\api\services\MemberService;
use app\common\models\Member\Finance;
use app\common\models\Member\Member;
use app\common\models\Member\MemberGxzLog;
use app\common\models\Member\MemberProfitLog;
use app\common\models\Member\MemberProfitPoolLog;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\facade\Db;

class Profit extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('profit')
            ->setDescription('the profit command');
    }

    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    protected function execute(Input $input, Output $output)
    {
        Finance::getInstance()->where('user_id', '>', 0)->update(['day_bonus' => 0]);

        //分润
        $data = Marketing::getKeyData('profit', 'set');

        //禁用
        if ($data['status'] != 1) {
            return false;
        }
        //1每天 2每周 3每月
        if ($data['time_type'] == 2 && date('w') != $data['week']) {
            return false;
        }

        if ($data['time_type'] == 3 && date('j') != $data['month']) {
            return false;
        }

        //不是自动领取
        if ($data['get_type'] != 1) {
            return false;
        }

        if($data['profit_type'] == 1){
            //按贡献值加权分
            MemberService::getInstance()->gxzProfit($data,$output);
        }else{
            //按会员等级分
            MemberService::getInstance()->levelProfit($data,$output);
        }

        // 指令输出
        $output->writeln('结束：' . date('Y-m-d H:i:s'));
    }
}
