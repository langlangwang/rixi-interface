<?php
declare (strict_types = 1);

namespace app\command\logic;

use app\command\logic\BaseLogic;
use think\console\Output;

/**
 * 手动测试功能
 * php think cli test --msg='{"scene":"user_level_check","uid":1002395}'
 */
class CliTestLogic extends BaseLogic implements CliInterface
{

    /**
     * 入口方法
     * {@inheritdoc}
     */
    public function run(Output $output, array $msg): bool
    {
        $scene = $msg['scene'] ?? '';
        if ($scene == 'user_level_check') {
            \app\api\services\UserLevelService::getInstance()->checkUpgrade($msg['uid']);
        }
        return true;
    }
}
