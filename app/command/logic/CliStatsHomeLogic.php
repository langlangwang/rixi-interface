<?php
declare (strict_types = 1);

namespace app\command\logic;

use app\common\models\Community\CommunityOrder;
use think\console\Output;

/**
 * 统计-控制台数据
 * php think cli stats_home --msg='{"days":30}'
 */
class CliStatsHomeLogic extends BaseLogic implements CliInterface
{

    /**
     * 入口方法
     * {@inheritdoc}
     */
    public function run(Output $output, array $msg): bool
    {
        $insertData = [];
        $date       = isset($msg['date']) ? ($msg['date'] == 'today' ? date('Y-m-d') : $msg['date']) : null;
        if ( ! empty($date)) {
            $insertData[] = self::dataForHome($date);
        } else {
            $days = intval($msg['days'] ?? 1);
            for ($i = 1; $i <= $days; $i++) {
                $date = date('Y-m-d', strtotime("-$i days"));
                // echo $date . "\n";exit;
                $insertData[] = self::dataForHome($date);
            }
        }
        if ( ! empty($insertData)) {
            \think\facade\Db::table('stats_home')
                ->replace()
                ->insertAll($insertData);
        }
        return true;
    }

    private function dataForHome($date)
    {
        $end = $date . ' 23:59:59';

        $field = [
            'count(*) AS new_member_count',
            'COALESCE(SUM(CASE WHEN is_vip = 1 THEN 1 ELSE 0 END), 0) AS new_vip_count',
            'COALESCE(SUM(CASE WHEN is_partner = 1 THEN 1 ELSE 0 END), 0) AS new_partner_count',
        ];
        $data = \think\facade\Db::table('member')
            ->field($field)
            ->whereBetweenTime('create_at', $date, $end)
            ->find();

        $data['date'] = $date;

        CommunityOrder::getInstance()
            ->where('pay_at', 'not null')
            ->where('status',3)
            ->whereBetweenTime('create_at', $date, $end)
            ->update(['status' => 2]);

        $data['new_shop_count'] = \think\facade\Db::table('member_shop')
            ->where('status', 1)
            ->whereBetweenTime('create_at', $date, $end)
            ->count();
        $field = [
            'count(*) AS community_order_count',
            'COALESCE(SUM(CASE WHEN status = 2 THEN total_amount ELSE 0 END), 0) AS community_order_price',
        ];
        $community = \think\facade\Db::table('community_order')
            ->field($field)
            ->where('status', 2)
            ->whereBetweenTime('create_at', $date, $end)
            ->find();
        $data['community_order_count'] = $community['community_order_count'];

        $field = [
            'count(*) AS packs_order_count',
            'COALESCE(SUM(real_price), 0) AS packs_order_price',
        ];
        $packs = \think\facade\Db::table('packs_order')
            ->field($field)
            ->where('status', 'in', [1,2,3])
            ->whereBetweenTime('create_at', $date, $end)
            ->find();
        $data['packs_order_count'] = $packs['packs_order_count'];
        $field                     = [
            'COALESCE(SUM(CASE WHEN order_type = 1 or order_type = 3 THEN 1 ELSE 0 END), 0) AS offline_order_count',
            'COALESCE(SUM(CASE WHEN order_type = 1 or order_type = 3 THEN pay_price ELSE 0 END), 0) AS offline_order_price',
            'COALESCE(SUM(CASE WHEN order_type = 2 THEN 1 ELSE 0 END), 0) AS online_order_count',
            'COALESCE(SUM(CASE WHEN order_type = 2 THEN pay_price ELSE 0 END), 0) AS online_order_price',
            'COALESCE(SUM(CASE WHEN order_type = 4 THEN 1 ELSE 0 END), 0) AS supply_order_count',
            'COALESCE(SUM(CASE WHEN order_type = 4 THEN pay_price ELSE 0 END), 0) AS supply_order_price',
            'COALESCE(SUM(CASE WHEN order_type = 1 and order_type = 2 and order_type = 3 and status = 1 THEN 1 ELSE 0 END), 0) AS wait_order_count',
            'COALESCE(SUM(CASE WHEN order_type = 4 and status = 1 THEN 1 ELSE 0 END), 0) AS wait_supply_order_count',
        ];
        $order = \think\facade\Db::table('order')
            ->field($field)
            ->where('status', '>', 0)
            ->whereBetweenTime('create_at', $date, $end)
            ->find();

        $data['offline_order_count']   = $order['offline_order_count'];
        $data['online_order_count']    = $order['online_order_count'];
        $data['supply_order_count']    = $order['supply_order_count'];
        $data['community_order_price'] = $community['community_order_price'];
        $data['packs_order_price']     = $packs['packs_order_price'];
        $data['online_order_price']    = $order['online_order_price'];
        $data['offline_order_price']   = $order['offline_order_price'];
        $data['supply_order_price']    = $order['supply_order_price'];

        $data['wait_order_count']        = $order['wait_order_count'];
        $data['wait_supply_order_count'] = $order['wait_supply_order_count'];
        $data['wait_shop_count']         = \think\facade\Db::table('member_shop')
            ->where('status', '=', 0)
            ->count();
        $data['wait_member_withdraw'] = \think\facade\Db::table('member_withdraw')
            ->where('status', '=', 0)
            ->count();
        $data['wait_shop_withdraw'] = 0;
        return $data;
    }
}
