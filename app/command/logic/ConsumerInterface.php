<?php
declare (strict_types = 1);

namespace app\command\logic;

interface ConsumerInterface
{
    /**
     * 运行接口的方法
     *
     * @param array $params 参数数组
     * @return bool 返回布尔值结果
     */
    public function run(array $params): bool;
}
