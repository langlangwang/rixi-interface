<?php
declare (strict_types = 1);

namespace app\command\logic;

use app\common\models\Member\MemberAmountLog;
use app\common\models\Member\MemberCommunityAmountLog;
use app\common\models\Member\MemberDigitalMoneyLog;
use app\common\models\Member\MemberGxzLog;
use think\console\Output;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

/**
 * 统计-财务明细
 * php think cli stats_finance_detail --msg='{"days":30}'
 */
class CliStatsFinanceDetailLogic extends BaseLogic implements CliInterface
{
    /**
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    public function run(Output $output, array $msg): bool
    {
        $insertData = [];
        $date       = isset($msg['date']) ? ($msg['date'] == 'today' ? date('Y-m-d') : $msg['date']) : null;
        if ( ! empty($date)) {
            $insertData[] = self::dataForHome($date);
        } else {
            $days = intval($msg['days'] ?? 1);
            for ($i = 1; $i <= $days; $i++) {
                $date = date('Y-m-d', strtotime("-$i days"));
                // echo $date . "\n";exit;
                $insertData[] = self::dataForHome($date);
            }
        }
        if ( ! empty($insertData)) {
            \think\facade\Db::table('stats_finance_detail')
                ->replace()
                ->insertAll($insertData);
        }
        return true;
    }

    /**
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    private function dataForHome($date): array
    {
        $end = $date . ' 23:59:59';

        $where = [
            ['create_at', 'between', [$date, $end]],
        ];

        $field  = 'SUM(if(status=1,amount,0)) as amount_in,SUM(if(status=2,amount,0)) as amount_out';
        $amount = MemberAmountLog::getInstance()->field($field)->where($where)->find();

        $gxz   = MemberGxzLog::getInstance()->where($where)->field($field)->where($where)->find();
        $field = 'SUM(if(type=0 and status=1,amount,0)) as community_in,SUM(if(type=0 and status=2,amount,0)) as community_out,
            SUM(if(type=1 and status=1,amount,0)) as packs_in,SUM(if(type=1 and status=2,amount,0)) as packs_out,
            SUM(if(type=2 and status=1,amount,0)) as invite_in,SUM(if(type=2 and status=2,amount,0)) as invite_out,
            SUM(if(type=3 and status=1,amount,0)) as bonus_in,SUM(if(type=3 and status=2,amount,0)) as bonus_out';

        $green = MemberCommunityAmountLog::getInstance()->where($where)->field($field)->where($where)->find();
        $field = 'SUM(if(status=1,amount,0)) as amount_in,SUM(if(status=2,amount,0)) as amount_out';

        $digital = MemberDigitalMoneyLog::getInstance()->where($where)->field($field)->where($where)->find();
        $data    = [
            'amount_in'                 => $amount['amountIn'] ?: 0,
            'amount_out'                => $amount['amountOut'] ?: 0,
            'gxz_in'                    => $gxz['amountIn'] ?: 0,
            'gxz_out'                   => $gxz['amountOut'] ?: 0,
            'community_green_score_in'  => $green['communityIn'] ?: 0,
            'community_green_score_out' => $green['communityOut'] ?: 0,
            'packs_green_score_in'      => $green['packsIn'] ?: 0,
            'packs_green_score_out'     => $green['packsOut'] ?: 0,
            'invite_green_score_in'     => $green['inviteIn'] ?: 0,
            'invite_green_score_out'    => $green['inviteOut'] ?: 0,
            'bonus_green_score_in'      => $green['bonusIn'] ?: 0,
            'bonus_green_score_out'     => $green['bonusOut'] ?: 0,
            'digital_asset_in'          => $digital['amountIn'] ?: 0,
            'digital_asset_out'         => $digital['amountOut'] ?: 0,
            'lang_score_in'             => 0,
            'lang_score_out'            => 0,
            'create_at'                 => date('Y-m-d H:i:s'),
        ];
        $data['date'] = $date;

        return $data;
    }

}
