<?php
declare (strict_types = 1);

namespace app\command\logic;

use think\console\Output;

interface CliInterface
{
    /**
     * 运行接口的方法
     *
     * @param array $msg 参数数组
     * @return bool 返回布尔值结果
     */
    public function run(Output $output, array $msg): bool;
}
