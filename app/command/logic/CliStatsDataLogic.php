<?php
declare (strict_types = 1);

namespace app\command\logic;

use app\command\logic\BaseLogic;
use app\common\models\Member\Member;
use think\console\Output;

/**
 * 统计-订单交易数据
 * php think cli stats_data --msg='{"days":30}'
 */
class CliStatsDataLogic extends BaseLogic implements CliInterface
{

    /**
     * 入口方法
     * {@inheritdoc}
     */
    public function run(Output $output, array $msg): bool
    {
        $insertData = [];
        $date       = isset($msg['date']) ? ($msg['date'] == 'today' ? date('Y-m-d') : $msg['date']) : null;
        if ( ! empty($date)) {
            $insertData[] = self::dataForData($date);
        } else {
            $days = intval($msg['days'] ?? 1);
            for ($i = 1; $i <= $days; $i++) {
                $date = date('Y-m-d', strtotime("-$i days"));
                // echo $date . "\n";exit;
                $insertData[] = self::dataForData($date);
            }
        }
        if ( ! empty($insertData)) {
            \think\facade\Db::table('stats_data')
                ->replace()
                ->insertAll($insertData);
        }
        return true;
    }

    private static function dataForData($date)
    {
        $end = $date . ' 23:59:59';

        $list = Member::getInstance()->where('deleted',0)->select();
        foreach($list as $v){
            try{
                $where = [
                    ['status','=',2],
                    ['user_id','=',$v['id']],
                    ['create_at','between', [$date, $end]]
                ];
                $community_today_total = \think\facade\Db::table('community_order')->where($where)->count();
                $where = [
                    ['user_id','=',$v['id']],
                ];
                $community_total = \think\facade\Db::table('community_user')->where($where)->count();
                $where = [
                    ['user_id','=',$v['id']],
                ];
                $account = \think\facade\Db::table('member_account')->where($where)->find();

                if($community_today_total == 0 && $community_total == 0){
                    continue;
                }

                $data = [
                    'user_id' => $v['id'],
                    'real_name' => $account['real_name'] ?? '',
                    'phone' => $v['phone'],
                    'community_total' => $community_total,
                    'community_today_total' => $community_today_total,
                    'date' => $date,
                    'reg_time' => $v['create_at']
                ];
                $where = [
                    ['date','=',$date],
                    ['user_id','=',$v['id']]
                ];
                $ret = \think\facade\Db::table('stats_data')->where($where)->find();
                if($ret){
                    $data['update_at'] = date('Y-m-d H:i:s');
                    \think\facade\Db::table('stats_data')->where($where)->update($data);
                }else{
                    \think\facade\Db::table('stats_data')->insert($data);
                }
            }catch (\Exception $ex){
                echo $ex->getMessage();
            }

        }
        return true;

    }
}
