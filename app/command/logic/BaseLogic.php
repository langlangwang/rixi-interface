<?php
declare (strict_types = 1);

namespace app\command\logic;

class BaseLogic
{
    public $error = '';

    public function getError()
    {
        return $error;
    }

}
