<?php
declare (strict_types = 1);

namespace app\command\logic;

use app\command\logic\BaseLogic;
use app\common\models\Member\Member;
use app\common\models\Member\MemberAgency;
use think\console\Output;

/**
 * 统计-订单交易数据
 * php think cli stats_community_member_data --msg='{"days":30}'
 */
class CliStatsCommunityMemberLogic extends BaseLogic implements CliInterface
{

    /**
     * 入口方法
     * {@inheritdoc}
     */
    public function run(Output $output, array $msg): bool
    {
        $this->dataForData();
        return true;
    }

    private static function dataForData(): void
    {
        $list = Member::getInstance()->where('deleted',0)->select();
        foreach($list as $v){
            try{
                $where = [
                    ['status','=',2],
                    ['user_id','=',$v['id']],
                ];
                $community_total = \think\facade\Db::table('community_order')->where($where)->count();
                $community_start_time = \think\facade\Db::table('community_order')->where($where)->order('id asc')->value('create_at');
                $community_end_time = \think\facade\Db::table('community_order')->where($where)->order('id desc')->value('create_at');
                $where = [
                    ['user_id','=',$v['id']],
                ];
                $current_total = \think\facade\Db::table('community_user')->where($where)->count();
                $account = \think\facade\Db::table('member_account')->where($where)->find();
                if($community_total == 0){
                    continue;
                }
                $identity = '普通会员';
                if($v['is_partner'] == 1){
                    $identity = '服务商 ';
                }
                $where = [
                    ['status','=',1],
                    ['deleted','=',0],
                    ['user_id','=',$v['id']]
                ];
                $agency = MemberAgency::getInstance()->where($where)->find();
                if($agency){
                    switch ($agency['agency_type']){
                        case 1:
                            $identity = $v['is_partner'] ? $identity.'/'.$agency['province_name'].'省代':$agency['province_name'].'省代';
                            break;
                        case 2:
                            $identity = $v['is_partner'] ? $identity.'/'.$agency['city_name'].'市代':$agency['city_name'].'市代';
                            break;
                        case 3:
                            $identity = $v['is_partner'] ? $identity.'/'.$agency['country_name'].'区代':$agency['country_name'].'区代';
                            break;
                        case 4:
                            $identity = $v['is_partner'] ? $identity.'/'.$agency['street_name'].'区代':$agency['street_name'].'区代';
                            break;
                    }
                }
                $data = [
                    'user_id' => $v['id'],
                    'real_name' => $account['real_name'] ?? '',
                    'phone' => $v['phone'],
                    'community_start_time' => $community_start_time,
                    'community_total' => $community_total,
                    'identity' => $identity,
                    'reg_time' => $v['create_at'],
                    'community_end_time' => $community_end_time,
                    'current_total' => $current_total,
                ];
                $where = [
                    ['user_id','=',$v['id']]
                ];
                $ret = \think\facade\Db::table('stats_community_member_data')->where($where)->find();
                if($ret){
                    $data['update_at'] = date('Y-m-d H:i:s');
                    \think\facade\Db::table('stats_community_member_data')->where($where)->update($data);
                }else{
                    \think\facade\Db::table('stats_community_member_data')->insert($data);
                }
            }catch (\Exception $ex){
                echo $ex->getMessage();
            }

        }

    }
}
