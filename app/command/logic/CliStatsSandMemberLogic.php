<?php
declare (strict_types = 1);

namespace app\command\logic;

use app\api\services\MemberService;
use app\api\services\SandService;
use app\command\logic\BaseLogic;
use app\common\models\Member\Member;
use app\common\models\Member\MemberAgency;
use app\common\models\Member\MemberRechargeOrder;
use DateInterval;
use DatePeriod;
use DateTime;
use think\console\Output;
use think\facade\Log;

/**
 * 统计-订单交易数据
 * php think cli stats_sand_member_data --msg='{"days":30}'
 */
class CliStatsSandMemberLogic extends BaseLogic implements CliInterface
{

    /**
     * 入口方法K
     * {@inheritdoc}
     */
    public function run(Output $output, array $msg): bool
    {
        if(isset($msg['date']) && $msg['date'] == 'today'){
            $this->dataForData(date('Ymd'));
        }elseif(isset($msg['date'])){
            $this->dataForData($msg['date']);
        }else{
            $this->dataForData(date('Ymd',strtotime('-1 day')));
        }

        return true;
    }

    private static function dataForData($date): void
    {
        dump($date);
        /*$startDate = new DateTime('20240401');
        $time = date('YmdHis',strtotime('-1 day'));
        $endDate = new DateTime($time);
        // 使用DateInterval创建一个24小时的间隔
        $interval = new DateInterval('P1D');
        // 使用DatePeriod根据起始日期和间隔创建一个周期
        $period = new DatePeriod($startDate, $interval, $endDate);
        foreach ($period as $date) {
            $start = $date->format('Ymd');
            $list = Member::getInstance()->where('deleted',0)->select();
            foreach($list as $v){
                $m = MemberService::getInstance()->account($v['id']);
                if(empty($m) || $m['isActiveSd'] == 0){
                    continue;
                }
                $ret = SandService::getInstance()->detailQuery($v['id'],$start,$start,1,100);
                if(!isset($ret['resultStatus']) || $ret['resultStatus'] != 'success'){
                    continue;
                }
                if(empty($ret['dataList'])){
                    continue;
                }
                foreach($ret['dataList'] as $value){
                    if(!in_array($value['transName'],['提现','银行卡充值'])){
                        continue;
                    }
                    try{
                        $data = [
                            'user_id' => $v['id'],
                            'real_name' => $account['real_name'] ?? '',
                            'phone' => $v['phone'],
                            'trans_name' => $value['transName'],
                            'trans_time' => $value['transTime'],
                            'post_script' => $value['postscript'],
                            'oppo_name' => $value['oppoName'],
                            'act_amt' => $value['actAmt'],
                            'io_flag' => $value['ioFlag'],
                            'oppo_account_type' => $value['oppoAccountType'],
                            'oppo_biz_user_no' => $value['oppoBizUserNo'],
                            'act_bal_aft' => $value['actBalAft'],
                            'remark' => $value['remark'],
                            'out_order_no' => $value['outOrderNo'],
                        ];
                        $where = [
                            ['user_id','=',$v['id']],
                            ['out_order_no','=',$value['outOrderNo']],
                        ];
                        $ret = \think\facade\Db::table('stats_sand_member_data')->where($where)->find();
                        dump(\think\facade\Db::table('stats_sand_member_data')->getLastSql());
                        if($ret){
                            $data['update_at'] = date('Y-m-d H:i:s');
                            \think\facade\Db::table('stats_sand_member_data')->where($where)->update($data);
                        }else{
                            \think\facade\Db::table('stats_sand_member_data')->insert($data);
                        }
                    }catch (\Exception $ex){
                        dump($ex->getMessage());
                        Log::error($ex->getMessage());
                    }
                }
            }
        }*/
        //$start = date('Ymd',strtotime('-1 day'));
        $start = $date;
        $list = Member::getInstance()->where('deleted',0)->select();
        foreach($list as $v){
            $m = MemberService::getInstance()->account($v['id']);
            if(empty($m) || $m['isActiveSd'] == 0){
                continue;
            }
            $ret = SandService::getInstance()->detailQuery($v['id'],$start,$start,1,100);
            if(!isset($ret['resultStatus']) || $ret['resultStatus'] != 'success'){
                continue;
            }
            if(empty($ret['dataList'])){
                continue;
            }
            foreach($ret['dataList'] as $value){
                if(!in_array($value['transName'],['提现','银行卡充值'])){
                    continue;
                }
                try{
                    $data = [
                        'user_id' => $v['id'],
                        'real_name' => $account['real_name'] ?? '',
                        'phone' => $v['phone'],
                        'trans_name' => $value['transName'],
                        'trans_time' => $value['transTime'],
                        'post_script' => $value['postscript'],
                        'oppo_name' => $value['oppoName'],
                        'act_amt' => $value['actAmt'],
                        'io_flag' => $value['ioFlag'],
                        'oppo_account_type' => $value['oppoAccountType'],
                        'oppo_biz_user_no' => $value['oppoBizUserNo'],
                        'act_bal_aft' => $value['actBalAft'],
                        'remark' => $value['remark'],
                        'out_order_no' => $value['outOrderNo'],
                    ];
                    $where = [
                        ['user_id','=',$v['id']],
                        ['out_order_no','=',$value['outOrderNo']],
                    ];
                    $ret = \think\facade\Db::table('stats_sand_member_data')->where($where)->find();
                    dump(\think\facade\Db::table('stats_sand_member_data')->getLastSql());
                    if($ret){
                        $data['update_at'] = date('Y-m-d H:i:s');
                        \think\facade\Db::table('stats_sand_member_data')->where($where)->update($data);
                    }else{
                        \think\facade\Db::table('stats_sand_member_data')->insert($data);
                    }
                }catch (\Exception $ex){
                    dump($ex->getMessage());
                    Log::error($ex->getMessage());
                }
            }
        }

    }
}
