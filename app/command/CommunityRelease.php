<?php


namespace app\command;


use app\api\consDir\CommunityConst;
use app\api\services\CommunityService;
use app\common\models\Community\CommunityOrder;
use app\common\models\Community\CommunityUser;
use app\common\models\Order\OrderError;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\facade\Log;

class CommunityRelease extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('community:release')
            ->setDescription('社区释放');
    }

    /**
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    protected function execute(Input $input, Output $output)
    {
        $output->info("开始:");

        $settlementTop = CommunityService::getInstance()->getCommunityConfig('settlement_top');
        $yesterday = date('Y-m-d', strtotime('-1 day')).' 23:59:59';
        $where = [
            ['community_status', '=', CommunityConst::CHECK_STATUS],
            ['is_sign', '=', 1],
            ['update_at', '<=', $yesterday],
        ];
        $community = CommunityUser::getInstance()->where($where)->select();
        $output->info(CommunityUser::getInstance()->getLastSql());
        foreach ($community as $val) {
            //获得上次社区订单
            $lastCommunityOrder = CommunityService::getInstance()->getLastCommunityOrder($val['lastOrderId']);
            //计算下一次社区价格
            $rateInfo = CommunityService::getInstance()->getCommunityPrice($val['totalAmount'], $lastCommunityOrder);
            CommunityUser::getInstance()->startTrans();
            try {
                $price = $val['totalAmount'];
                if ($settlementTop <= $rateInfo['totalAmount'] && $val['lastOrderId']) {//封顶了
                    $price = bcmul($val['totalAmount'], 0.5, 2);
                    $val['opPrice'] = bcmul($val['opPrice'], 0.5, 2);
                    CommunityService::getInstance()->splitCommunity($lastCommunityOrder, $price, $val['opPrice'], $val['opOrderNo']);
                    //裂变订单
                    CommunityOrder::getInstance()->where('id', $val['lastOrderId'])->update(['is_fission' => 1]);
                }
                $data = [
                    'last_enter_amount' => $price,
                    'community_status' => CommunityConst::RELEASE_STATUS,
                    'lock_status' => CommunityConst::LOCK,
                    'type' => 0,
                    'is_pay_service' => CommunityConst::NO_PAY_SERVICE,
                    'is_sign' => 0,
                    'total_amount' => $price,
                    'op_price' => $val['opPrice'],
                    'activity_id' => 0,
                ];
                CommunityUser::getInstance()->where('id', $val['id'])->update($data);
            } catch (\Exception $e) {
                Log::error($e->getMessage());
                CommunityUser::getInstance()->rollback();
            }
            CommunityUser::getInstance()->commit();
        }
        $output->info("结束:");
    }
}